﻿using eLedgerBusiness.DAL;
using eLedgerEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace eLedgerMVC.Controllers
{
    public class SearchNewController : System.Web.Http.ApiController
    {
        public string SearchLocation(Location searchParam)
        {
            ArrayList paramText = new ArrayList();
            paramText.Add("@SearchULN");
            paramText.Add("@SearchName");
            paramText.Add("@SearchAddress");
            paramText.Add("@SearchCity");
            paramText.Add("@SearchState");
            paramText.Add("@SearchZip");
            paramText.Add("@SearchWebNum");
            ArrayList paramValues = new ArrayList();
            paramValues.Add(replaceNull(searchParam.ULN));
            paramValues.Add(replaceNull(searchParam.Name));
            paramValues.Add(replaceNull(searchParam.Address));
            paramValues.Add(replaceNull(searchParam.City));
            paramValues.Add(replaceNull(searchParam.State));
            paramValues.Add(replaceNull(searchParam.Zip));
            paramValues.Add(replaceNull(searchParam.WebNum));
            string querycode = "getLoclist";
            DataTable dt = new WashDAL().getDataTable(querycode, paramText, paramValues);
            
            StringBuilder sbJson = new StringBuilder("{");
            sbJson.Append("\"SearchLocation\":[");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    sbJson.Append("{");
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", formattedULN(row["ULN"].ToString()));
                    sbJson.AppendFormat("\"{0}\":\"{1}\",", "Address", row["Address"]);
                    sbJson.AppendFormat("\"{0}\":\"{1}\",", "City", row["City"]);
                    sbJson.AppendFormat("\"{0}\":\"{1}\",", "State", row["State"]);
                    sbJson.AppendFormat("\"{0}\":\"{1}\",", "Zip", row["Zip"]);
                    sbJson.AppendFormat("\"{0}\":\"{1}\",", "Status", row["Status"]);
                    sbJson.AppendFormat("\"{0}\":\"{1}\",", "ContractType", row["ContractType"]);
                    sbJson.AppendFormat("\"{0}\":\"{1}\" ", "PropertyName", row["PropertyName"]);

                    sbJson.Append("}, ");
                }
                sbJson.Replace(", ", "],", sbJson.Length - 2, 2);
            }
            else
            {
                sbJson.Append("],");
            }
            sbJson.Append("\"total\":\"" + dt.Rows.Count + "\"}");

            sbJson.Replace(@"\", " ");
            return sbJson.ToString();

        }
        public string replaceNull(string value)
        {
            if (value == null)
                return "";
            else
                return value;
        }
        public string formattedULN(string ULN)
        {
            if (ULN.Length > 8)
            {
                ULN = ULN.Insert(4, "-");
                ULN = ULN.Insert(7, "-");
                return ULN;
            }
            else
                return ULN;
        } 
      
    }
}
