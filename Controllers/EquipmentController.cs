﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;

namespace eLedgerMVC.Controllers
{
    public class EquipmentController : ApiController
    {
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]         
        public dynamic getEquipment(string ULN)
        {
            string Equipment = new EquipmentBusiness().getEquipmentJson(ULN, "", "");
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Equipment, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]         
        public dynamic getmachinelist(string ULN)
        {
            string EqMachList = new EquipmentBusiness().getMachineListJson(ULN);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(EqMachList, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]         
        public dynamic getmachinelistbywebid(string uln, string webid)
        {
            string EqMachList = new EquipmentBusiness().getMachineListByWebIdJson(uln, webid);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(EqMachList, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
         }
         [System.Web.Http.HttpGet]
         public dynamic LocInventory(string ULN)
         {
             string LocInv = new EquipmentBusiness().getLocInventoryURL(ULN);

             var response = new HttpResponseMessage(HttpStatusCode.OK)
             {
                 Content = new StringContent(LocInv, System.Text.Encoding.UTF8, "application/json")
             };
             return response;
         }
    }
}
