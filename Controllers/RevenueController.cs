﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;


namespace eLedgerMVC.Controllers
{
    public class RevenueController : ApiController
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getRevenueamtSplt(string Id)
        {
            string Revenue = "{" + new RevenueBusiness().getRevenueCardSplitJson(Id) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Revenue, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getRevenue(string ULN, string splitRevenueNotes, string showRecords)
        {
            string Revenue;
            if (showRecords == "All")
                Revenue = "{" + new RevenueBusiness().getRevenueheader(ULN, "", "All") + "}";
            else
                Revenue = "{" + new RevenueBusiness().getRevenueheader(ULN, "", "") + "}";            

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Revenue, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        [OutputCache(Duration = 2000, Location = OutputCacheLocation.Client, VaryByParam = "*")]
        public dynamic getCommercial(string ULN, string splitRevenueNotes, string showRecords, string year)
        {
            string Revenue;
            year = year.Trim();
                Revenue = "{" + new RevenueBusiness().getRevenuedetails(ULN, "", "All",year) + "}";
                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(Revenue, System.Text.Encoding.UTF8, "application/json")
                };
                return response;
         }

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getRevenueCheckNumber(string I_CheckNumber_Start, string I_CheckNumber_End, string ULN)
        {
            string Revenue;

            Revenue = "{" + new RevenueBusiness().getRevenueCheckNumber(I_CheckNumber_Start, I_CheckNumber_End, ULN) + "}";

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Revenue, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }

        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getRevenueDocumentNumber(string DocumentNumber)
        {
            string Revenue;

            Revenue = "{" + new RevenueBusiness().getRevenueDocumentNumber(DocumentNumber) + "}";

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Revenue, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }

    }
}
