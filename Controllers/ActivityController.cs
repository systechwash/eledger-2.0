﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;
using System.Data;
using Newtonsoft.Json;

namespace eLedgerMVC.Controllers
{
    public class ActivityController : ApiController
    {
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]         
        public dynamic getActivity(string ULN)
        {

            string Activity = "{\"Activity\":" + new ActivityBusiness().getActivity(ULN) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Activity, System.Text.Encoding.UTF8, "application/json")
            };
            return response;     
        }

    }
}