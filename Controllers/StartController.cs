﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using eLedgerBusiness;
using eLedgerEntities;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Collections;
using eLedgerBusiness.DAL;
using System.Data;
using System;


namespace eLedgerMVC.Controllers
{
    public class StartController : Controller
    {       
        public ActionResult queryHelp()
        {
            var result = new FilePathResult("~/Views/Start/queryHelp.htm", "text/html");
            return result;
        }
        public ActionResult AccessDenied()
        {
            return View("AccessDenied"); //looks for a "AccessDenied.cshtml" page
        }
        [OutputCache(Duration = 100000, VaryByParam = "none")]
        public ActionResult search()
        {            
           string UserName = System.Web.HttpContext.Current.User.Identity.Name;
           // string UserName = "WASHLAUNDRY\\BBARATAM";
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }
            User userDet = new UserCredentials().GetLogonUser(UserName);
            if (userDet != null)
            {
                return View("eLedger2"); //looks for a "eLedger2.cshtml" page
              
            }
            else
            {
                return View("AccessDenied"); //looks for a "AccessDenied.cshtml" page
                //var result = new FilePathResult("~/Views/Shared/accessdenied.html", "text/html");
                //return result;
            }
        }
        [HttpGet]
        public ActionResult GoogleMapPlus(string uln)
        {
            var result = new FilePathResult("~/Views/Start/mapFile.html", "text/html");
            return result;
        }

        [HttpGet]
        public ActionResult AS400PDFPage(string uln)
        {
            var result = new FilePathResult("~/Views/Start/AS400PDFPage.html", "text/html");
            return result;
        }

        [HttpGet]
        public ActionResult ExportExcel(string param1, string param2, String param3)
        {
            ViewBag.uln = param1;
            ViewBag.loctype = param2;
            ViewBag.distance = param3;
            return View();
        }

       [HttpGet]
       public ActionResult AddNotes(string uln, string webid)
       {
           var result = new FilePathResult("~/Views/Start/AddNotes.html", "text/html");
           return result;
       }
       [HttpGet]
       public ActionResult AddAlert(string uln)
       {
           ViewBag.uln = uln;
           return View();
       }       
       [HttpGet]
       public ActionResult LegalNote(string uln, string threadid, string seqNo, string legalAction)
       {
           ViewBag.uln = uln;
           ViewBag.id = threadid;
           ViewBag.seqno = seqNo;
           ViewBag.legalAction = legalAction;
           return View(new Legal());
       }
       //Equipment LocInventory Interative View
       public string LocInventory(string ULN)
       {
           string LocInv = eLedgerBusiness.Utils.ReadWebConfig.LocInventory()+ULN;
           return LocInv;
       }
       public string Userlist()
       {
           string userlist = "{\"userlist\":" + new UserCredentials().GetUsersList() + "}";
           return userlist;
       }
       public string getUserName()
       {
           string UserName = System.Web.HttpContext.Current.User.Identity.Name;
           
          //string UserName="WASHLAUNDRY\\BBARATAM";
           if (UserName == null || UserName == "")
               UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
           string userlist = "{\"User\":" + new UserCredentials().GetJsonUserData(UserName) + "}";
           return userlist;
       }
       public ActionResult ServerError()
       {
           var result = new FilePathResult("~/Views/Shared/ServerError.html", "text/html");
           return result;
       }
    }
    }

