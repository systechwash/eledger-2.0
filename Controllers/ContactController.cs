﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;

namespace eLedgerMVC.Controllers
{
    public class ContactController : ApiController
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        [OutputCache(Duration = 1800, VaryByParam = "none", NoStore = true)]
        public dynamic getContactdetails(string Id, string Type)
        {
            string contact = new ContactBusiness().getContactOverviewJson(Id, Type);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(contact, System.Text.Encoding.UTF8, "application/json")
            };
            return response; 
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        [OutputCache(Duration = 2000, Location = OutputCacheLocation.Client, VaryByParam = "*")]
        public dynamic SearchContact(string id, string Name, string Address, string city, string State, string Zip, string phone, string flag)
        {
            string Searchcontact = new SearchBusiness().SearchContactList(id, Name, Address, city, State, Zip, phone, flag);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Searchcontact, System.Text.Encoding.UTF8, "application/json")
            };
            return response; 
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        [OutputCache(Duration = 2000, Location = OutputCacheLocation.Client, VaryByParam = "*")]
        public dynamic getlocationContacts(string ULN)
        {
            string Contacts = "{\"Contact\":" + new ContactBusiness().getlocContacts(ULN) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Contacts, System.Text.Encoding.UTF8, "application/json")
            };
            return response; 
        }

    }
}
