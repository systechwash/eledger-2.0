﻿var callurl;
var alertPath = "";
var alertapiUrl = alertPath + '/api/Front';

function AlertDocs(url) {
    window.open(url, "AlertsHelp", "height=590,width=795,status=no,scrollbars=yes,resizable=yes,titlebar=no,menubar=no,left=10,top=10")
}
function getAlertULN(ULN) {
    callurl = "/api/Alerts/getAlertsaction?uln=" + ULN + "&History=0&Future=0";
    $('#AlertsWidget').hide();
    var dataSourceAlerts = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                $.ajax({
                    url: callurl,
                    dataType: "json",
                    type: "POST",
                    success: function (result) {
                        if (result.HistoryFlag == 1) $("#bt_history").removeAttr("disabled");
                        else $("#bt_history").attr("disabled", "disabled");
                        if (result.FutureFlag == 1) $("#bt_future").removeAttr("disabled");
                        else $("#bt_future").attr("disabled", "disabled");
                        if (result.Alert.length > 0) {
                            $('#AlertsWidget').show();
                            $("#AlertsWidget").kendoGrid({
                                rowTemplate: kendo.template($("#AlertsWidgetTemplate").html()),
                                dataSource: result.Alert,
                                scrollable: false,
                                pagable: true,
                                selectable: "row"
                            })
                        }
                    },
                    error: function (result) {
                        options.error(result)
                    }
                })
            }
        }
    });
    dataSourceAlerts.fetch(function () {
        console.log(dataSourceAlerts.view().length)
    })
}
function getfuturealertsuln(ULN) {
    callurl = "/api/Alerts/getAlertsaction?uln=" + ULN + "&History=0&Future=1";
    $('#AlertsWidget').hide();
    var dataSourceAlerts = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                $.ajax({
                    url: callurl,
                    dataType: "json",
                    type: "POST",
                    success: function (result) {
                        if (result.HistoryFlag == 1) $("#bt_history").removeAttr("disabled");
                        else $("#bt_history").attr("disabled", "disabled");
                        if (result.FutureFlag == 1) $("#bt_future").removeAttr("disabled");
                        else $("#bt_future").attr("disabled", "disabled");
                        if (result.Alert.length > 0) {
                            $('#AlertsWidget').show();
                            $("#AlertsWidget").kendoGrid({
                                rowTemplate: kendo.template($("#AlertsWidgetTemplate").html()),
                                dataSource: result.Alert,
                                scrollable: false,
                                pagable: true,
                                selectable: "row"
                            })
                        }
                    },
                    error: function (result) {
                        options.error(result)
                    }
                })
            }
        }
    });
    dataSourceAlerts.fetch(function () {
        console.log(dataSourceAlerts.view().length)
    })
}
function AlertDocs(url) {
    window.open(url, "AlertsHelp", "height=590,width=795,status=no,scrollbars=yes,resizable=yes,titlebar=no,menubar=no,left=10,top=10")
}
function toggleAlertDetail(AlertInstNum) {
    $("#AlertInstNum" + AlertInstNum).toggle()
}
function removeTdColor() {
    $(".activeAlert").css("background-color", "");
    $(".activeAlert").removeAttr('style')
}
function getAlertList() {
    callurl = alertPath + "/api/Alerts/searchAlerts/all/all";
    LoadDynamicContent(callurl, "dv_searchAlerts")
}
function closeAlert() {
    parent.$.colorbox.close();
    return false
}
function showAddAlerts() {
    var ULN = $('#hd_Uln').val();
    $('#txtUsrLstFltr').val("");
    $('#AlertMSG').val("");
    $('#SelUSRList1').find('option').remove();
    FillListBox();
    $('#myModalAlert').modal('show');
    $("#DueDate").kendoDatePicker({
        value: new Date(),
        format: "MM/dd/yyyy"
    })
}
function showhistoryAlerts(id) {
    $('#AlertsWidget').show();
    $('#bodyAlerts').html('<tr><td style="height:64px;text-align:center;" colspan="8"><img src="/Content/eledger2/Default/loading-image.gif" /></td></tr>');
    var alertappPath = "";
    var Future = 0;
    var History = 0;
    var ULN = $('#hd_Uln').val();
    if (id == "1") {
        if ($("#bt_history").val() == "Show Past Alerts") {
            History = 1;
            $("#bt_history").val("Hide Past Alerts");
            $("#bt_history").attr("title", "hide past alerts list for " + ULN);
            document.getElementById('bt_history').innerHTML = '<i class="icon-cog"></i>Hide Past Alerts'
        } else if ($("#bt_history").val() == "Hide Past Alerts") {
            History = 0;
            $("#bt_history").val("Show Past Alerts");
            $("#bt_history").attr("title", "show past alerts list for " + ULN);
            document.getElementById('bt_history').innerHTML = '<i class="icon-cog"></i>Show Past Alerts'
        } else History = 0;
        if ($("#bt_future").val() == "Show Future Alerts") Future = 0;
        else Future = 1
    }
    if (id == "2") {
        if ($("#bt_future").val() == "Show Future Alerts") {
            Future = 1;
            $("#bt_future").val("Hide Future Alerts");
            $("#bt_future").attr("title", "hide future alerts list for " + ULN);
            document.getElementById('bt_future').innerHTML = '<i class="icon-cog"></i>Hide Future Alerts'
        } else if ($("#bt_future").val() == "Hide Future Alerts") {
            Future = 0;
            $("#bt_future").val("Show Future Alerts");
            $("#bt_future").attr("title", "show future alerts list for " + ULN);
            document.getElementById('bt_future').innerHTML = '<i class="icon-cog"></i>Show Future Alerts'
        } else Future = 0;
        if ($("#bt_history").val() == "Show Past Alerts") History = 0;
        else History = 1
    }
    callurl = "/api/Alerts/getAlertsaction?uln=" + ULN + "&History=" + History + "&Future=" + Future;
    $("div").removeClass(".k-grid k-widget k-secondary");
    var dataSourceAlerts = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                $.ajax({
                    url: callurl,
                    dataType: "json",
                    type: "POST",
                    success: function (result) {
                        if (result.HistoryFlag == 1) $("#bt_history").removeAttr("disabled");
                        else $("#bt_history").attr("disabled", "disabled");
                        if (result.FutureFlag == 1) $("#bt_future").removeAttr("disabled");
                        else $("#bt_future").attr("disabled", "disabled");
                        if (result.Alert.length > 0) {
                            $("#AlertsWidget").kendoGrid({
                                rowTemplate: kendo.template($("#AlertsWidgetTemplate").html()),
                                dataSource: result.Alert,
                                scrollable: false,
                                pagable: true
                            })
                        }
                        else { $('#AlertsWidget').hide(); }
                    },
                    error: function (result) {
                        options.error(result)
                    }
                })
            }
        }
    });
    dataSourceAlerts.fetch(function () {
        console.log(dataSourceAlerts.view().length)
    })
}

function FillListBox() {
    var alertapiUrl = alertPath + "/api/Front";
    handler = "UserHandler";
    action = "UsersList";
    callurl = "start/Userlist";
    $("#USRList1").empty();
    $.getJSON(callurl).done(function (data) {
        $.each(data, function (key, item) {
            for (j = 0; j < item.length; j++) {
                $("#USRList1").append($("<option>       </option>").val(item[j].UserID).html(item[j].Display_Name))
            }
            jQuery.fn.filterByText = function (textbox, selectSingleMatch) {
                return this.each(function () {
                    var select = this;
                    var options = [];
                    $(select).find('option').each(function () {
                        options.push({
                            value: $(this).val(),
                            text: $(this).text()
                        })
                    });
                    $(select).data('options', options);
                    $(textbox).bind('change keyup', function () {
                        var options = $(select).empty().data('options');
                        var search = $.trim($(this).val());
                        var regex = new RegExp(search, "gi");
                        $.each(options, function (i) {
                            var option = options[i];
                            if (option.text.match(regex) !== null) {
                                $(select).append($('<option>').text(option.text).val(option.value))
                            }
                        });
                        if (selectSingleMatch === true && $(select).children().length === 1) {
                            $(select).children().get(0).selected = true
                        }
                    })
                })
            }
        });
        $('#USRList1').filterByText($('#txtUsrLstFltr'), true)
    })
}
function listbox_Clear(sourceID) {
    var src = document.getElementById(sourceID);
    for (var count = 0; count < src.options.length; count++) {
        try {
            src.remove(count, null)
        } catch (error) {
            src.remove(count, null)
        }
        count--
    }
}
function listbox_Remove(sourceID) {
    var src = document.getElementById(sourceID);
    for (var count = 0; count < src.options.length; count++) {
        if (src.options[count].selected == true) {
            try {
                src.remove(count, null)
            } catch (error) {
                src.remove(count, null)
            }
            count--
        }
    }
}
function listbox_Add(sourceID, destID) {
    var src = document.getElementById(sourceID);
    var dest = document.getElementById(destID);
    for (var count = 0; count < src.options.length; count++) {
        if (src.options[count].selected == true) {
            var option = src.options[count];
            var newOption = document.createElement("option");
            newOption.value = option.value;
            newOption.text = option.text;
            newOption.selected = true;
            var exists = false;
            $('#' + destID + ' option').each(function () {
                if (this.value == newOption.value) {
                    exists = true
                }
            });
            if (!exists) {
                try {
                    dest.add(newOption, null)
                } catch (error) {
                    dest.add(newOption)
                }
            }
        }
    }
}
function EventTypeSelect() {
    var DueDate = $("#DueDate").val();
    var txtSeqNum = $("#txtSeqNum").val();
    var txtRptNum = $("#txtRptNum").val();
    if (DueDate.Text == "") {
        DueDate.Text = DateTime.Now.ToShortDateString()
    }
    if (txtSeqNum.Text == "") {
        txtSeqNum.Text = "1"
    }
    if (txtRptNum.Text == "") {
        txtRptNum.Text = "1"
    }
    var radios = document.getElementsByName("Event");
    var datepicker = $("#DueDate").data("kendoDatePicker");
    datepicker.enable(true);
    if (radios[0].checked) {
        datepicker.enable(true);
        $("LnkDueDate").prop("disabled", false);
        $("#txtSeqNum").prop("disabled", true);
        $("#txtRptNum").prop("disabled", true);
        $("#DueDate").datepicker('enable');        
    } else if (radios[1].checked) {
        $("#txtSeqNum").prop("disabled", false);
        $("#txtRptNum").prop("disabled", false);
        $("#DueDate").prop("disabled", true);
        $("LnkDueDate").prop("disabled", true);
        datepicker.enable(false);                
    }
}
function getAlertRecipients(AlertEventNum, Type) {
    var callurl = alertPath + "/api/Alerts/AlertEventRecipientList?uln=" + AlertEventNum + "&webid=" + Type;
    var data = "";
    $.getJSON(callurl).done(function (data) {
        $.each(data, function (key, item) {
            if (item != "") {
                data += "<tr><td>" + item.Recipient_Name + "</td></tr>"
            }
        });
        data = data.replace("[object Object],[object Object]", "");
        $("#tbl_Recipients_" + AlertEventNum).empty();
        $("#tbl_Recipients_" + AlertEventNum).append(data);
        $("#tbl_receipientmain_" + AlertEventNum).toggle()
    });
    $(".activeAlert").css("background-color", "")
}
function SaveAlert() {
    var uln = $('#hd_Uln').val();
    var select = document.getElementById('SelUSRList1');
    var alertapiUrl = alertPath + "/api/Front";
    callurl = '/api/Alerts/AddAlerts/';
    var ULN, ToUserID = "",
        AlertType, AlertMSG, DueDate, isPrivate, AlertInstNum, SequenceType, SequenceNum, RepeatNum;
    var d = new Date();
    var radios = document.getElementsByName("Event");
    AlertInstNum = 0;
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var dt = month + "/" + day + "/" + year;
    var d1 = Date.parse(dt);
    var d2 = Date.parse($("#DueDate").val());
    DueDate = $("#DueDate").val();
    if ($("#AlertMSG").val() == "") {
        alert("**Please enter a message**")
    } else if ($("#txtSeqNum").val() == "" || $("#txtSeqNum").val() == "0") {
        alert("**Cycle sequence number cannot be 0 or null**")
    } else if ($("#txtRptNum").val() == "" || $("#txtSeqNum").val() == "0") {
        alert("**Number of times to repeat should be greater than 0**")
    } else if (!(/^\d+$/.test($("#txtSeqNum").val()))) {
        alert("**Cycle sequence should be an integer value greater than 0**")
    } else if (!(/^\d+$/.test($("#txtRptNum").val()))) {
        alert("**Repeat num should be an integer greater than 0**")
    } else if (select.options.length == 0) {
        alert("No recipient user selected.")
    } else if (d1 > d2) {
        alert("Past date cannot be a due date.")
    } else {
        if (radios[0].checked) {
            SequenceType = 2;
            SequenceNum = 0;
            RepeatNum = 0;
            DueDate = $("#DueDate").val()
        } else {
            SequenceType = 1;
            SequenceNum = $("#txtSeqNum").val();
            RepeatNum = $("#txtRptNum").val()
        }
        isPrivate = $('.Private').prop("checked");
        AlertType = 8;
        ULN = uln;
        AlertMSG = $("#AlertMSG").val();
        for (var i = 0; i < select.options.length; i++) {
            if (ToUserID != "") ToUserID = ToUserID + "," + select.options[i].value;
            else ToUserID = select.options[i].value
        }
        var Alerts = {
            ULN: ULN,
            AlertType: AlertType,
            AlertMSG: AlertMSG,
            isPrivate: isPrivate,
            SequenceType: SequenceType,
            SequenceNum: SequenceNum,
            RepeatNum: RepeatNum,
            ToUserID: ToUserID,
            DueDate: DueDate
        };
        $.ajax({
            url: callurl,
            type: "POST",
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(Alerts),
            success: function (data, st) {
                $('#myModalAlert').modal('hide');
                $('#AlertsWidget').hide();
                var dataSourceAlerts = new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            $.ajax({
                                url: "/eledger2/api/Alerts/getAlerts?ULN=" + ULN,
                                dataType: "json",
                                type: "POST",
                                success: function (result) {
                                    if (result.HistoryFlag == 1) $("#bt_history").removeAttr("disabled");
                                    else $("#bt_history").attr("disabled", "disabled");
                                    if (result.FutureFlag == 1) $("#bt_future").removeAttr("disabled");
                                    else $("#bt_future").attr("disabled", "disabled");
                                    if (result.Alert.length > 0) {
                                        $('#AlertsWidget').show();
                                        $("#AlertsWidget").kendoGrid({
                                            rowTemplate: kendo.template($("#AlertsWidgetTemplate").html()),
                                            dataSource: result.Alert,
                                            scrollable: false,
                                            pagable: true
                                        })
                                    }
                                },
                                error: function (result) {
                                    options.error(result)
                                }
                            })
                        }
                    }
                });
                dataSourceAlerts.fetch(function () {
                    console.log(dataSourceAlerts.view().length)
                })
            },
            error: function () {
                $('#myModalAlert').modal('hide')
            }
        })
    }
}
function AlertUpdate(ULN, ALID, newStatus, newClear) {
    callurl = '/api/Alerts/UpdAlerts/';
    var Alerts = {
        ULN: ULN,
        AlertInstNum: ALID,
        Staus: newStatus,
        Clear: newClear
    };
    $.ajax({
        url: callurl,
        type: "POST",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(Alerts)
    }).always(function () {
        if (newClear == 3 || newClear == 2) getfuturealertsuln(ULN);
        else showhistoryAlerts(2)
    })
}