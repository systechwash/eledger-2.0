﻿function searchLocationDetails(e, t, n, r, i, s, o, u) {
    $("#btn_LocationSub").prop("disabled", true);
    $(".k-grid-pager").remove();
    $("div").parent().removeClass(".k-grid k-widget");
    $("#bodyLocationResults").html('<tr><td style="height:64px;"></td></tr>');
    $("#dvContact-results").hide();
    $("#dvAlert-results").hide();
    $("#empty-results").hide();
    $("#error-results").hide();
    $("#dvLocation-results").show();
    $("#dvAlert-results_Details").hide();
    $("#search-results").show();
    var a = "/eledger2/api/Location/SearchLocation";
    if (e.length > 9) e = ReplaceformatUln(e);
    if (e == "" && t == "" && r == "" && i == "" && s == "" && o == "" && u == "" && n == "") {
        $("#error-results").show();
        $("#empty-results").hide();
        $("#dvLocation-results").hide();
        $("#btn_LocationSub").prop("disabled", false)
    } else {
        postArgs = new Object;
        postArgs.ULN = e;
        postArgs.Name = t;
        postArgs.Address = r;
        postArgs.City = i;
        postArgs.State = s;
        postArgs.Zip = o;
        postArgs.WebNum = u;
        postArgs.LaundryRooms_Fmt = n;
        postData = JSON.stringify(postArgs);
        var f = new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    url: a,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                },
                parameterMap: function (e, t) {
                    return postData
                }
            },
            schema: {
                data: "SearchLocation",
                total: "total"
            },
            pageSize: 3
        });
        $.get("/Templates/searchLocationResultsTemplate.htm", function (e) {
            $("#Location-Results").kendoGrid({
                rowTemplate: kendo.template(e),
                dataSource: f,
                scrollable: false,
                selectable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 3
                },
                dataBound: function () {
                    if (f._view.length == 0) {
                        $("#empty-results").show();
                        $("#dvLocation-results").hide();
                        $("#error-results").hide();
                        $("#btn_LocationSub").prop("disabled", false)
                    } else {
                        $("#dvLocation-results").show();
                        $("#empty-results").hide();
                        $("#error-results").hide();
                        $("#btn_LocationSub").prop("disabled", false)
                    }
                }
            })
        })
    }
}
function searchLocation() {
    location.hash = "#searchLoc~" + $("#ULN").val() + "~" + $("#Name").val() + "~" + $("#LRD").val() + "~" + $("#Address").val() + "~" + $("#City").val() + "~" + $("#State").val() + "~" + $("#Zip").val() + "~" + $("#WebNum").val()
}
function searchContact() {
    
    $("#btn_ContactSub").prop("disabled", true);
    $(".k-grid-pager").remove();
    $("#bodyContactResults").html('<tr><td style="height:64px;"><B>Loading . . .</B></td></tr>');
    $("#LocationWidget").hide();
    $("div").parent().removeClass(".k-grid k-widget k-secondary");
    $("#ContactWidget").hide();
    $("#dvLocation-results").hide();
    $("#dvAlert-results").hide();
    $("#dvAlert-results_Details").hide();
    $("#error-results").hide();
    $("#empty-results").hide();
    
    $("#search-results").show();
    Id = $("#ContactID").val();
    Category = $("#selcat").val();
    Name = $("#ContactName").val();
    City = $("#ContactCity").val();
    State = $("#ContactState").val();
    Phone = $("#ContactPhone").val();
    Zip = $("#ContactZip").val();
    Address = $("#ContactAddress").val();
    if (Id == "" && Name == "" && City == "" && State == "" && Phone == "" && Address == "" && Zip == "") {
        $("#dvContact-results").hide();
        $("#empty-results").hide();
        $("#error-results").show();
        $("#btn_ContactSub").prop("disabled", false)
    } else {
        var e = "/eledger2/api/Contact/SearchContact?ID=" + Id + "&Name=" + Name + "&Address=" + Address + "&City=" + City + "&State=" + State + "&Zip=" + Zip + "&phone=" + Phone + "&flag=" + Category;
        var t = new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    url: e,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }
            },
            schema: {
                data: "SearchContact",
                total: "total"
            },
            pageSize: 10
        });
        $.get("/Templates/searchContactResultTemplate.html", function (e) {
            $(".k-grid-pager").remove();
            $("#Contact-Results").kendoGrid({
                rowTemplate: e,
                dataSource: t,
                scrollable: false,
                selectable: true,
                pageable: {
                    Input: false,
                    Numeric: true,
                    Info: true,
                    PreviousNext: true,
                    refresh: false,
                    pageSizes: false,
                    buttonCount: 5
                },
                dataBound: function () {
                    if (t._view.length == 0) {
                        $("#empty-results").show();
                        $("#dvContact-results").hide();
                        $("#error-results").hide();
                        $("#btn_ContactSub").prop("disabled", false)
                    } else {
                        $("#dvContact-results").show();
                        $("#empty-results").hide();
                        $("#error-results").hide();
                        $("#btn_ContactSub").prop("disabled", false)
                    }
                }
            })
        })
    }
}
function showAlertsdetails() {    
    $("#bodyAlertResults").html('<tr><td style="height:64px;"></td></tr>');
    $("#error-results").hide();
    $("#empty-results").hide();
    $("#dvLocation-results").hide();
    $("#dvContact-results").hide();
    $("#dvAlert-results").show();
    var e = new kendo.data.DataSource({
        transport: {
            read: function (e) {
                $.ajax({
                    url: "/eledger2/api/Alerts/SearchAlert/",
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e == "") {
                            $("#dvAlert-results").hide();
                            $("#empty-results").show()
                        } else {
                            $.get("/Templates/searchAlertResultTemplate.html", function (t) {
                                $("#alertsresults").kendoGrid({
                                    rowTemplate: t,
                                    dataSource: e,
                                    scrollable: false,
                                    pagable: true
                                });                                
                            })
                        }
                    },
                    error: function (t) {
                        e.error(t)
                    }
                })
            }
        }
    });
    e.fetch(function () {
        console.log(e.view().length)
    });    
}
function showAlertSearchDetails(e, t) {
    $("#alertstatus").val(e);
    $("#alertsubtype").val(t);
    var n = "/api/Alerts/SearchAlertsType/?alertstatus=" + e + "&alerttype=" + t;
    var r = new kendo.data.DataSource({
        transport: {
            read: {
                type: "POST",
                url: n,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }
        },
        schema: {
            data: "SearchAlert",
            total: "total"
        },
        pageSize: 10
    });    
    $("#search-results").hide();
    $("#empty-results").hide();
    $("#dvAlert-results").show();
    $("#search-results").show();
    $("#dvAlert-results_Details").show();
    $.get("/Templates/AlertSearchResultTemplate.html", function (e) {        
        $("#Alert-Results_Details").kendoGrid({
            rowTemplate: e,
            dataSource: r,
            scrollable: false,
            pageable: {
                Input: false,
                Numeric: true,
                Info: true,
                PreviousNext: true,
                refresh: false,
                pageSizes: false,
                buttonCount: 5
            },
            dataBound: function () {
                if (r._view.length == 0) {
                    $("#empty-results").show();
                    $("#dvAlert-results_Details").hide();
                    $("#error-results").hide()
                } else {
                    $("#dvAlert-results_Details").show();
                    $("#empty-results").hide();
                    $("#error-results").hide()
                }
            }
        })
    });
    r.fetch(function () {
        console.log(r.view().length)
    })
}
function AlertSearchULN(e) {
    $("#LocationWidget").hide();
    $("#ContactWidget").hide();
    $("#dvLoading").attr("style", "display:inline-block; width:100%; height:100%; text-align:center; vertical-align:top;");
    if (!flagTemplateAppended) {
        $.get("/Templates/locationOverviewTemplate.html", function (e) {
            $("div").parent().removeClass(".k-grid k-widget k-secondary");
            $("body").append(e);
            $("#LocationWidget").css("padding-bottom", "500px")
        }).done(function () {
            flagTemplateAppended = true;
            loactionResult(e)
        })
    } else loactionResult(e)
}
function getLocationOverview(e) {
    $(this).css("background-color", "#CEE3D0");
    scrollToWidget("main-content");
    $("#hd_Uln").val(ReplaceformatUln(e));
    $("#LocationWidget").hide();
    $("#ContactWidget").hide();
    $("#dvLoading").attr("style", "display:inline-block; width:100%; height:100%; text-align:center; vertical-align:top;");
    if (!flagTemplateAppended) {
        $.get("/Templates/locationOverviewTemplate.html", function (e) {
            $("div").removeClass(".k-grid k-widget k-secondary");
            $("body").append(e);
            $("#LocationWidget").css("padding-bottom", "500px")
        }).done(function () {
            flagTemplateAppended = true;
            loactionResult(e)
        })
    } else {
        loactionResult(e)
    }
}
function clearcontent() {
    $("#locationOverview tbody").remove();
    $("#locationHeader tbody").remove();
    $("#locationOverview1 tbody").remove();
    $("#locationOverview2 tbody").remove();
    $("#locationOverview3 tbody").remove();
    $("#locationOverview4 tbody").remove();
    $("#contactWidget tbody").remove();
    $("#agreementTable1 tbody").remove();
    $("#agreementTable2 tbody").remove();
    $("#agreementTable3 tbody").remove();
    $("#agreementTable4 tbody").remove();
    $("#AgreementNotes tbody").remove()
}
function loactionResult(e) {
    EqGridDetails('list');
    $("#hd_Uln").val(e);
    countWidgets = 0;
    e = ReplaceformatUln(e);
    $("#contactTitle").attr("title", "this widget shows the contact associated with " + e);
    $("#activityTitle").attr("title", "this widget shows the important activity associated for " + e);
    $("#legalTitle").attr("title", "this widget shows the legal details for " + e);
    $("#alertsTitle").attr("title", "this widget shows is any alert you have for " + e);
    $("#notesTitle").attr("title", "this widget shows eledger notes for " + e);
    $("#agreementTitle").attr("title", "this widget shows agreement information for " + e);
    $("#revenueTitle").attr("title", "this widget shows the revenue for " + e);
    $("#revenueSpltTitle").attr("title", "this widget shows the revenue for " + e);
    $("#equipmentTitle").attr("title", "this widget shows the equipment details for " + e);
    $("#serviceTitle").attr("title", "this widget shows the service history for " + e);
    $("#bt_history").attr("title", "show past alerts list for " + e);
    $("#bt_future").attr("title", "show future alerts list for " + e);
    $("#btAddAlert").attr("title", "add a new alert for " + e);
    $("#bt_list").attr("title", "show list of equipment associated with " + e);
    $("#bt_Inter").attr("title", "show interactive list of equipment associated with " + e + " in a new window");
    $("html, body").animate({
        scrollTop: 0
    }, 600);

    var t = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2/api/Location/getLocationSummary?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.LocationSummary.length >= 1) {
                            $("#LocationOverviewWidget").show();
                            $("#locationOverview").kendoGrid({
                                rowTemplate: kendo.template($("#locationOverviewTemplate").html()),
                                dataSource: e.LocationSummary,
                                scrollable: false,
                                pagable: true,
                                refresh: false,
                                selectable:false
                            });
                            $("#locationHeader").kendoGrid({
                                rowTemplate: kendo.template($("#locationHeaderTemplate").html()),
                                dataSource: e.LocationSummary,
                                scrollable: false,
                                pagable: true,
                                refresh: false,
                                selectable:false
                            });
                            $("#locationOverview1").kendoGrid({
                                rowTemplate: kendo.template($("#locationOverview1Template").html()),
                                dataSource: e.LocationSummary,
                                scrollable: false,
                                pagable: true,
                                refresh: false,
                                selectable:false
                            });
                            $("#locationOverview2").kendoGrid({
                                rowTemplate: kendo.template($("#locationOverview2Template").html()),
                                dataSource: e.LocationSummary,
                                scrollable: false,
                                pagable: true,
                                refresh: false,
                                selectable:false
                            });
                            $("#locationOverview3").kendoGrid({
                                rowTemplate: kendo.template($("#locationOverview3Template").html()),
                                dataSource: e.LocationSummary,
                                scrollable: false,
                                pagable: true,
                                refresh: false,
                                selectable:false
                            });
                            $("#locationOverview4").kendoGrid({
                                rowTemplate: kendo.template($("#locationOverview4Template").html()),
                                dataSource: e.SalesTeam,
                                scrollable: false,
                                pagable: true,
                                refresh: false,
                                selectable:false
                            });
                            $("th").removeClass("k-header");
                            $(document).attr("title", $("#locationPageTitle").val())
                        } else {
                            $("#LocationOverviewWidget").hide()
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    },
                    complete: function (e) {
                        widgetCompleted("Location")
                    }
                })
            }
        }
    });
    t.fetch(function () {
        console.log(t.view().length)
    });
    var n = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2/api/Contact/getlocationContacts?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.Contact.length >= 1) {
                            $("#contactWidget").show();
                            $("#contactWidget").kendoGrid({
                                rowTemplate: kendo.template($("#contactWidgetTemplate").html()),
                                dataSource: e.Contact,
                                scrollable: false,
                                pagable: true
                            });
                            $("th").removeClass("k-header")
                        } else {
                            $("#contactWidget").hide()
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    }
                })
            }
        }
    });
    n.fetch(function () {
        console.log(n.view().length)
    });
    var r = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2/api/Agreement/getAgreementDetails?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.Agreement.length <= 0) {
                            $("#agreementWidget").hide();
                            $("#AgreementNotes").hide()
                        } else {
                            $("#agreementWidget").show();
                            $("#agreementTable1").kendoGrid({
                                rowTemplate: kendo.template($("#AgreementTable1WidgetTemplate").html()),
                                dataSource: e.Agreement,
                                scrollable: false,
                                pagable: true
                            });
                            $("#agreementTable2").kendoGrid({
                                rowTemplate: kendo.template($("#AgreementTable2WidgetTemplate").html()),
                                dataSource: e.Agreement,
                                scrollable: false,
                                pagable: true
                            });
                            $("#agreementTable3").kendoGrid({
                                rowTemplate: kendo.template($("#AgreementTable3WidgetTemplate").html()),
                                dataSource: e.Agreement,
                                scrollable: false,
                                pagable: true
                            });
                            $("#agreementTable4").kendoGrid({
                                rowTemplate: kendo.template($("#AgreementTable4WidgetTemplate").html()),
                                dataSource: e.Agreement,
                                scrollable: false,
                                pagable: true
                            });
                            if (e.AgreementNotes.length > 0) {
                                $("#AgreementNotes").show();
                                $("#AgreementNotes").kendoGrid({
                                    rowTemplate: kendo.template($("#AgreementNotesWidgetTemplate").html()),
                                    dataSource: e.AgreementNotes,
                                    scrollable: false,
                                    pagable: true
                                });
                                if (e.AgreementSalesTeam.length > 0) {
                                    $("#salesAgreePopuptable").kendoGrid({
                                        rowTemplate: kendo.template($("#salesAgreePopuptableTemplate").html()),
                                        dataSource: e.AgreementSalesTeam,
                                        scrollable: false,
                                        pagable: true
                                    })
                                }
                                $("#AgreementNotes").show()
                            }
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    }
                })
            }
        }
    });
    r.fetch(function () {
        console.log(r.view().length)
    });
    $("#NotesWidget").kendoGrid({
        rowTemplate: kendo.template($("#NotesWidgetTemplate").html()),
        dataSource: {
            transport: {
                read: {
                    url: "/eledger2/api/Notes/getNotes?ULN=" + e,
                    dataType: "json",
                    type: "POST"
                }
            },
            schema: {
                data: "Notes"
            }
        },
        scrollable: false,
        pagable: true
    });
    $("#NotesWidget").hide();
    var i = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2/api/Notes/getNotes?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.Notes.length > 0) {
                            $("#NotesWidget").show();
                            $("#NotesWidget").kendoGrid({
                                rowTemplate: kendo.template($("#NotesWidgetTemplate").html()),
                                dataSource: e.Notes,
                                scrollable: false,
                                pagable: true
                            })
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    }
                })
            }
        }
    });
    i.fetch(function () {
        console.log(i.view().length)
    });
    $("#AlertsWidget").hide();
    var s = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2/api/Alerts/getAlerts?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.HistoryFlag == 1) {
                            $("#bt_history").removeAttr("disabled");
                            $("#bt_history").val("Show Past Alerts");
                            document.getElementById("bt_history").innerHTML = '<i class="icon-cog"></i>Show Past Alerts'
                        } else $("#bt_history").attr("disabled", "disabled");
                        if (e.FutureFlag == 1) {
                            $("#bt_future").removeAttr("disabled");
                            $("#bt_future").val("Show Future Alerts");
                            document.getElementById("bt_future").innerHTML = '<i class="icon-cog"></i>Show Future Alerts'
                        } else $("#bt_future").attr("disabled", "disabled");
                        if (e.Alert.length > 0) {
                            $("#AlertsWidget").show();
                            $("#AlertsWidget").kendoGrid({
                                rowTemplate: kendo.template($("#AlertsWidgetTemplate").html()),
                                dataSource: e.Alert,
                                scrollable: false,
                                pagable: false,
                                selectable: false
                            })
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    }
                })
            }
        }
    });
    s.fetch(function () {
        console.log(s.view().length)
    });
    $("#LegalWidget").hide();
    var o = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2/api/Legal/getLegal?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        ExportHide(e.hasLegalAccess);
                        if (e.Legal.length > 0) {
                            $("#LegalWidget").show();
                            if (e.hasLegalAccess == 1 || e.hasLegalAccess == 4) {
                                $("#NewIssue").css("display", "block");
                                $("#AdminLegal").css("display", "table-row");
                                $("#NormalLegal").css("display", "none")
                            } else {
                                $("#NewIssue").css("display", "none");
                                $("#AdminLegal").css("display", "none");
                                $("#NormalLegal").css("display", "table-row")
                            }
                            $("#LegalWidget").kendoGrid({
                                rowTemplate: kendo.template($("#LegalWidgetTemplate").html()),
                                dataSource: e.Legal,
                                selectable: false,
                                scrollable: false,
                                pagable: true
                            })
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    }
                })
            }
        }
    });
    $("th").removeClass("k-header");
    o.fetch(function () {
        console.log(o.view().length)
    });
    $("#" + currentYear + "ExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
    $("#" + currentYear + "RExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
    $("#" + currentYear + "ReExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
    $("#" + currentYear + "CExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
    $("#RevenueDetails_" + currentYear).hide();
    $("#RevenueDetails_" + currentYear + "_Rental").hide();
    $("#RevenueSpltNotes" + currentYear).hide();
    $("#RevenueSpltDetails_" + currentYear).hide();
    $("#RevenueSpltNotes_" + currentYear).hide();
    $("#RevenuWidgetHeader").show();
    $("#showall").attr("title", "Show All Records");
    $("#Spltshowall").attr("title", "Show All Records");
    $("#RevenueSpltNoteDet").hide();
    var u = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/api/Revenue/getRevenue?ULN=" + e + "&splitRevenueNotes=" + "" + "&showRecords=" + "",
                    dataType: "json",
                    type: "POST",
                    success: function (t) {
                        if (t.RevenueHeader.length > 0) {
                            Revenueheaders = t.RevenueHeader;
                            if (t.RevenueHeader[0].DisplayYear != "Pinned Notes") revenueyear = t.RevenueHeader[0].DisplayYear;
                            else revenueyear = t.RevenueHeader[1].DisplayYear;
                            $("#RevenuWidgetHeader").kendoGrid({
                                rowTemplate: kendo.template($("#RevenuWidgetHeaderTemplate").html()),
                                dataSource: t.RevenueHeader,
                                scrollable: false,
                                pagable: true
                            });
                            $("div").removeClass("k-grid k-widget k-secondary");
                            revenuedetailsdata(revenueyear, e, "", "");
                            $("#RevenueDetails_" + revenueyear).show();
                            $("#RevenueDetails_" + revenueyear + "_Rental").show();
                            $("#RevenueSpltDetails_" + revenueyear).show();
                            $("#RevenueSpltDetails_" + revenueyear + "_Rental").show();
                            $("#RevenueSpltNotes_" + revenueyear).show();
                            $("#RevenueWidget_SpltDetails").kendoGrid({
                                rowTemplate: kendo.template($("#RevenuWidgetSpltHeaderTemplate").html()),
                                dataSource: t.RevenueHeader,
                                scrollable: false,
                                scrollable: false,
                                pagable: true
                            });
                            $("#RevenueWidget_SpltDetails").removeClass("k-selectable")
                        } else {
                            $("#revenueTitle").show();
                            $("#RevenuWidgetHeader").hide();
                            $("#RevenueWidget_SpltDetails").hide()
                        }
                    },
                    error: function (e) {
                        $("#RevenueWidgetExport").html("");
                        $("#Revenue_DateNotes").hide();
                        t.error(e)
                    }
                })
            }
        }
    });
    u.fetch(function () {
        console.log(u.view().length)
    });
    var a = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2/api/Activity/getActivity?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.Activity.length != 0) {
                            $("#activityWidgetinner").kendoGrid({
                                rowTemplate: kendo.template($("#activityWidgetTemplate").html()),
                                dataSource: e.Activity,
                                scrollable: false,
                                pagable: true
                            });
                            $("#activityWidgetinner").show()
                        } else {
                            $("#activityWidgetinner").hide()
                        }
                    },
                    error: function (e) {
                        t.error(e);
                        $("#activityWidgetinner").hide()
                    }
                })
            }
        }
    });
    a.fetch(function () {
        console.log(a.view().length)
    });
    $("#WebIdMachList").hide();
    $("#bt_summary").hide();
    $("#EqMachineList").hide();
    var f = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/api/Equipment/getEquipment?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.Equipment.length > 0) {
                            $("#bt_list_disabled").hide();
                            $("#bt_list").show();
                            $("#bt_Inter").prop("disabled", false);
                            if (e.OtherEquipment.length == 0) {
                                $("#dvEquipment_OtherMachines").hide();
                                $("#dv_Equipment").show();
                                $("#Equipment").kendoGrid({
                                    rowTemplate: kendo.template($("#EquipmentTemplate").html()),
                                    dataSource: e.Equipment,
                                    scrollable: false,
                                    pagable: true
                                });
                                $("th").removeClass("k-header")
                            } else {
                                $("#TwoTables").attr("value", "Yes");
                                $("#dv_Equipment").hide();
                                $("#dvEquipment_OtherMachines").show();
                                $("#Equipment1").kendoGrid({
                                    rowTemplate: kendo.template($("#EquipmentTemplate").html()),
                                    dataSource: e.Equipment,
                                    scrollable: false,
                                    pagable: true
                                });
                                $("#Equipment_OtherMachines").kendoGrid({
                                    rowTemplate: kendo.template($("#EquipmentOthermachineTemplate").html()),
                                    dataSource: e.OtherEquipment,
                                    scrollable: false,
                                    pagable: true
                                });
                                $("th").removeClass("k-header")
                            }
                        } else {
                            $("#dv_Equipment").hide();
                            $("#dvEquipment_OtherMachines").hide();
                            $("#bt_Inter").prop("disabled", true);
                            $("#bt_list_disabled").show();
                            $("#bt_list").hide()
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    }
                })
            }
        }
    });
    f.fetch(function () {
        console.log(f.view().length)
    });
    var l = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/api/Service/getService?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.maxDays > 365) document.getElementById("ServiceHeader").innerHTML = "Service/Repair Summary (Service History : 12+ months)";
                        else if (e.maxDays < 60) document.getElementById("ServiceHeader").innerHTML = "Service/Repair Summary (Service History : 1 month)";
                        else document.getElementById("ServiceHeader").innerHTML = "Service/Repair Summary (Service History : " + Math.floor(e.maxDays / 30) + " months)";
                        serviceMaxDays = e.maxDays;
                        $("#Service").kendoGrid({
                            rowTemplate: kendo.template($("#ServiceTemplate").html()),
                            dataSource: e.ServiceSummary,
                            scrollable: false,
                            pagable: true
                        });
                        $("th").removeClass("k-header");
                        $("#ServicePie").hide();
                        loadServiceDetails("90")
                    },
                    error: function (e) {
                        t.error(e)
                    },
                    oncomplete: function (e) {
                        $("#" + currentYear + "ExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#" + currentYear + "RExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#" + currentYear + "ReExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#" + currentYear + "CExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#RevenueDetails_" + currentYear).show();
                        $("#RevenueDetails_" + currentYear + "_Rental").show();
                        $("#RevenueSpltDetails_" + currentYear).show();
                        var t = $("#Revenue_DateNotes_" + currentYear + " tbody");
                        if (t.children().length != 0) {
                            $("#RevenueSpltNotes_" + currentYear).show()
                        } else {
                            $("#RevenueSpltNotes_" + currentYear).hide()
                        }
                    }
                })
            }
        }
    });
    l.fetch(function () {
        console.log(l.view().length)
    });
    var c = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/api/LMSurvey/getLMSurvey?ULN=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        $("#LMSurvey").kendoGrid({
                            rowTemplate: kendo.template($("#LMSurveyWidgetTemplate").html()),
                            dataSource: e.LMSurveyCount,
                            scrollable: false,
                            pagable: true
                        });
                        $("#LMSurveyDetails").kendoGrid({
                            rowTemplate: kendo.template($("#LMSurveyDetailsTemplate").html()),
                            dataSource: e.LMSurveyDet,
                            scrollable: false,
                            pagable: true
                        });
                        $("#LMSurveyDetails1").kendoGrid({
                            rowTemplate: kendo.template($("#LMSurveyDetailsTemplate").html()),
                            dataSource: e.LMSurveyDet1,
                            scrollable: false,
                            pagable: true
                        });
                        $("#LMSurveyDetails2").kendoGrid({
                            rowTemplate: kendo.template($("#LMSurveyDetailsTemplate").html()),
                            dataSource: e.LMSurveyDet2,
                            scrollable: false,
                            pagable: true
                        });
                        if (e.LMSurveyDet == "") $("#bt_0").attr("disabled", "disabled");
                        if (e.LMSurveyDet1 == "") $("#bt_1").attr("disabled", "disabled");
                        if (e.LMSurveyDet2 == "") $("#bt_2").attr("disabled", "disabled");
                        $("th").removeClass("k-header")
                    },
                    error: function (e) {
                        t.error(e)
                    }
                })
            }
        }
    });
    c.fetch(function () {
        console.log(c.view().length)
    });
    $("th").removeClass("k-header")
}
function revenuedetailsdata(e, t, n, r) {
    $.ajax({
        url: "/eledger2/api/Revenue/getCommercial?ULN=" + t + "&splitRevenueNotes=" + n + "&showRecords=" + r,
        dataType: "json",
        type: "POST",
        success: function (t) {
            if (t.Commercial.length > 0) {
                revenuedatasource = t;
                revenuedetails(e)
            }
        }
    })
}
function revenuedetails(e) {
    revenueyear = e;
    $("#" + currentYear + "ExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
    $("#" + currentYear + "RExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
    $("#" + currentYear + "ReExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
    $("#" + currentYear + "CExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
    if (revenuedatasource != "") {
        $("#Revenue_Pinned").kendoGrid({
            rowTemplate: kendo.template($("#RevenuWidgetPinnedNotesTemplate").html()),
            dataSource: revenuedatasource.Commercial,
            scrollable: false,
            pagable: true
        });
        $("#Revenue_SplitPinned").kendoGrid({
            rowTemplate: kendo.template($("#RevenuWidgetPinnedNotesTemplate").html()),
            dataSource: revenuedatasource.Commercial,
            scrollable: false,
            pagable: true,
            group: true
        });
        $('#Revenue_Pinned').css('width', '100%');
        $("#Revenue_" + e).kendoGrid({
            rowTemplate: kendo.template($("#RevenueWidgetCommercialDetailsTemplate").html()),
            dataSource: {
                data: revenuedatasource.Commercial,
                filter: {
                    logic: "and",
                    filters: [{
                        field: "DisplayYear_Fmt",
                        operator: "eq",
                        value: e
                    }, {
                        field: "RecordType",
                        operator: "neq",
                        value: "R"
                    }]
                }
            },
            scrollable: false,
            pagable: true,
            selectable: "row",
            scrollable: false,
            pagable: true,
            group: true
        });
        $("#Revenue_" + e + "_Rental").kendoGrid({
            rowTemplate: kendo.template($("#RevenueWidgetCommercialDetailsTemplate").html()),
            dataSource: {
                data: revenuedatasource.Commercial,
                filter: {
                    logic: "and",
                    filters: [{
                        field: "DisplayYear_Fmt",
                        operator: "eq",
                        value: e
                    }, {
                        field: "RecordType",
                        operator: "eq",
                        value: "R"
                    }]
                }
            },
            scrollable: false,
            pagable: true,
            selectable: "row",
            scrollable: false,
            pagable: true,
            group: true
        });
        $("#RevenueSplt_" + e).kendoGrid({
            rowTemplate: kendo.template($("#RevenueWidgetSpltDetailsTemplate").html()),
            dataSource: {
                data: revenuedatasource.Commercial,
                filter: {
                    logic: "and",
                    filters: [{
                        field: "DisplayYear_Fmt",
                        operator: "eq",
                        value: e
                    }, {
                        field: "RecordType",
                        operator: "neq",
                        value: "R"
                    }]
                }
            },
            scrollable: false,
            pagable: true
        });
        $("#RevenueSplt_" + e + "_Rental").kendoGrid({
            rowTemplate: kendo.template($("#RevenueWidgetSpltDetailsTemplate").html()),
            dataSource: {
                data: revenuedatasource.Commercial,
                filter: {
                    logic: "and",
                    filters: [{
                        field: "DisplayYear_Fmt",
                        operator: "eq",
                        value: e
                    }, {
                        field: "RecordType",
                        operator: "eq",
                        value: "R"
                    }]
                }
            },
            scrollable: false,
            pagable: true
        });
        $("div").removeClass("k-grid k-widget k-secondary");
        $("#Revenue_DateNotes_" + e).kendoGrid({
            rowTemplate: kendo.template($("#RevenuWidgetDateNotesTemplate").html()),
            dataSource: {
                data: revenuedatasource.Commercial,
                filter: {
                    filters: [{
                        field: "RecordType",
                        operator: "eq",
                        value: "N"
                    }, {
                        field: "RecordType",
                        operator: "eq",
                        value: "L"
                    }, {
                        field: "DisplayYear_Fmt",
                        operator: "eq",
                        value: e
                    }]
                }
            },
            scrollable: false,
            pagable: true,
            group: true
        })
    }
    $("div").parent().removeClass("k-grid k-widget k-secondary");
    $("#RevenueWidgetExport").show();
    $("#RevenueWidgetloading").hide()
}
function widgetCompleted(e) {
    if (e == "Location") {
        $("#Slider").removeClass("open");
        $("#Slider").css("right", "-277px");
        $("#dvLoading").hide();
        $("#LocationWidget").show();
        $(".k-loading-mask").remove();
        $("#" + currentYear + "ExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
        $("#" + currentYear + "RExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
        $("#" + currentYear + "ReExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
        $("#" + currentYear + "CExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
        $("#RevenueDetails_" + currentYear).show();
        $("#RevenueDetails_" + currentYear + "_Rental").show();
        $("#RevenueSpltDetails_" + currentYear + "_Rental").show();
        $("#RevenueSpltDetails_" + currentYear).show();
        var t = $("#Revenue_DateNotes_" + currentYear + " tbody");
        if (t.children().length != 0) {
            $("#RevenueSpltNotes_" + currentYear).show()
        } else {
            $("#RevenueSpltNotes_" + currentYear).hide()
        }
    } else {
        $("#Slider").removeClass("open");
        $("#Slider").css("right", "-277px");
        $("#LocationWidget").hide();
        $("#dvLoading").hide();
        $("#ContactWidget").show();
        $(".k-loading-mask").remove()
    }
}
function ExportHide(e) {
    if (e == 4 || e == 1) {
        $("#btnserviceExport").show();
        $("#btnrevenueExport").show();
        $("#btnrevenueExport_split").show()
    } else if (e == 2) {
        $("#btnserviceExport").show();
        $("#btnrevenueExport").hide();
        $("#btnrevenueExport_split").hide()
    } else if (e == 3) {
        $("#btnserviceExport").hide();
        $("#btnrevenueExport").show();
        $("#btnrevenueExport_split").show()
    }
}
function queryHelpPage() {
    window.open("/Start/queryHelp/", "SearchInstructions", "height=600,width=600,status=no,scrollbars=no,resizable=yes,titlebar=no,menubar=no,left=10,top=10")
}
function getsplitNotes(e) {
    if (e == "show") {
        $("#RevenueSpltDetails_" + currentYear).show();
        var r = $("#Revenue_DateNotes_" + currentYear + " tbody");
        if (r.children().length != 0) {
            $("#RevenueSpltNotes_" + currentYear).show()
        } else {
            $("#RevenueSpltNotes_" + currentYear).hide()
        }
        $("#RevenuWidgetHeader").hide();
        $("#Revenue_DateNotes").show();
        $("#RevenueSpltNoteDet").show()
    } else {
        $("#RevenuWidgetHeader").show();
        $("#Revenue_DateNotes").hide();
        $("#RevenueSpltNoteDet").hide()
    }
}
function getRevenueDetails(e, t, n) {
    $("#RevenueWidgetExport").hide();
    $("#RevenueWidgetloading").show();
    setTimeout(function () {
        if (e == "") {
            e = $("#hd_Uln").val();
            e = ReplaceformatUln(e)
        }
        if ($("#showall").attr("title") == "Show All Records" || $("#Spltshowall").attr("title") == "Show All Records") {
            $("#showall").text("Show Recent Record");
            $("#showall").attr("title", "Show Recent Record");
            $("#Spltshowall").text("Show Recent Record ");
            $("#Spltshowall").attr("title", "Show Recent Record")
        } else {
            $("#showall").text("Show All Records");
            $("#showall").attr("title", "Show All Records");
            $("#Spltshowall").text("Show All Records");
            $("#Spltshowall").attr("title", "Show All Records");
            n = ""
        }
        $.ajax({
            url: "/api/Revenue/getRevenue?ULN=" + e + "&splitRevenueNotes=" + t + "&showRecords=" + n,
            dataType: "json",
            type: "POST",
            success: function (r) {
                if (r.RevenueHeader.length > 0) {
                    Revenueheaders = r.RevenueHeader;
                    $("#showall").show();
                    if (r.RevenueHeader[0].DisplayYear != "Pinned Notes") revenueyear = r.RevenueHeader[0].DisplayYear;
                    else revenueyear = r.RevenueHeader[1].DisplayYear;
                    $("#RevenuWidgetHeader").kendoGrid({
                        rowTemplate: kendo.template($("#RevenuWidgetHeaderTemplate").html()),
                        dataSource: r.RevenueHeader,
                        scrollable: false,
                        pagable: true
                    });
                    $("#RevenueWidget_SpltDetails").kendoGrid({
                        rowTemplate: kendo.template($("#RevenuWidgetSpltHeaderTemplate").html()),
                        dataSource: r.RevenueHeader,
                        scrollable: false,
                        pagable: true
                    });
                    revenuedetailsdata(revenueyear, e, t, n);
                    $("#RevenueDetails_" + revenueyear).show();
                    $("#RevenueDetails_" + revenueyear + "_Rental").show();
                    $("#RevenueSpltDetails_" + revenueyear).show();
                    $("#RevenueSpltDetails_" + revenueyear + "_Rental").show();
                    $("#RevenueSpltNotes_" + revenueyear).show();
                    var r = $("#Revenue_DateNotes_" + n + " tbody");
                    if (r.children().length != 0) {
                        $("#RevenueSpltNotes_" + n).show()
                    } else {
                        $("#RevenueSpltNotes_" + n).hide()
                    }
                } else {
                    $("#revenueTitle").show();
                    $("#RevenuWidgetHeader").hide();
                    $("#RevenueWidget_SpltDetails").hide();
                    $("#RevenueWidgetExport").hide()
                }
            },
            error: function (e) {
                $("#RevenueWidgetExport").html("");
                $("#Revenue_DateNotes").hide();
                options.error(e)
            }
        })
    }, 2000);
}
function showSalesTeam() {
    $("td").removeAttr("background-color", "#CCCCCC");
    document.getElementById("salesPopup").style.visibility = document.getElementById("salesPopup").style.visibility == "visible" ? "hidden" : "visible"
}
function showAgreeSalesTeam() {
    $("td").removeAttr("background-color", "#CCCCCC");
    document.getElementById("salesAgreePopup").style.visibility = document.getElementById("salesAgreePopup").style.visibility == "visible" ? "hidden" : "visible"
}
function AS400PDFPage(e) {
    $("#render").html('<img src="/eledger2/Content/eledger2/Default/loading-image.gif" />');
    var t = "";
    AS400appPath = "";
    t = AS400+"/eledger2/AS400PDFPage/getas400pdf?uln=" + e;
    $.get(t, function (e) {
        var t = "<img src='" + "data:image/jpg;base64," + e + "'/>";
        $("#render").html(t)
    });
    $("#as400").modal("show")
}
function getRevenueAmtSplit(e, t) {
    if (t == "RevenueDetails") {
        $("#revenueAmtspltcoin-" + e).toggle()
    } else {
        $("#RevenueSpltDetailsCoin-" + e).toggle()
    }
}
function togglerevenuedetails(e, t, n) {
    if (e.indexOf("RevenueDetails_") != 0 || e.indexOf("RevenueSpltDetails_") != 0) {
        $("#" + e).toggle();
        var r = $("#Revenue_DateNotes_" + revenueyear + " tbody");
        if (r.children().length != 0) {
            $("#RevenueSpltNotes_" + n).toggle()
        } else {
            $("#RevenueSpltNotes_" + n).toggle()
        }
    } else {
        $("#RevenueDetails_" + n).toggle();
        $("#RevenueSpltDetails_" + n).toggle();
        var r = $("#Revenue_DateNotes_" + n + " tbody");
        if (r.children().length != 0) {
            $("#RevenueSpltNotes_" + n).toggle()
        } else {
            $("#RevenueSpltNotes_" + n).toggle()
        }
    }
    revenuedetails(n);

    if (n.indexOf('C') != -1) {
        if ($("#" + n + "ExpCol").attr("class") != undefined && $("#" + n + "ExpCol").hasClass("k-icon k-i-collapse") || $("#" + n + "ExpCol").hasClass("k-i-collapse k-icon") || $("#" + n + "CExpCol").attr("class") != undefined && $("#" + n + "CExpCol").hasClass("k-icon k-i-collapse")) {
            $("#" + n + "ExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
            $("#" + n + "CExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
        }
        else {
            $("#" + n + "ExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
            $("#" + n + "CExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
        }
    }
        else {
        if ($("#" + n + "RExpCol").attr("class") != undefined && $("#" + n + "RExpCol").hasClass("k-icon k-i-collapse") || $("#" + n + "ReExpCol").attr("class") != undefined && $("#" + n + "ReExpCol").hasClass("k-icon k-i-collapse")) {
            $("#" + n + "RExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
            $("#" + n + "ReExpCol").removeClass("k-icon k-i-collapse").addClass("k-icon k-i-expand");
        }
        else {
            $("#" + n + "RExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
            $("#" + n + "ReExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
        }
    }   
}
function getRevenueAmtSplt(e, t) {
    var n = "";
    var r = $("#revenueamtsplt_" + t + " tbody");
    if (r.children().length == 0) {
        var i = new kendo.data.DataSource({
            transport: {
                read: function (r) {
                    $.ajax({
                        url: "/eledger2/api/Revenue/getRevenueamtSplt?Id=" + e,
                        dataType: "json",
                        type: "POST",
                        success: function (e) {
                            $(".revenueamtsplt").remove();
                            if (e.RevenueCardSplit.length > 0) {
                                $.each(e.RevenueCardSplit, function () {
                                    n = n + "<tr><td style='width:15.5%;text-align:right' class='lable'>" + this.MerchantType + "</td><td>" + this.Amt_Fmt + "</td>"
                                })
                            }
                            $("#Card-" + t).after("<tr id='RevenuecardAmtSplit_" + t + "'><td colspan='6'><div><table id='revenueamtsplt_" + t + "'>" + n + "</table></div></td></tr>")
                        },
                        error: function (e) {
                            r.error(e)
                        },
                        oncomplete: function (e) { }
                    })
                }
            }
        });
        i.fetch(function () {
            console.log(i.view().length)
        })
    } else {
        $("#RevenuecardAmtSplit_" + t).toggle()
    }
}
function loadServiceDetails(e) {
    $("#serviceloadimage").show();
    $("#hd_dateWindow").val(e);
    ULN = $("#hd_Uln").val();
    ULN = ReplaceformatUln(ULN);
    if (serviceMaxDays >= 730) {
        $("#24M").prop("disabled", false);
        $("#12M").prop("disabled", false);
        $("#3M").prop("disabled", false)
    } else if (serviceMaxDays >= 365) {
        $("#24M").prop("disabled", true);
        $("#12M").prop("disabled", false);
        $("#3M").prop("disabled", false)
    } else {
        $("#24M").prop("disabled", true);
        $("#12M").prop("disabled", true);
        $("#3M").prop("disabled", false)
    }
    if (e == "90") {
        document.getElementById("CallListHeader").innerHTML = "Call List (Showing Last 3 Months)";
        $("#3M").prop("disabled", true);
        $("#3M").attr("title", "now showing all calls within the past 3 months");
        $("#12M").attr("title", "show all calls within the past 12 months");
        $("#24M").attr("title", "show all calls within the past 24 months")
    } else if (e == "365") {
        document.getElementById("CallListHeader").innerHTML = "Call List (Showing Last 12 Months)";
        $("#12M").prop("disabled", true);
        $("#3M").attr("title", "show all calls within the past 3 months");
        $("#24M").attr("title", "show all calls within the past 24 months");
        $("#12M").attr("title", "now showing all calls within the past 12 months")
    } else if (e == "730") {
        document.getElementById("CallListHeader").innerHTML = "Call List (Showing Last 24 Months)";
        $("#24M").prop("disabled", true);
        $("#3M").attr("title", "show all calls within the past 3 months");
        $("#12M").attr("title", "show all calls within the past 12 months");
        $("#24M").attr("title", "now showing all calls within the past 24 months")
    } else document.getElementById("CallListHeader").innerHTML = "Call List";
    $("#btMerge").attr("value", "Merge");
    $("#btMerge").attr("title", "merge call categories");
    document.getElementById("btMerge").innerHTML = '<i class="icon-cog"></i>Merge Calls';
    var t = new kendo.data.DataSource({
        transport: {
            read: function (t) {
                $.ajax({
                    url: "/eledger2api/Service/getServiceDetails?ULN=" + ULN + "&dateWindow=" + e,
                    dataType: "json",
                    type: "POST",
                    success: function (e) {
                        if (e.ServiceCategories.length > 0) {
                            $("#ServiceCategories").kendoGrid({
                                rowTemplate: kendo.template($("#ServiceCategoriesTemplate").html()),
                                dataSource: e.ServiceCategories,
                                scrollable: false,
                                pagable: true
                            });
                            $("#ServicePie").kendoChart({
                                theme: $(document).data("kendoSkin") || "default",
                                title: {
                                    text: "Service Category Breakdown"
                                },
                                legend: {
                                    position: "bottom"
                                },
                                dataSource: {
                                    data: e.Servicecategorypie
                                },
                                seriesDefaults: {
                                    labels: {
                                        visible: true,
                                        template: "${ value }%"
                                    }
                                },
                                series: [{
                                    type: "pie",
                                    field: "series1",
                                    categoryField: "category",
                                    explodeField: "explode"
                                }],
                                tooltip: {
                                    visible: false,
                                    template: "${ category } - ${ value }%"
                                }
                            });
                            $("#bt_PieChart").prop("disabled", false);
                            $("#ServiceCategories").show()
                        } else {
                            $("#bt_PieChart").prop("disabled", true);
                            $("#ServiceCategories").hide();
                            $("#ServicePie").hide();
                            $("#serviceloadimage").hide()
                        }
                        $("#dvServiceDetails").show();
                        if (e.PriceChangesTotal != "0") {
                            $("#dvPriceChanges tbody").remove();
                            $("#dvPriceChanges .k-grid-pager").remove();
                            $("#dvPriceChanges").show();
                            $("#ServicePriceChanges").kendoGrid({
                                rowTemplate: kendo.template($("#ServiceDetailsTemplate").html()),
                                dataSource: {
                                    data: e.PriceChanges,
                                    total: e.PriceChangesTotal,
                                    pageSize: 15
                                },
                                scrollable: false,
                                sortable: false,
                                filterable: false,
                                pageable: {
                                    input: true,
                                    numeric: false
                                }
                            })
                        } else {
                            $("#dvPriceChanges").hide();
                            $("#serviceloadimage").hide()
                        }
                        if (e.ServiceRepairTotal != "0") {
                            $("#btnserviceExport").prop("disabled", false);
                            $("#dvServiceRepair tbody").remove();
                            $("#dvServiceRepair .k-grid-pager").remove();
                            $("#serviceloadimage").hide();
                            $("#dvServiceRepair").show();
                            $("#tblServiceRepair").kendoGrid({
                                rowTemplate: kendo.template($("#ServiceDetailsTemplate").html()),
                                dataSource: {
                                    data: e.ServiceRepair,
                                    total: e.ServiceRepairTotal,
                                    pageSize: 15
                                },
                                scrollable: false,
                                sortable: false,
                                filterable: false,
                                pageable: {
                                    input: true,
                                    numeric: false
                                }
                            })
                        } else {
                            $("#dvServiceRepair").hide();
                            $("#btnserviceExport").prop("disabled", true);
                            $("#hd_dateWindow").val('0');
                            $("#serviceloadimage").hide()
                        }
                        if (e.MaintenanceTotal != "0") {
                            $("#dvMaintenance tbody").remove();
                            $("#dvMaintenance .k-grid-pager").remove();
                            $("#serviceloadimage").hide();
                            $("#dvMaintenance").show();
                            $("#ServiceMaintenance").kendoGrid({
                                rowTemplate: kendo.template($("#ServiceDetailsTemplate").html()),
                                dataSource: {
                                    data: e.Maintenance,
                                    total: e.MaintenanceTotal,
                                    pageSize: 15
                                },
                                scrollable: false,
                                sortable: false,
                                filterable: false,
                                pageable: {
                                    input: true,
                                    numeric: false
                                }
                            })
                        } else {
                            $("#dvMaintenance").hide();
                            $("#serviceloadimage").hide()
                        }
                        if (e.MachineMovementTotal != "0") {
                            $("#dvMachineMovement tbody").remove();
                            $("#dvMachineMovement .k-grid-pager").remove();
                            $("#serviceloadimage").hide();
                            $("#dvMachineMovement").show();
                            $("#ServiceMachineMovement").kendoGrid({
                                rowTemplate: kendo.template($("#ServiceMachineMovementTemplate").html()),
                                dataSource: {
                                    data: e.MachineMovement,
                                    total: e.MachineMovementTotal,
                                    pageSize: 15
                                },
                                scrollable: false,
                                sortable: false,
                                filterable: false,
                                pageable: {
                                    input: true,
                                    numeric: false
                                }
                            })
                        } else {
                            $("#dvMachineMovement").hide();
                            $("#serviceloadimage").hide()
                        }
                        if (e.ServiceAllTotal != "0") {
                            $("#dvServiceAll tbody").remove();
                            $("#dvServiceAll .k-grid-pager").remove();
                            $("#serviceloadimage").hide();
                            $("#dvServiceAll").hide();
                            $("#tblServiceAll").kendoGrid({
                                rowTemplate: kendo.template($("#ServiceDetailsTemplate").html()),
                                dataSource: {
                                    data: e.ServiceAll,
                                    total: e.ServiceAllTotal,
                                    pageSize: 15
                                },
                                scrollable: false,
                                sortable: false,
                                filterable: false,
                                pageable: {
                                    input: true,
                                    numeric: false
                                }
                            })
                        } else {
                            //$("#btMerge").attr("disabled", "disabled");
                            $("#serviceloadimage").hide()
                        }
                    },
                    error: function (e) {
                        t.error(e)
                    },
                    oncomplete: function (e) {
                        $("thead").removeClass("k-grid-header");
                        $("#" + currentYear + "ExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#" + currentYear + "RExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#" + currentYear + "ReExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#" + currentYear + "CExpCol").removeClass("k-icon k-i-expand").addClass("k-icon k-i-collapse");
                        $("#RevenueDetails_" + currentYear).show();
                        $("#RevenueDetails_" + currentYear + "_Rental").show();
                        $("#RevenueSpltDetails_" + currentYear).show();
                        $("#RevenueSpltDetails_" + currentYear + "_Rental").show();
                        var t = $("#Revenue_DateNotes_" + n + " tbody");
                        if (t.children().length != 0) {
                            $("#RevenueSpltNotes_" + n).show()
                        } else {
                            $("#RevenueSpltNotes_" + n).hide()
                        }
                    }
                })
            }
        }
    });
    t.fetch(function () {
        console.log(t.view().length)
    })
}
function toggleServiceAll() {
    var e = $("#btMerge").attr("value");
    if (e == "Merge") {
        $("#btMerge").attr("value", "Split");
        $("#btMerge").attr("title", "split call categories");
        document.getElementById("btMerge").innerHTML = '<i class="icon-cog"></i>Split Calls'
    } else {
        $("#btMerge").attr("value", "Merge");
        $("#btMerge").attr("title", "merge call categories");
        document.getElementById("btMerge").innerHTML = '<i class="icon-cog"></i>Merge Calls'
    }
    $("#dvServiceAll").toggle();
    $("#dvServiceDetails").toggle()
}
function getServiceDet(e, t) {
    var n = e + t;
    $("#" + n).toggle();
    var r = $("#tbl" + n + " tbody");
    if (r.children().length == 0) {
        callurl = "/eledger2api/Service/getServiceCallIDDetails/?serviceCallId=" + t + "&tblID=" + e;
        $("#tbl" + n).kendoGrid({
            rowTemplate: kendo.template($("#ServiceCallIDDetilsTemplate").html()),
            dataSource: {
                transport: {
                    read: {
                        url: callurl,
                        dataType: "json",
                        type: "POST"
                    }
                },
                schema: {
                    data: "ServiceCallIDDetils"
                }
            },
            scrollable: false,
            pagable: true
        })
    }
}
function getservicemachinelistbywebid(e, t, n) {
    var r = e + t + n;
    $("#" + r).toggle();
    $("#tbl" + r + " tbody").html('<tr><td style="height:64px;text-align:center;" colspan="8"></td></tr>');
    $("#hd_Uln").val(ReplaceformatUln($("#hd_Uln").val()));
    ULN = $("#hd_Uln").val();
    var i = $("#tbl" + r + " tbody");
    $("#tbl" + r).kendoGrid({
        rowTemplate: kendo.template($("#ServiceDetilsWebIDTemplate").html()),
        dataSource: {
            transport: {
                read: {
                    url: "/eledger2api/Service/getmachinelistbywebid/?uln=" + ULN + "&webid=" + e,
                    dataType: "json",
                    type: "POST"
                }
            },
            schema: {
                data: "MachineList"
            }
        },
        scrollable: false,
        pagable: true
    })
}
function toggleDistance(e, t) {
    var n = $("#" + t).attr("value");
    if (n == "Show") {
        $("#" + t).attr("value", "Hide");
        document.getElementById(t).innerHTML = '<i class="icon-cog"></i>Hide'
    } else {
        $("#" + t).attr("value", "Show");
        document.getElementById(t).innerHTML = '<i class="icon-cog"></i>Show'
    }
    $("#" + e + t).toggle()
}
function interactiveView() {
    ULN = $("#hd_Uln").val();
    ULN = ReplaceformatUln(ULN);
    var e = "/eledger2/api/Equipment/LocInventory?ULN=" + ULN;
    var t;
    $.ajax({
        url: e,
        dataType: "json",
        success: function (e) {
            $.each(e.LocInventoryURL, function () {
                window.open(this["URL"], "InventoryLookup", "height=590,width=915,status=no,scrollbars=yes,resizable=yes,titlebar=no,location=yes,menubar=no,left=10,top=10")
            })
        },
        error: function (e) {
            options.error(e)
        },
        complete: function (e) { }
    })
}
function viewRefunds() {
    ULN = $("#hd_Uln").val();
    ULN = ReplaceformatUln(ULN);
    var e = "/eledger2/api/Service/viewRefunds?ULN=" + ULN;
    var t;
    $.ajax({
        url: e,
        dataType: "json",
        success: function (e) {
            $.each(e.viewRefundURL, function () {
                window.open(this["URL"], "_blank")
            })
        },
        error: function (e) {
            options.error(e)
        },
        complete: function (e) { }
    })
}
var eqdsTotalLaundryMachine = '';
var eqdsTotalMachine = '';

function EqGridDetails(e) {
    ULN = $("#hd_Uln").val();
    ULN = ReplaceformatUln(ULN);
    if (e == "list") {
        var t = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    $.ajax({
                        url: "/api/Equipment/getmachinelist?ULN=" + ULN,
                        dataType: "json",
                        success: function (e) {
                            eqdsTotalLaundryMachine = e.TotalLaundryMachine;
                            eqdsTotalMachine = e.TotalMachine
                        },
                        error: function (t) {
                            e.error(t)
                        }
                    })
                }
            }
        });
        t.fetch(function () {
            console.log(t.view().length)
        })
    }
}
function EqGridDetail(e) {
    if (e == "list") {
        if (eqdsTotalLaundryMachine.length > 0) {
            if (eqdsTotalMachine.length > 0) {
                $("#TotalMachine").kendoGrid({
                    rowTemplate: kendo.template($("#TotalMachineTemplate").html()),
                    dataSource: {
                        data: eqdsTotalMachine,
                        group: {
                            field: "RoomID"
                        }
                    },
                    columns: [{
                        field: "RoomID",
                        groupHeaderTemplate: "<table style='width:100%' id='Room${value}'></table>"
                    }],
                    scrollable: false,
                    pagable: true
                })
            }
        } else {
            $("#EqMachineList").hide()
        }
        $("#bt_list").hide();
        $("#bt_list_disabled").show();
        $("#EqMachineList").show();
        $("#dv_Equipment").hide();
        $("#dvEquipment_OtherMachines").hide();
        $("#WebIdMachList").hide();
        $("#EqMachineListbody").html('');
        $('.k-grouping-row').closest('tr').find('td').removeAttr('colspan');
        $('.k-grouping-row').closest('tr').find('td').attr('colspan', '20');
        $('.k-grouping-row').closest('tr').find('td').attr('style', 'width:100%');        
        $('p').remove();
        $.each(eqdsTotalLaundryMachine, function () {
            $('#Room' + this["RoomID"]).html('<tr><td colspan="13" style="width: 613px; font-weight:bold; background-color: #677C97; color: #efefef"><span>' + this["RoomID"] + '<span>:</span></span><span>' + this["Description"] + '</span></td><td class="dL" style="background-color: #DEEEFF">Size:</td><td style="background-color: white">' + this["Size"] + '</td><td class="dL" style="background-color: #DEEEFF">Installers:</td><td style="background-color: white"><span>' + this["NumberofInstallers"] + '</span></td></tr><tr><td style="border: 0; padding: 0;" colspan="19"><table width="100%" border="0" style="margin: 0;"><tbody><tr><td class="dL" style="background-color: #DEEEFF;width:8%"><nobr>LR Notes:</nobr></td><td style="background-color: white"><b><span>' + this["SpecialInstruction"] + '</span></b></td></tr></tbody></table></td></tr>')
        });
        $("#EqMachineList").show();
        $('.k-grouping-row').after('<tr style="background-color: #677C97;color: #efefef"><td colspan="4" >&nbsp;</td><td style="text-align:center"  colspan="2">Purchase</td><td style="text-align: center" colspan="4">Install</td><td style="text-align: center" colspan="2">Count As</td><td rowspan="2" style="text-align: center; width: 4%" title="Meter Type">Meter Type</td><td rowspan="2" style="text-align: center; width: 4%" title="Sleeved?">Sleeved</td><td style="text-align: center; width: 5%" rowspan="2">Vend Method</td><td style="text-align: center; width: 8%" rowspan="2">Fuel/Load</td><td style="text-align: center" rowspan="2">Price/<br />Time</td></tr><tr style="background-color: #677C97;color: #efefef"><td style="text-align: center; width: 8%">WebId</td><td style="text-align: center; width: 8%">Type</td><td style="text-align: center; width: 8%">Mfg</td><td style="text-align: center; width: 11%">Model</td><td style="text-align: center; width: 5%">Date</td><td style="text-align: center; width: 5%" title="Age when purchased">Age</td><td style="text-align: center; width: 5%">Date</td><td style="text-align: center; width: 4%" title="Time on loc since most recent install">On Loc</td><td style="text-align: center; width: 6%" title="Condition at most recent install">Cond</td><td style="text-align: center; width: 4%" title="Age since very first install">Age</td><td style="text-align: center; width: 4%">W</td><td style="text-align: center; width: 4%">D</td></tr>');
        $('tr').removeClass('k-grouping-row')
    } else if (e == "summary") {
        $("#bt_list").show();
        $("#bt_list_disabled").hide();
        $("#bt_summary").hide();
        if ($("#TwoTables").val() == "Yes") {
            $("#EqMachineList").hide();
            $("#dvEquipment_OtherMachines").show();
            $("#WebIdMachList").hide()
        } else {
            $("#EqMachineList").hide();
            $("#dv_Equipment").show();
            $("#WebIdMachList").hide()
        }
    }
    $("#bt_list_disabled").hide();
    $("#bt_summary").show()
}
function getmachinelistbywebid(e, t) {
    $("#EqMachineList").hide();
    $("#WebIdMachList").show();
    $("#WebIdMachbody").html('<tr><td style="height:64px;text-align:center;" colspan="8"><img src="/eledger2/Content/eledger2/Default/loading-image.gif" /></td></tr>');
    ULN = $("#hd_Uln").val();
    ULN = ReplaceformatUln(ULN);
    $("#bt_list").show();
    $("#bt_summary").show();
    $("#bt_summary").attr("title", "show  equipment summary associated with " + ULN);
    $("#EquipmentWidget").focus();
    var n = new kendo.data.DataSource({
        transport: {
            read: function (n) {
                $.ajax({
                    url: "/api/Equipment/getmachinelistbywebid/?uln=" + t + "&webid=" + e,
                    dataType: "json",
                    success: function (e) {
                        if (e.MachineList.length > 0) {
                            $("#dv_Equipment").hide();
                            $("#dvEquipment_OtherMachines").hide();
                            $("#WebIdMach").kendoGrid({
                                rowTemplate: kendo.template($("#WebIdMachTemplate").html()),
                                dataSource: e.MachineList,
                                scrollable: false,
                                pagable: true
                            });
                            $("div").removeClass("k-grid k-widget")
                        } else {
                            $("#WebIdMachList").hide()
                        }
                        if (e.AttributeList.length > 0) {
                            $("#dv_Equipment").hide();
                            $("#dvEquipment_OtherMachines").hide();
                            $("#EqMachineList").hide();
                            $.each(e.AttributeList, function () {
                                $("#AttributeListrow").after("<tr><td>" + this["GPAttributeId"] + "</td><td colspan='3'>" + this["AttributeValue"] + "</td></tr>")
                            })
                        } else {
                            $("#WebIdMachList").hide()
                        }
                    },
                    error: function (e) {
                        n.error(e)
                    }
                });
                $("div").removeClass("k-grid k-widget")
            }
        }
    });
    n.fetch(function () {
        console.log(n.view().length)
    });
    $("div").removeClass("k-grid k-widget")
}
function getContactOverview(e, t) {
    $("#dvLoading").attr("style", "display:inline-block; width:100%; height:100%; text-align:center; vertical-align:top;");
    $("#LocationWidget").hide();
    $("#dvLocation-results").hide();
    $("#ContactWidget").hide();
    $("#contactOverviewWidget").hide();
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    scrollToWidget("Contacts");
    $.get("/Templates/contactOverviewTemplate.html", function (n) {
        $("body").append(n);
        var r = new kendo.data.DataSource({
            transport: {
                read: function (n) {
                    $.ajax({
                        url: "/eledger2/api/Contact/getContactdetails?Id=" + e + "&Type=" + t,
                        dataType: "json",
                        success: function (e) {
                            $("#dvLoading").hide();
                            $("#ContactWidget").show();
                            $("#contactOverviewWidget").show();
                            if (e.RelatedContacts.length > 0) {
                                $("#relatedContact").show();
                                $("#relatedContact").kendoGrid({
                                    rowTemplate: kendo.template($("#relatedContactTemplate").html()),
                                    dataSource: e.RelatedContacts,
                                    scrollable: false,
                                    pagable: true,
                                    selectable:false
                                });
                                $("div").removeClass("k-grid k-widget k-secondary")
                            }
                            $("#contactOverview").kendoGrid({
                                rowTemplate: kendo.template($("#contactOverviewTemplate").html()),
                                dataSource: e.ContactSummary,
                                scrollable: false,
                                pagable: true
                            });
                            $("div").removeClass("k-grid k-widget k-secondary");
                            $("#contactOverview1").kendoGrid({
                                rowTemplate: kendo.template($("#contactOverviewTemplate1").html()),
                                dataSource: e.ContactSummary,
                                scrollable: false,
                                pagable: true
                            });
                            if ($("#hdnActiveLoc").val() != "") {
                                $("#activeLoc").html();
                                $("#activeLoc").html($("#hdnActiveLoc").val())
                            }
                            $("div").removeClass("k-grid k-widget k-secondary");
                            if (e.ContactMaster.length > 0) {
                                $("#contactMasterSummary").kendoGrid({
                                    rowTemplate: kendo.template($("#contactMasterSummaryTemplate").html()),
                                    dataSource: e.ContactMaster,
                                    scrollable: false,
                                    pagable: true
                                });
                                $("#contactMasterSummary").show();
                                $("div").removeClass("k-grid k-widget k-secondary")
                            }
                            $.each(e.RelatedContacts, function () {
                                if (this["showmastercard"] == "true") {
                                    $("#masterCard").kendoGrid({
                                        rowTemplate: kendo.template($("#mastercardtemplate").html()),
                                        dataSource: e.ContactSummary,
                                        scrollable: false,
                                        pagable: true
                                    });
                                    $("#masterCard").show();
                                    $("div").removeClass("k-grid k-widget k-secondary")
                                }
                            });
                            $(document).attr("title", $("#contactPageTitle").val())
                        },
                        error: function (e) {
                            $("#contactOverviewWidget").show();
                            $("#dvLoading").hide();
                            $("#ContactWidget").show()
                        },
                        complete: function (e) {
                            widgetCompleted("Contact")
                        }
                    })
                }
            }
        });
        r.fetch(function () {
            console.log(r.view().length)
        })
    })
}
function cmsPage(e) {
    window.open(e, "CMS")
}
function LMSurveyLink(e) {
    $.ajax({
        url: "/api/LMSurvey/getLMSurveyLink?LocID=" + e,
        dataType: "json",
        success: function (e) {
            $.each(e.LMSurveyLink, function () {
                window.open(this["URL"], "LMSurvey", "height=590,width=915,status=no,scrollbars=yes,resizable=yes,titlebar=no,location=yes,menubar=no,left=10,top=10")
            })
        },
        error: function (e) { },
        complete: function (e) { }
    })
}
function createChart() {
    $("#ServicePie").toggle()
}
function getRevenueDetailDataExport(e) {
    var t = window.navigator.userAgent;
    var n = t.indexOf("MSIE ");
    var r = "";
    $.each(Revenueheaders, function (e, t) {
        if (this["DisplayYear"] != "Pinned Notes") {
            if ($("#RevenueDetails_" + this["DisplayYear"]).is(":visible") || $("#RevenueSpltDetails_" + this["DisplayYear"]).is(":visible")) if (r != "") r = r + "," + this["DisplayYear"];
            else r = r + this["DisplayYear"];
            if ($("#RevenueDetails_" + this["DisplayYear"] + "_Rental").is(":visible") || $("#RevenueSpltDetails_" + this["DisplayYear"] + "_Rental").is(":visible")) if (r != "") r = r + "," + this["DisplayYear"];
            else r = r + this["DisplayYear"]
        }
    });
    $("#hdn_Revenueyear").val('');
    $("#hdn_Revenueyear").val(r);
    var uln = $("#hd_Uln").val();
    uln = (uln.replace(/-/gi, ''));
    $("#fileExportRevenue").html("Revenue_" + uln);
    $("#exportExcelRevenue").modal("show")
}
function exportexcelRevenue() {
    var uln = $("#hd_Uln").val();
    uln = (uln.replace(/-/gi, ''));
    var url = "/eledger2/ExportToExcel/RevenueExportToExcel?uln=" + uln + "&year=" + $("#hdn_Revenueyear").val();
    var winX = window.open(url);
    $("#exportExcelRevenue").modal("hide")
}
function getCallListExportToExcel() {
    var uln = $("#hd_Uln").val();
    uln = (uln.replace(/-/gi, ''));
    $("#fileExportService").html("ServiceCalls_" + uln);
    $("#exportExcelService").modal("show")
}
function exportexcelservice() {
    var uln = $("#hd_Uln").val();
    uln = (uln.replace(/-/gi, ''));
    var dateWindow = $("#hd_dateWindow").val();
    var url = "/eledger2/ExportToExcel/ServiceExportToExcel?uln=" + uln + "&dateWindow=" + dateWindow;
    var winX = window.open(url);
    winX.close();
}
function tableToExcelIE(e) {
    var t = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        n = function (e) {
            return window.btoa(unescape(encodeURIComponent(e)))
        },
        r = function (e, t) {
            return e.replace(/{(\w+)}/g, function (e, n) {
                return t[n]
            })
        };
    var i = {
        worksheet: "Report" || "Worksheet",
        table: e
    };
    var s = function () {
        var e = new Blob([r(t, i)], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(e, "Report.xls")
    };
    s()
}
var Name, previousyear, Address, City, State, Zip, Machine, currentYear = (new Date).getFullYear(),
    Revenueheaders, Kendo = 0,
    ExportAccess = 0,
    ULN, Phone, Category, Id, LRDes; var revecount1; var revecount;
var Revenuedatasource, revenuedatasource, revenueyear;
var appPath = "";
var apiUrl = "/api/SearchLocationJson/";
var flagTemplateAppended = false;
var countWidgets = 0;
var searchcount = 0;
var serviceMaxDays = 0;
var tableToExcel = function () {
    var e = "data:application/vnd.ms-excel;base64,",
        t = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        n = function (e) {
            return window.btoa(unescape(encodeURIComponent(e)))
        },
        r = function (e, t) {
            return e.replace(/{(\w+)}/g, function (e, n) {
                return t[n]
            })
        };
    return function (i, s) {
        var o = {
            worksheet: s || "Worksheet",
            table: i
        };
        window.location.href = e + n(r(t, o))
    }
}();
var saveAs = saveAs || typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob && navigator.msSaveOrOpenBlob.bind(navigator) ||
function (e) {
    "use strict";
    if (typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
        return
    }
    var t = e.document,
        n = function () {
            return e.URL || e.webkitURL || e
        },
        r = e.URL || e.webkitURL || e,
        i = t.createElementNS("http://www.w3.org/1999/xhtml", "a"),
        s = !e.externalHost && "download" in i,
        o = function (n) {
            var r = t.createEvent("MouseEvents");
            r.initMouseEvent("click", true, false, e, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            n.dispatchEvent(r)
        },
        u = e.webkitRequestFileSystem,
        a = e.requestFileSystem || u || e.mozRequestFileSystem,
        f = function (t) {
            (e.setImmediate || e.setTimeout)(function () {
                throw t
            }, 0)
        },
        l = "application/octet-stream",
        c = 0,
        h = [],
        p = function () {
            var e = h.length;
            while (e--) {
                var t = h[e];
                if (typeof t === "string") {
                    r.revokeObjectURL(t)
                } else {
                    t.remove()
                }
            }
            h.length = 0
        },
        d = function (e, t, n) {
            t = [].concat(t);
            var r = t.length;
            while (r--) {
                var i = e["on" + t[r]];
                if (typeof i === "function") {
                    try {
                        i.call(e, n || e)
                    } catch (s) {
                        f(s)
                    }
                }
            }
        },
        v = function (r, o) {
            var f = this,
                p = r.type,
                v = false,
                m, g, y = function () {
                    var e = n().createObjectURL(r);
                    h.push(e);
                    return e
                },
                b = function () {
                    d(f, "writestart progress write writeend".split(" "))
                },
                w = function () {
                    if (v || !m) {
                        m = y(r)
                    }
                    if (g) {
                        g.location.href = m
                    } else {
                        window.open(m, "_blank")
                    }
                    f.readyState = f.DONE;
                    b()
                },
                E = function (e) {
                    return function () {
                        if (f.readyState !== f.DONE) {
                            return e.apply(this, arguments)
                        }
                    }
                },
                S = {
                    create: true,
                    exclusive: false
                },
                x;
            f.readyState = f.INIT;
            if (!o) {
                o = "download"
            }
            if (s) {
                m = y(r);
                t = e.document;
                i = t.createElementNS("http://www.w3.org/1999/xhtml", "a");
                i.href = m;
                i.download = o;
                var T = t.createEvent("MouseEvents");
                T.initMouseEvent("click", true, false, e, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                i.dispatchEvent(T);
                f.readyState = f.DONE;
                b();
                return
            }
            if (e.chrome && p && p !== l) {
                x = r.slice || r.webkitSlice;
                r = x.call(r, 0, r.size, l);
                v = true
            }
            if (u && o !== "download") {
                o += ".download"
            }
            if (p === l || u) {
                g = e
            }
            if (!a) {
                w();
                return
            }
            c += r.size;
            a(e.TEMPORARY, c, E(function (e) {
                e.root.getDirectory("saved", S, E(function (e) {
                    var t = function () {
                        e.getFile(o, S, E(function (e) {
                            e.createWriter(E(function (t) {
                                t.onwriteend = function (t) {
                                    g.location.href = e.toURL();
                                    h.push(e);
                                    f.readyState = f.DONE;
                                    d(f, "writeend", t)
                                };
                                t.onerror = function () {
                                    var e = t.error;
                                    if (e.code !== e.ABORT_ERR) {
                                        w()
                                    }
                                };
                                "writestart progress write abort".split(" ").forEach(function (e) {
                                    t["on" + e] = f["on" + e]
                                });
                                t.write(r);
                                f.abort = function () {
                                    t.abort();
                                    f.readyState = f.DONE
                                };
                                f.readyState = f.WRITING
                            }), w)
                        }), w)
                    };
                    e.getFile(o, {
                        create: false
                    }, E(function (e) {
                        e.remove();
                        t()
                    }), E(function (e) {
                        if (e.code === e.NOT_FOUND_ERR) {
                            t()
                        } else {
                            w()
                        }
                    }))
                }), w)
            }), w)
        },
        m = v.prototype,
        g = function (e, t) {
            return new v(e, t)
        };
    m.abort = function () {
        var e = this;
        e.readyState = e.DONE;
        d(e, "abort")
    };
    m.readyState = m.INIT = 0;
    m.WRITING = 1;
    m.DONE = 2;
    m.error = m.onwritestart = m.onprogress = m.onwrite = m.onabort = m.onerror = m.onwriteend = null;
    e.addEventListener("unload", p, false);
    g.unload = function () {
        p();
        e.removeEventListener("unload", p, false)
    };
    return g
}(typeof self !== "undefined" && self || typeof window !== "undefined" && window || this.content);
if (typeof module !== "undefined" && module !== null) {
    module.exports = saveAs
} else if (typeof define !== "undefined" && define !== null && define.amd != null) {
    define([], function () {
        return saveAs
    })
}