﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;

namespace eLedgerMVC.Controllers
{
    public class ServiceController : ApiController
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        [OutputCache(Duration = 2000, Location = OutputCacheLocation.Client, VaryByParam = "*")]
        public dynamic getService(string ULN)
        {
            string Service = "{" + new ServiceBusiness().getJsonServiceSummary(ULN) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Service, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getServiceDetails(string ULN, string dateWindow)
        {
            string ServiceDet = new ServiceBusiness().getJsonServiceDetails(ULN, dateWindow);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(ServiceDet, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]

        public dynamic getServiceCallIDDetails(string serviceCallId, string tblID)
        {
            string ServiceCallID = new ServiceBusiness().getServiceDetail(serviceCallId, tblID);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(ServiceCallID, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]

        public dynamic getmachinelistbywebid(string uln, string webid)
        {
            string EqMachList = new EquipmentBusiness().getMachineListByWebIdJson(uln, webid);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(EqMachList, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpGet]
        public dynamic viewRefunds(string ULN)
        {
            string viewRefund = new ServiceBusiness().getviewRefundURL(ULN);

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(viewRefund, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
    }
}
 