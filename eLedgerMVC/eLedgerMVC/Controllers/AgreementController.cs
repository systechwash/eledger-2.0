﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;

namespace eLedgerMVC.Controllers
{
    public class AgreementController : ApiController
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getAgreementDetails(string ULN)
        {
            string Agreement = "{" + new AgreementBusiness().getAgreementDetails(ULN) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Agreement, System.Text.Encoding.UTF8, "application/json")
            };
            return response;  
        }
    }
}
