﻿using eLedgerBusiness;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using eLedgerBusiness.DAL;
using eLedgerEntities;
using System.Text.RegularExpressions;
using System.Text;

namespace eLedgerMVC.Controllers
{
    public class ExportToExcelController : Controller
    {
        //
        // GET: /ExportToExcel/

        StringBuilder str;
        string TicketNo = string.Empty;

        public dynamic MapExportToExcel(string uln, string locationtype, string distance)
        {
            string fileName = "";
            fileName = "NearBy_" + uln + ".xls";
            GridView gv = new GridView();
            gv.DataSource = new GogoleMapPlusBusiness().getexporttoexcel(uln, locationtype, distance);
            gv.DataBind();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Buffer = true;
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index", "EventsData");
        }
        public string serviceexport(Alert serviceparam)
        {
            string success = "[{\"flag\":\"true\"}]";
            string svgText = System.Web.HttpUtility.UrlDecode(serviceparam.AlertMSG);
            if (svgText != "")
            {
                var fpath = string.Empty;
                var svgpath = string.Empty;
                var pngpath = string.Empty;
                if (Directory.Exists(Server.MapPath("Servicechart")) == false)
                    Directory.CreateDirectory(Server.MapPath("Servicechart"));
                removefile(Server.MapPath("Servicechart"));
                fpath = Server.MapPath("Servicechart/Service_" + Session.SessionID + ".xls");
                svgpath = Server.MapPath("Servicechart/ServiceXml_" + serviceparam.ULN + "_" + Session.SessionID + ".txt");
                pngpath = Server.MapPath("Servicechart/ServicePng_" + serviceparam.ULN + "_" + Session.SessionID + ".png");
                if (System.IO.File.Exists(fpath) == false)
                    System.IO.File.Create(fpath).Close();
                else
                    System.IO.File.Create(fpath).Close();
                if (System.IO.File.Exists(svgpath) == false)
                    System.IO.File.Create(svgpath).Close();
                else
                    System.IO.File.Create(svgpath).Close();
                if (System.IO.File.Exists(pngpath) == false)
                    System.IO.File.Create(pngpath).Close();
                else
                    System.IO.File.Create(pngpath).Close();

                using (StreamWriter writer = new StreamWriter(svgpath, true))
                {
                    writer.WriteLine(svgText);
                }
                getPdf(serviceparam.ULN);
            }
            else
            {

            }
            return success;
        }

        public void getPdf(string ULn)
        {
            var svgpath = string.Empty;
            var pngpath = string.Empty;
            svgpath = Server.MapPath("Servicechart/ServiceXml_" + ULn + "_" + Session.SessionID + ".txt");
            pngpath = Server.MapPath("Servicechart/ServicePng_" + ULn + "_" + Session.SessionID + ".png");

            var svgDocument = Svg.SvgDocument.Open(svgpath);
            var bitmap = svgDocument.Draw();
            MemoryStream docMemStream = new MemoryStream();
            bitmap.Save(pngpath, System.Drawing.Imaging.ImageFormat.Png);
        }


        public dynamic ServiceExportToExcel(string uln, string dateWindow, string serviceticketnos)
        {
            try
            {
                string fileName = "";
                fileName = "ServiceCalls_" + uln + ".xls";
                DataSet ds = new DataSet();
                string FPath = "";
                GridView gv = new GridView();
                ds = new ServiceBusiness().getexporttoexcel(uln, dateWindow);
                if (ds != null)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        var fpath = string.Empty;
                        if (Directory.Exists(Server.MapPath("ExcelTemplates")) == false)
                            Directory.CreateDirectory(Server.MapPath("ExcelTemplates"));
                        removefile(Server.MapPath("ExcelTemplates"));
                        fpath = Server.MapPath("ExcelTemplates/Service_" + Session.SessionID + ".xls");
                        if (System.IO.File.Exists(fpath) == false)
                            System.IO.File.Create(fpath).Close();
                        else
                            System.IO.File.Create(fpath).Close();
                        if (fpath.Trim() != string.Empty)
                            FPath = DataSetToExcel(ds, fpath, serviceticketnos, dateWindow, uln);
                        String strRequest = Request.QueryString["file"];
                        FileInfo file = new FileInfo(FPath);
                        if (file.Exists)
                        {
                            Response.Clear();
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                            Response.AddHeader("Content-Length", file.Length.ToString());
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.WriteFile(file.FullName);
                            Response.End();
                            return RedirectToAction("Index", "EventsData");
                        }
                        else
                        {
                            Response.Write("<script type=\"text/javascript\">alert('No Service Data Available');window.close();</script>");
                            return null;
                        }
                    }
                    else
                    {
                        Response.Write("<script type=\"text/javascript\">alert('No Service Data Available');window.close();</script>");
                        return null;
                    }
                }
                else
                {
                    Response.Write("<script type=\"text/javascript\">alert('No Service Data Available');window.close();</script>");
                    return null;
                }
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ServiceExport", "uln:" + uln + " dateWindow:" + dateWindow);
                return new StartController().ServerError();
            }
        }

        public void removefile(string folder)
        {
            string[] files = Directory.GetFiles(folder);

            foreach (string file1 in files)
            {
                FileInfo fi = new FileInfo(file1);
                if (fi.LastAccessTime < DateTime.Now.AddMinutes(-15))
                    fi.Delete();
            }
        }

        public void serviceCategorybreakdown(string TicketNos, string Vehicle, string Webid, string ClassicationDesc, string ResolutionCode, string PartType)
        {
            if (TicketNo != TicketNos)
            {
                TicketNo = TicketNos;
            }

        }


        public string DataSetToExcel(DataSet dsExport, string path, string ticketnos, string reportterm, string ULN)
        {
            string dateformat = "";           
            
            if (Request.UserLanguages[0].Equals("en-US")) // https://msdn.microsoft.com/en-us/library/system.web.httprequest.userlanguages.aspx
            {
                dateformat = "d";
            }
            else
            {
                dateformat = "dd/MM/yyyy";
            }
            var SumRes = dsExport.Tables[3].AsEnumerable().Sum(dr => Convert.ToDecimal(dr["ServiceRepairCount"]));
            string TotalMachine = dsExport.Tables[2].Rows[0].Field<String>("TotPhysicalCountAs_Fmt");

            if (TotalMachine.Contains(','))
            {
                TotalMachine = TotalMachine.Replace(",", "");
            }
            int TotMach = Convert.ToInt32(TotalMachine);

            decimal Count = Convert.ToDecimal((from Rows in dsExport.Tables[1].AsEnumerable()
                                               select Rows["TicketNo"]).Distinct().Count().ToString());

            //decimal Count = dsExport.Tables[1].Rows.Count;

            decimal CallsRatio;
            if (TotMach != 0)
            {
                CallsRatio = ((Count / (Convert.ToDecimal(reportterm))) * 365) / TotMach;
                CallsRatio = Math.Round(CallsRatio, 1);
            }
            else
            {
                CallsRatio = 0.00m;
                CallsRatio = Math.Round(CallsRatio, 1);
            }
            var avgRes = Convert.ToDecimal(dsExport.Tables[4].Rows[0]["AvgResponseTime"]);

            avgRes = Math.Round(avgRes, 1);

            if (reportterm == "90")
                reportterm = "3 Months";
            if (reportterm == "365")
                reportterm = "12 Months";
            if (reportterm == "730")
                reportterm = "24 Months";
            if (path != string.Empty)
            {
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
                StreamWriter SWriter = new StreamWriter(fs);
                str = new StringBuilder();
                Int32 colspan = dsExport.Tables[1].Columns.Count;
                //cchoi 5/8/2015 added first 2 lines to accomidate UTF-8 and replaced String with StringBuilder...String concats are costly! 
                if (dsExport.Tables[0].Rows.Count > 0)
                {
                    DataRow row = dsExport.Tables[0].Rows[0];                  
                    str.Append("<head>");
                    str.Append("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
                    str.Append("<Table border=0><TR ><TD style='font-size:18px' bgcolor='E5E4E2' align='center' colspan='14'>" + "<B>WASH Service History Report</B>" + "</TD></tr><tr></tr><tr></tr></TR></Table>");
                    str.Append("<Table border=0><tr><TD align='left' bgcolor='E5E4E2'><B>Location Name:</B></TD> <TD colspan='3'>" + row["PropertyName"].ToString() + "</TD><TD></TD>");
                    str.Append("<TD bgcolor='E5E4E2'><B>Created Date:</B></TD><TD align='center' colspan=2>" + DateTime.Now.Date.ToString(dateformat) + "</TD></tr>");
                    str.Append("<tr><TD align='left' bgcolor='E5E4E2'><B>Location Address:</TD></B><TD colspan='7'>" + row["Address"] + ", " + row["City"] + ", " + row["State"] + " " + row["Zip"] + " </TD></tr>");
                    str.Append("<tr><TD align='left' bgcolor='E5E4E2'><B>Location #:</TD></B><TD colspan='7'>" + row["ULN"] + "</TD></tr>");
                    str.Append("<tr><TD align='left' bgcolor='E5E4E2'><B>Report Term:</TD></B><TD colspan='7'>" + reportterm + "</TD></tr>");
                    str.Append("<tr></tr><tr></TR></Table>");
                }
                str.Append("<Table border=0>");
                foreach (DataRow DBRow in dsExport.Tables[1].Rows)
                {
                    if (TicketNo != DBRow["TicketNo"].ToString())
                    {
                        if (TicketNo != "")
                            str.Append("<TR></TR><TR>");

                        TicketNo = DBRow["TicketNo"].ToString();

                        foreach (DataColumn DBCol in dsExport.Tables[1].Columns)
                        {
                            if (DBCol.ColumnName != "responsehours" && DBCol.ColumnName != "Vehicle" && DBCol.ColumnName != "WebId" && DBCol.ColumnName != "ClassificationDesc" && DBCol.ColumnName != "ResolutionCode" && DBCol.ColumnName != "PartType")
                                str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + DBCol.ColumnName + "</b></TD>");
                        }
                        str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + "Tech Vehicle" + "</b></TD>");
                        str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + "Equip ID" + "</b></TD>");
                        str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + "Service Category" + "</b></TD>");
                        str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + "Resolution" + "</b></TD>");
                        str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + "Repair Notes" + "</b></TD>");

                        str.Append("</TR>");

                        str.Append("<TR>");

                        foreach (DataColumn DBCol in dsExport.Tables[1].Columns)
                        {
                            if (DBCol.ColumnName == "Complaint")
                                str.Append("<TD Valign='top' align='left'>" + Convert.ToString(DBRow[DBCol.ColumnName]) + "</TD>");
                            else if (DBCol.ColumnName != "responsehours" && DBCol.ColumnName != "Vehicle" && DBCol.ColumnName != "WebId" && DBCol.ColumnName != "ClassificationDesc" && DBCol.ColumnName != "ResolutionCode" && DBCol.ColumnName != "PartType" && DBCol.ColumnName != "Complaint")
                                if ((DBCol.ColumnName == "Created Date" || DBCol.ColumnName == "Closed Date"))
                                {
                                    str.Append("<TD Valign='top' align='center'>" + Convert.ToDateTime(DBRow[DBCol.ColumnName]).ToString(dateformat) + "</TD>");
                                }
                                else
                                {
                                    str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow[DBCol.ColumnName]) + "</TD>");
                                }
                        }
                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["Vehicle"].ToString()) + "</TD>");
                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["WebId"].ToString()) + "</TD>");

                        if (DBRow["ClassificationDesc"].ToString().Trim() == "Building / LR Related")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Building / LR Issues" + "</TD>");
                        }
                        else if (DBRow["ClassificationDesc"].ToString().Trim() == "User / Customer Related")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Customer Education Tickets" + "</TD>");
                        }
                        else if (DBRow["ClassificationDesc"].ToString().Trim() == "Internal Company Business")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Internal Company Business" + "</TD>");
                        }
                        else if (DBRow["ClassificationDesc"].ToString().Trim() == "Mechanical Related")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Machine Repairs" + "</TD>");
                        }
                        else
                            str.Append("<TD Valign='top' align='center'>" + "" + "</TD>");


                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["ResolutionCode"].ToString()) + "</TD>");

                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["PartType"].ToString()) + "</TD>");
                        str.Append("</TR>");
                    }
                    else
                    {
                        str.Append("<TR><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD>");
                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["Tech Name"].ToString()) + "</TD>");
                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["Vehicle"].ToString()) + "</TD>");
                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["WebId"].ToString()) + "</TD>");

                        if (DBRow["ClassificationDesc"].ToString().Trim() == "Building / LR Related")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Building / LR Issues" + "</TD>");
                        }
                        else if (DBRow["ClassificationDesc"].ToString().Trim() == "User / Customer Related")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Customer Education Tickets" + "</TD>");
                        }
                        else if (DBRow["ClassificationDesc"].ToString().Trim() == "Internal Company Business")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Internal Company Business" + "</TD>");
                        }
                        else if (DBRow["ClassificationDesc"].ToString().Trim() == "Mechanical Related")
                        {
                            str.Append("<TD Valign='top' align='center'>" + "Machine Repairs" + "</TD>");
                        }
                        else
                            str.Append("<TD Valign='top' align='center'>" + "" + "</TD>");

                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["ResolutionCode"].ToString()) + "</TD>");

                        str.Append("<TD Valign='top' align='center'>" + Convert.ToString(DBRow["PartType"].ToString()) + "</TD>");
                        str.Append("</TR>");
                    }

                    //string[] Amt = ticketnos.Split(',');
                    //foreach (string amtvalue in Amt)
                    //{
                    //   if (amtvalue == DBRow["TicketNo"].ToString())
                    // {


                    //}
                    // }
                }
                str.Append("<tr></tr><tr></TR></TABLE>");
                str.Append("<Table border=0>");
                str.Append("<TR><TD colspan='2' bgcolor='E5E4E2'><B>Total Machines:</B></TD><TD>" + TotMach + "</TD><TD></TD><TD></TD><TD colspan='2' bgcolor='E5E4E2'><B>Avg Response Time:</b></TD><TD>" + avgRes.ToString() + "</TD></TR>");
                str.Append("<TR><TD colspan='2' bgcolor='E5E4E2'><B>Total Calls:</B></TD><TD>" + Count + "</TD><TD></TD><TD></TD><TD colspan='2'bgcolor='E5E4E2'><B>Calls Per Machine Ratio:</b></TD><TD>" + CallsRatio + "</TD></TR>");
                str.Append("<TR></TR><TR></TR><TR></TR><TR></TR></Table>");

                if (dsExport.Tables[3].Rows.Count > 0)
                {
                    str.Append("<Table><TR ><TD style='font-size:18px' bgcolor='E5E4E2' align='center' colspan='3'>" + "<B>12 Month Service History Breakdown</B>" + "</TD></tr><tr></tr><tr></tr></TR></Table>");
                    str.Append("<Table border=0>");
                    str.Append("<TR>");
                    foreach (DataColumn DBCol in dsExport.Tables[3].Columns)
                    {
                        if (DBCol.ColumnName == "ServiceRepairCategory")
                            str.Append("<TD bgcolor='E5E4E2'  align='center'><b>" + "Category" + "</b></TD>");

                        if (DBCol.ColumnName == "ServiceRepairCount")
                            str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + "Repair Count" + "</b></TD>");

                        if (DBCol.ColumnName == "ServiceRepairPercentage")
                            str.Append("<TD bgcolor='E5E4E2' align='center'><b>" + "Repair %" + "</b></TD>");
                    }
                    str.Append("</TR>");


                    foreach (DataRow DBRow in dsExport.Tables[3].Rows)
                    {
                        str.Append("<TR>");
                        foreach (DataColumn DBCol in dsExport.Tables[3].Columns)
                        {
                            if (DBCol.ColumnName == "ServiceRepairCategory")
                            {
                                if (DBRow["ServiceRepairCategory"].ToString().Trim() == "Building / LR Related")
                                {
                                    str.Append("<TD align='left'>" + "Building / LR Issues" + "</TD>");
                                }
                                else if (DBRow["ServiceRepairCategory"].ToString().Trim() == "User / Customer Related")
                                {
                                    str.Append("<TD align='left'>" + "Customer Education Tickets" + "</TD>");
                                }
                                else if (DBRow["ServiceRepairCategory"].ToString().Trim() == "Internal Company Business")
                                {
                                    str.Append("<TD align='left'>" + "Internal Company Business" + "</TD>");
                                }
                                else if (DBRow["ServiceRepairCategory"].ToString().Trim() == "Mechanical Related")
                                {
                                    str.Append("<TD align='left'>" + "Machine Repairs" + "</TD>");
                                }
                                else if (DBRow["ServiceRepairCategory"].ToString().Trim() == "Unknown")
                                {
                                    str.Append("<TD align='left'>" + "Unknown" + "</TD>");
                                }
                            }
                            else if (DBCol.ColumnName == "ServiceRepairPercentage")
                                str.Append("<TD Valign='top' align='center'>" + Math.Round(Convert.ToDecimal(DBRow[DBCol.ColumnName])).ToString() + " %</TD>");
                            else if (DBCol.ColumnName != "SortOrder") str.Append("<TD align='center'>" + Convert.ToString(DBRow[DBCol.ColumnName]) + "</TD>");
                        }
                        str.Append("</TR>");
                    }
                    str.Append("<TR></TR><TR><TD bgcolor='E5E4E2' align='center'><B>Total</B></TD><TD align='center'>" + SumRes + "</TD><TD align='center'>" + "100" + "%</TD></TR>");
                    str.Append("<TR></TR><TR></TR></Table>");
                   


                    string imgPath2 = System.Net.Dns.GetHostName();
                    FileInfo file = new FileInfo(Server.MapPath("Servicechart/ServicePng_" + ULN + "_" + Session.SessionID + ".png"));

                    if (file.Exists)
                    {
                        str.Append("<Table><TR><TD><img src= 'http://" + imgPath2 + "/eledger2/ExportToExcel/Servicechart/ServicePng_" + ULN + "_" + Session.SessionID + ".png'  alt='Banner'/></td></tr></Table>");
                        //str .Append( @"<Table><tr><td><img src=""C:\\Dev\\EledgerTFS\\EledgerTFS_Latest\\eLedger2.1\\eLedgerMVC\\eLedgerMVC\\ExportToExcel\\Servicechart\\ServicePng.png"" \></td></tr></Table>";
                    }
                    
                    SWriter.WriteLine(str.ToString());
                    SWriter.Flush();
                    SWriter.Close();
                    fs.Close();
                }
            }
            return path;
        }

        protected string GetUrl()
        {
            string[] splits = Request.Url.AbsoluteUri.Split('/');
            string url = splits[0] + "//";
            for (int i = 2; i < splits.Length - 1; i++)
            {
                url += splits[i];
                url += "/";
            }
            return url;
        }


        public dynamic RevenueExportToExcel(string uln, string year, string splAmt)
        {
            try
            {
                string FPath = "";
                string fileName = "";
                fileName = "Revenue_" + uln + ".xls";
                GridView gv = new GridView();
                DataSet ds = new DataSet();
                ds = new RevenueBusiness().getRevenueExportdetails(uln, year);
                if (ds != null && ds.Tables.Count > 1)
                {
                    var fpath = string.Empty;
                    if (Directory.Exists(Server.MapPath("ExcelTemplates")) == false)
                        Directory.CreateDirectory(Server.MapPath("ExcelTemplates"));
                    removefile(Server.MapPath("ExcelTemplates"));
                    fpath = Server.MapPath("ExcelTemplates/Revenue_" + Session.SessionID + ".xls");
                    if (System.IO.File.Exists(fpath) == false)
                        System.IO.File.Create(fpath).Close();
                    else
                        System.IO.File.Create(fpath).Close();
                    if (fpath.Trim() != string.Empty)
                        FPath = RevenueDataSetToExcel(ds, fpath, "", splAmt);
                    String strRequest = Request.QueryString["file"];
                    FileInfo file = new FileInfo(FPath);
                    if (file.Exists)
                    {
                        Response.Clear();
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                        Response.AddHeader("Content-Length", file.Length.ToString());
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.WriteFile(file.FullName);
                        Response.End();
                        return RedirectToAction("Index", "EventsData");
                    }
                    else
                    {
                        Response.Write("<script type=\"text/javascript\">alert('This file does not exist.');window.close();</script>");
                        return null;
                    }
                    
                }
                else
                {
                    Response.Write("<script type=\"text/javascript\">alert('No Revenue Data Available');window.close();</script>");
                    return null;
                }
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "RevenueExport", "ULN:" + uln + " year:" + year);
                return new StartController().ServerError();
            }
        }


        public string RevenueDataSetToExcel(DataSet dsExport, string path, string tableName, string splAmt)
        {
            string dateformat = "";
            if (System.Security.Principal.WindowsIdentity.GetCurrent().Name.Contains("COINAMATIC"))
            {
                dateformat = "dd/MM/yyyy";
            }
            else
            {
                dateformat = "d";
            }
            if (path != string.Empty)
            {
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
                StreamWriter SWriter = new StreamWriter(fs);
                string str = string.Empty;
                Int32 colspan = dsExport.Tables[1].Columns.Count;
                if (dsExport.Tables[2].Rows.Count > 0)
                {
                    DataRow row = dsExport.Tables[2].Rows[0];
                    if (dsExport.Tables[0].Rows.Count > 0)
                        str += "<Table border=0><TR  bgcolor='E5E4E2'><TD style='font-size:18px' bgcolor='E5E4E2' align='center' colspan='17'>" + "<B>WASH Revenue History Report</B>" + "</TD></tr><tr></tr><tr></tr></TR></Table>";
                    else
                        str += "<Table border=0><TR  bgcolor='E5E4E2'><TD style='font-size:18px' bgcolor='E5E4E2' align='center' colspan='10'>" + "<B>WASH Revenue History Report</B>" + "</TD></tr><tr></tr><tr></tr></TR></Table>";
                    str += "<Table border=0><tr><TD align='left' bgcolor='E5E4E2'><B>Location Name:</B></TD><TD colspan='3'>" + row["PropertyName"].ToString() + "</TD><TD></TD><TD bgcolor='E5E4E2'><B>Created Date:</B></TD><TD align='center' colspan=2>" + DateTime.Now.Date.ToString(dateformat) + "</TD><td></td><td></td><td></td><td></td><td></td><td></td><td></td><TD></TD></tr>";
                    str += "<tr><TD align='left' bgcolor='E5E4E2'><B>Location Address:</TD></B><TD colspan='7'>" + row["Address"] + ", " + row["City"] + ", " + row["State"] + " " + row["Zip"] + "  </TD><td></td><td></td><td></td><td></td><td></td><td></td><td></td><TD></TD></tr>";
                    str += "<tr><TD align='left' bgcolor='E5E4E2'><B>Location #:</TD></B><TD colspan='7'>" + row["ULN"] + "</TD><td></td><td></td><td></td><td></td><td></td><td></td><td></td><TD></TD></tr>";
                    str += "<tr></tr><TR></TR></Table>";
                }
                if (dsExport.Tables[0].Rows.Count > 0)
                {
                    var NetWasher = String.Format("{0:N}", dsExport.Tables[0].AsEnumerable().Sum(dr => Convert.ToDecimal(dr["NetWasher"])));
                    var NetDryer = String.Format("{0:N}", dsExport.Tables[0].AsEnumerable().Sum(dr => Convert.ToDecimal(dr["NetDryer"])));
                    var PaymentAmount = String.Format("{0:N}", dsExport.Tables[0].AsEnumerable().Sum(dr => Convert.ToDecimal(dr["PaymentAmount"])));
                    var NetTotal = String.Format("{0:N}", dsExport.Tables[0].AsEnumerable().Sum(dr => Convert.ToDecimal(dr["NetTotal"])));
                    str += "<Table border=0><TR>";
                    foreach (DataColumn DBCol in dsExport.Tables[0].Columns)
                    {
                        if (DBCol.ColumnName != "DisplayYear_Fmt" && DBCol.ColumnName != "Row" && DBCol.ColumnName != "NoteText" && DBCol.ColumnName != "CurrencyAmt_Fmt" && DBCol.ColumnName != "CurrencyAmt" && DBCol.ColumnName != "CoinAmt_Fmt" && DBCol.ColumnName != "CardAmt_Fmt" && DBCol.ColumnName != "ManualAmt_Fmt" && DBCol.ColumnName != "CurrencyCardSalesAmt_Fmt" && DBCol.ColumnName != "CreditCardFeeAmt_Fmt" && DBCol.ColumnName != "CreditCardSalesAmt_Fmt" && DBCol.ColumnName != "CoinAmt" && DBCol.ColumnName != "CardAmt" && DBCol.ColumnName != "ManualAmt" && DBCol.ColumnName != "CurrencyCardSalesAmt" && DBCol.ColumnName != "CreditCardFeeAmt" && DBCol.ColumnName != "CreditCardSalesAmt" && DBCol.ColumnName != "Note" && DBCol.ColumnName != "InvoiceAmt" && DBCol.ColumnName != "Pay_Amt" && DBCol.ColumnName != "Net30PerMach" && DBCol.ColumnName != "NetWasher" && DBCol.ColumnName != "NetDryer" && DBCol.ColumnName != "NetTotal" && DBCol.ColumnName != "PaymentAmount" && DBCol.ColumnName != "Gross30perWasher" && DBCol.ColumnName != "Gross30PerDryer" && DBCol.ColumnName != "RecordType")
                            str += "<TD align='center' bgcolor='E5E4E2'><b>" + DBCol.ColumnName + "</b></TD>";
                    }
                    str += "</TR>";
                    foreach (DataRow DBRow in dsExport.Tables[0].Rows)
                    {
                        if (DBRow["RecordType"].ToString() != "P")
                        {
                            str += "<TR>";
                            foreach (DataColumn DBCol in dsExport.Tables[0].Columns)
                            {
                                if (DBCol.ColumnName != "DisplayYear_Fmt" && DBCol.ColumnName != "Row" && DBCol.ColumnName != "NoteText" && DBCol.ColumnName != "CurrencyAmt_Fmt" && DBCol.ColumnName != "CurrencyAmt" && DBCol.ColumnName != "CoinAmt_Fmt" && DBCol.ColumnName != "CardAmt_Fmt" && DBCol.ColumnName != "ManualAmt_Fmt" && DBCol.ColumnName != "CurrencyCardSalesAmt_Fmt" && DBCol.ColumnName != "CreditCardFeeAmt_Fmt" && DBCol.ColumnName != "CreditCardSalesAmt_Fmt" && DBCol.ColumnName != "CoinAmt" && DBCol.ColumnName != "CardAmt" && DBCol.ColumnName != "ManualAmt" && DBCol.ColumnName != "CurrencyCardSalesAmt" && DBCol.ColumnName != "CreditCardFeeAmt" && DBCol.ColumnName != "CreditCardSalesAmt" && DBCol.ColumnName != "Note" && DBCol.ColumnName != "InvoiceAmt" && DBCol.ColumnName != "Pay_Amt" && DBCol.ColumnName != "Net30PerMach" && DBCol.ColumnName != "NetWasher" && DBCol.ColumnName != "NetDryer" && DBCol.ColumnName != "NetTotal" && DBCol.ColumnName != "PaymentAmount" && DBCol.ColumnName != "Gross30perWasher" && DBCol.ColumnName != "Gross30PerDryer" && DBCol.ColumnName != "RecordType")
                                {
                                    if (DBCol.ColumnName == "Cycle Date")
                                    {
                                        if (DBRow["NoteText"].ToString().Length > 10)
                                        {
                                            if (DBRow["NoteText"].ToString().Substring(10).Replace(System.Environment.NewLine, "") != "Commission" && !(Convert.ToInt32(DBRow["NetTotal"]) > 0))
                                                str += "<TD align='center'>" + "" + "</TD>";
                                            else if (DBRow[DBCol.ColumnName].ToString().Length < 12) //Valid date/Date Range check
                                                str += "<TD align='center'>" + Convert.ToDateTime(DBRow[DBCol.ColumnName]).ToString(dateformat) + "</TD>";
                                            else
                                                str += "<TD align='center'>" + Convert.ToString(DBRow[DBCol.ColumnName]) + "</TD>";
                                        }
                                        else
                                        {
                                            if (DBRow[DBCol.ColumnName].ToString().Length < 12) //Valid date/Date Range check
                                                str += "<TD align='center'>" + Convert.ToDateTime(DBRow[DBCol.ColumnName]).ToString(dateformat) + "</TD>";
                                            else
                                                str += "<TD align='center'>" + Convert.ToString(DBRow[DBCol.ColumnName]) + "</TD>";
                                        }
                                    }
                                    else if (DBCol.ColumnName == "Collect Date" && DBRow[DBCol.ColumnName].ToString().Length < 12)
                                        str += "<TD align='center'>" + Convert.ToDateTime(DBRow[DBCol.ColumnName]).ToString(dateformat) + "</TD>";
                                    else
                                        str += "<TD align='center'>" + Convert.ToString(DBRow[DBCol.ColumnName]) + "</TD>";
                                }
                            }
                            str += "</TR>";
                            string[] Amt = splAmt.Split(',');
                            foreach (string amtvalue in Amt)
                            {
                                if (amtvalue != "")
                                {
                                    string[] year = amtvalue.Split('-');
                                    if (year[1].ToString() == DBRow["Row"].ToString() && DBRow["DisplayYear_Fmt"].ToString() == year[0].ToString())
                                    {
                                        decimal sum = Convert.ToDecimal(DBRow["CoinAmt"]) + Convert.ToDecimal(DBRow["CurrencyAmt"]) + Convert.ToDecimal(DBRow["CardAmt"]);
                                        if (DBRow["CoinAmt"].ToString() != "0.00")
                                            str += "<TR><td></td><td bgcolor='E5E4E2'><i>Coin:</i></td><td></td><td></td><td></td><td><i>" + DBRow["CoinAmt_Fmt"].ToString() + "</i></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></td><td></TR>";
                                        if (DBRow["CurrencyAmt"].ToString() != "0.00")
                                            str += "<TR><td></td><td bgcolor='E5E4E2'><i>Currency:</i></td><td></td><td></td><td></td><td><i>" + DBRow["CurrencyAmt_Fmt"].ToString() + "</i></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></td><td></TR>";
                                        if (DBRow["CardAmt"].ToString() != "0.00")
                                            str += "<TR><td></td><td bgcolor='E5E4E2'><i>Credit:</i></td><td></td><td></td><td></td><td><i>" + DBRow["CardAmt_Fmt"].ToString() + "</i></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></td><td></TR>";
                                    }
                                }
                            }
                        }
                    }
                    str += "<tr></tr><tr></TR></TABLE>";
                    str += "<Table border=0>";
                    str += "<TR><TD bgcolor='E5E4E2'><B>Totals</B></TD><TD></TD><TD></TD><TD align='center'><B>$" + NetWasher + "</B></TD><TD align='center'><B>$" + NetDryer + "</B></TD><TD align='center'><B>$" + NetTotal + "</B></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD align='center'><B>$" + PaymentAmount + "</B></TD><TD></TD><TD>" + "" + "</TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>";
                    str += "<tr></TR><tr></TR></Table>";
                }

                if (dsExport.Tables[1].Rows.Count > 0)
                {
                    var InvoiceAmt = String.Format("{0:N}", dsExport.Tables[1].AsEnumerable().Sum(dr => Convert.ToDecimal(dr["InvoiceAmt"])));
                    var PaymentAmount = String.Format("{0:N}", dsExport.Tables[1].AsEnumerable().Sum(dr => Convert.ToDecimal(dr["Pay_Amt"])));
                    str += "<Table border=0>";
                    foreach (DataColumn DBCol in dsExport.Tables[1].Columns)
                    {
                        if (DBCol.ColumnName != "DisplayYear_Fmt" && DBCol.ColumnName != "Row" && DBCol.ColumnName != "NoteText" && DBCol.ColumnName != "CurrencyAmt_Fmt" && DBCol.ColumnName != "CurrencyAmt" && DBCol.ColumnName != "CoinAmt_Fmt" && DBCol.ColumnName != "CardAmt_Fmt" && DBCol.ColumnName != "ManualAmt_Fmt" && DBCol.ColumnName != "CurrencyCardSalesAmt_Fmt" && DBCol.ColumnName != "CreditCardFeeAmt_Fmt" && DBCol.ColumnName != "CreditCardSalesAmt_Fmt" && DBCol.ColumnName != "CoinAmt" && DBCol.ColumnName != "CardAmt" && DBCol.ColumnName != "ManualAmt" && DBCol.ColumnName != "CurrencyCardSalesAmt" && DBCol.ColumnName != "CreditCardFeeAmt" && DBCol.ColumnName != "CreditCardSalesAmt" && DBCol.ColumnName != "Note" && DBCol.ColumnName != "InvoiceAmt" && DBCol.ColumnName != "Pay_Amt" && DBCol.ColumnName != "Net30PerMach" && DBCol.ColumnName != "NetWasher" && DBCol.ColumnName != "NetDryer" && DBCol.ColumnName != "NetTotal" && DBCol.ColumnName != "PaymentAmount" && DBCol.ColumnName != "Gross30perWasher" && DBCol.ColumnName != "Gross30PerDryer" && DBCol.ColumnName != "RecordType")
                            str += "<TD bgcolor='E5E4E2'><b>" + DBCol.ColumnName + "</b></TD>";
                    }
                    str += "</TR>";
                    foreach (DataRow DBRow in dsExport.Tables[1].Rows)
                    {
                        str += "<TR>";
                        foreach (DataColumn DBCol in dsExport.Tables[1].Columns)
                        {
                            if (DBCol.ColumnName != "DisplayYear_Fmt" && DBCol.ColumnName != "Row" && DBCol.ColumnName != "NoteText" && DBCol.ColumnName != "CurrencyAmt_Fmt" && DBCol.ColumnName != "CurrencyAmt" && DBCol.ColumnName != "CoinAmt_Fmt" && DBCol.ColumnName != "CardAmt_Fmt" && DBCol.ColumnName != "ManualAmt_Fmt" && DBCol.ColumnName != "CurrencyCardSalesAmt_Fmt" && DBCol.ColumnName != "CreditCardFeeAmt_Fmt" && DBCol.ColumnName != "CreditCardSalesAmt_Fmt" && DBCol.ColumnName != "CoinAmt" && DBCol.ColumnName != "CardAmt" && DBCol.ColumnName != "ManualAmt" && DBCol.ColumnName != "CurrencyCardSalesAmt" && DBCol.ColumnName != "CreditCardFeeAmt" && DBCol.ColumnName != "CreditCardSalesAmt" && DBCol.ColumnName != "Note" && DBCol.ColumnName != "InvoiceAmt" && DBCol.ColumnName != "Pay_Amt" && DBCol.ColumnName != "Net30PerMach" && DBCol.ColumnName != "NetWasher" && DBCol.ColumnName != "NetDryer" && DBCol.ColumnName != "NetTotal" && DBCol.ColumnName != "PaymentAmount" && DBCol.ColumnName != "Gross30perWasher" && DBCol.ColumnName != "Gross30PerDryer" && DBCol.ColumnName != "RecordType")
                                str += "<TD align='center'>" + Convert.ToString(DBRow[DBCol.ColumnName]) + "</TD>";
                        }
                        str += "</TR>";
                    }
                    str += "<tr></tr><tr></TR></TABLE>";
                    str += "<Table border=0>";
                    str += "<TR><TD bgcolor='E5E4E2'><B>Totals</B></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD align='center'><B>$" + InvoiceAmt + "</B></TD><TD></TD><TD align='center'><B>$" + PaymentAmount + "</B></TD></TR>";
                    str += "</Table>";
                }

                SWriter.WriteLine(str);
                SWriter.Flush();
                SWriter.Close();
                fs.Close();
            }
            return path;
        }
    }
}
