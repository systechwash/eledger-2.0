﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
namespace eLedgerMVC.Controllers
{
    public class AlertsController : ApiController
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic SearchAlert()
        {
            //Added the Change by Shyam for Bit Bucket
            string SearchAlerts = new AlertBusiness().getAlertList("", "");
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(SearchAlerts, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic SearchAlertsType(string alertstatus, string alerttype)
        {
            string SearchAlert = new AlertBusiness().AlertsLocationList(alertstatus, alerttype);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(SearchAlert, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getAlerts(string ULN)
        {
            string Alerts = "{\"Alert\":" + new AlertBusiness().getAlertsULN(ULN, 0, 0) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Alerts, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getAlertsaction(string uln, int History, int Future)
        {
            string Alerts = "{\"Alert\":" + new AlertBusiness().getAlertsULN(uln, History, Future) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Alerts, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic AddAlerts(Alert alertParam)
        {
            
            String lst = new AlertBusiness().AddNewAlert((string)alertParam.ULN, (string)alertParam.ToUserID, (string)alertParam.AlertType, (string)alertParam.AlertMSG, (string)alertParam.DueDate, (string)alertParam.isPrivate.ToString(), (string)alertParam.SequenceType, (string)alertParam.SequenceNum.ToString(), (string)alertParam.RepeatNum.ToString());

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(lst, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic UpdAlerts(Alert alertupdateParam)
        {

            String lst = new AlertBusiness().AlertUpdate((string)alertupdateParam.ULN, (string)alertupdateParam.AlertInstNum.ToString(), (string)alertupdateParam.Staus, (string)alertupdateParam.Clear);

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(lst, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic AlertEventRecipientList(string uln, string webid)
        {
            string list = new AlertBusiness().RecipientByType(uln, webid);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(list, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
    }
}
