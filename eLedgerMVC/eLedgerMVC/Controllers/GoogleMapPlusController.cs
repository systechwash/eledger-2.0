﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;


namespace eLedgerMVC.Controllers
{
    public class GoogleMapPlusController :ApiController
    {               
        public dynamic getgooglekey(string ULN)
        {
            string mapkey = new GogoleMapPlusBusiness().googleMapplus(ULN);

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(mapkey, System.Text.Encoding.UTF8, "application/json")
            };
            return response;            
        }        
        public dynamic getlatlongvalues(string uln, string hostname, string locationtype, string distance)
        {
            string mapkey = new GogoleMapPlusBusiness().getlatlongvalue(uln, locationtype, distance);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(mapkey, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
    }
}
