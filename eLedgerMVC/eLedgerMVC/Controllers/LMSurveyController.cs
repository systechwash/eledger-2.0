﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;

namespace eLedgerMVC.Controllers
{
    public class LMSurveyController : ApiController
    {
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]        
        public dynamic getLMSurvey(string ULN)
        {
            string LMSurvey = new LMSurveyBusiness().getLMSurvey(ULN);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(LMSurvey, System.Text.Encoding.UTF8, "application/json")
            };
            return response;

        }
        [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getLMSurveyLink(string LocId)
        {
            string LMSurvey = new LMSurveyBusiness().getLMSurveyLink(LocId);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(LMSurvey, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
        
    }
}
