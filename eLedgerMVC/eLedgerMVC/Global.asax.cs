﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using eLedgerWebAPI;
using eLedgerMVC.Controllers;
using eLedgerBusiness.DAL;


namespace eLedgerMVC
{

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
           // ViewEngines.Engines.Clear();

            // Registers our Razor C# specific view engine.
            // This can also be registered using dependency injection through the new IDependencyResolver interface.
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();
           
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            RouteTable.Routes.IgnoreRoute("*.html");
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
        }
        protected void Application_BeginRequest()
        {            
            HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(0.5));
            HttpContext.Current.Response.Cache.SetValidUntilExpires(true);
            HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Public);
        }
        void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError().GetBaseException();
            string ErrorMessage = ex.Message;
            string StackTrace = ex.StackTrace;
            string ExceptionType = ex.GetType().FullName;
            new WashDAL().CreateLog(ex.ToString(),"Global.aspx","");
            new StartController().ServerError();
        }
    }
}