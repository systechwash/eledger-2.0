﻿using System;
using System.Runtime.Serialization;
using System.Web;

namespace eLedgerEntities
{
    [DataContract]
    public class AS400PDFPage
    {
        [DataMember]
        public byte[] Image { get; set; }     
    }
}
