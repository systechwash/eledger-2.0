﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Collections.Generic;
namespace eLedgerEntities
{
    [DataContract]
    public class Location
    {
        [DataMember]
        public System.DateTime CreatedDate { get; set; }
        [DataMember]
        public int CreatedById { get; set; }
        [DataMember]
        public System.DateTime UpdatedDate { get; set; }
        [DataMember]
        public int UpdatedById { get; set; }
        [DataMember]
        public System.DateTime DeactivatedDate { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string LRDesc { get; set; }
        [DataMember]
        public int GPLocIndex { get; set; }
        [DataMember]
        public string PropertyType { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Zip { get; set; }
        [DataMember]
        public string County { get; set; }
        [DataMember]
        public string Phone1 { get; set; }
        [DataMember]
        public string Phone2 { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string WebNum { get; set; }   
       [DataMember]
        public string BranchCode { get; set; }
        [DataMember]
        public string ServiceGroup { get; set; }
        [DataMember]
        public int TimeZone { get; set; }
        [DataMember]
        public int DST { get; set; }
        [DataMember]
        public string showDay { get; set; }
        [DataMember]
        public decimal GeographicNetPerMachine { get; set; }
        [DataMember]
        public int NoFlyZone { get; set; }
        [DataMember]
        public int MenToService { get; set; }
        [DataMember]
        public int ResidentSideBySide { get; set; }
        [DataMember]
        public int ResidentSideBySideFurnished { get; set; }
        [DataMember]
        public int ResidentMiniStack { get; set; }
        [DataMember]
        public int ResidentMiniStackFurnished { get; set; }
        [DataMember]
        public string ContractType { get; set; }
        [DataMember]
        public string BillingMethod { get; set; }
        [DataMember]
        public System.DateTime FirstInstallDate { get; set; }
        [DataMember]
        public string ManagerName { get; set; }
        [DataMember]
        public string ManagerPhone { get; set; }
        [DataMember]
        public string ManagerAptNum { get; set; }
        [DataMember]
        public string Route { get; set; }
        [DataMember]
        public string RouteDay { get; set; }
        [DataMember]
        public int CycleDays { get; set; }
        [DataMember]
        public string SubCycle { get; set; }
        [DataMember]
        public string MapPage { get; set; }
        [DataMember]
        public string MapGrid { get; set; }
        [DataMember]
        public System.DateTime TimeStart { get; set; }
        [DataMember]
        public System.DateTime TimeEnd { get; set; }
        [DataMember]
        public int LockBox { get; set; }
        [DataMember]
        public int ProximityId { get; set; }
        [DataMember]
        public string Mappath{ get; set; }
        [DataMember]
        public string Proximity { get; set; }
        [DataMember]
        public int ServiceGoal { get; set; }
        [DataMember]
        public string MapInfo_Fmt { get; set; }
        [DataMember]
        public int POD { get; set; }
        [DataMember]
        public System.DateTime SlipPrinted { get; set; }
        [DataMember]
        public string SpecialInstructions { get; set; }
        [DataMember] public string PreviousOperator { get; set; }
        [DataMember] public string CompetitorName { get; set; }
        [DataMember] public string MaxPOD { get; set; }
        public int RouteAcquiredId { get; set; }
        [DataMember] public string RouteAcquired { get; set; }
        public int CompanyId { get; set; }
        [DataMember] public string CompanyName { get; set; }
        [DataMember] public string DefaultVndClsId { get; set; }
       
        [DataMember] public string Name { get; set; }
        [DataMember] public string Title { get; set; }
        [DataMember] public string MenuText { get; set; }
       
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        [DataMember] public string Branch{get;set;}
        [DataMember] public string Risk { get; set; }
        [DataMember] public string ApartmentUnits_Fmt { get; set; }
        [DataMember] public string CreatedBy { get; set; }
        [DataMember] public string CommissionCode { get; set; }
        [DataMember] public string CommissionType { get; set; }
        [DataMember] public string VendingMethod { get; set; }
        [DataMember]
        public System.DateTime ExpirationDate { get; set; }

        [DataMember]
        public List<User> SalesTeam { get; set; }
        [DataMember] public string AdjGross30TotalAvg { get; set; }
        [DataMember] public string CycleInfo_Fmt { get; set; }
        [DataMember] public string FirstInstallDate_Fmt { get; set; }
        [DataMember] public string TotPhysicalCountAsWash_Fmt{ get; set; }
        [DataMember] public string TotPhysicalCountAsDry_Fmt{ get; set; }
        [DataMember]
        public string PredomVendTimeDry_Fmt { get; set; }
        
        [DataMember] public string PredomVendPriceWash_Fmt{ get; set; }
        [DataMember] public string PredomVendPriceDry_Fmt { get; set; }

        [DataMember] public string RelationType { get; set; }
        [DataMember] public string ContactID { get; set; }
        [DataMember] public string Related_Fmt { get; set; }
        [DataMember] public string TotalMachineCountAs_Fmt { get; set; }
         [DataMember]
        public List<Display> displaydata { get; set; }
         [DataMember]
         public List<User> userLoc { get; set; }
        [DataMember]
        public int rows { get; set; }
        [DataMember]
        public int page { get; set; }
        [DataMember]
        public string ResidentSideBySide_Fmt { get; set; }
        [DataMember]
        public string ResidentMiniStack_Fmt { get; set; }
        [DataMember]
        public string ResidentMiniStackFurnished_Fmt { get; set; }

        [DataMember]
        public string GeographicNetPerMachine_Fmt { get; set; }
        [DataMember]
        public string LaundryRooms_Fmt { get; set; }
        [DataMember]
        public string PredomMfgAVS { get; set; }
        [DataMember]
        public string PredomSitecode { get; set; }
        [DataMember]
        public string VendingMethodAVS { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string LineOfBusiness { get; set; }
      

        
    }
}