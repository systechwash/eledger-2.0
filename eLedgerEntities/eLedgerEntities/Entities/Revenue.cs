﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eLedgerEntities
{
    public class Revenue
    {
        public string SplitRevenueNotes { get; set; }
        public string RecordType { get; set; }
        public string NoteType { get; set; }
        public string CheckNum { get; set; }
        public string ShowAllRevenue { get; set; }
        public string ULN { get; set; }
        public string CollectYear_Fmt { get; set; }
        public string RT { get; set; }
        public int NetTotal { get; set; }
        public int CycleDays { get; set; }
        public string ThreadId { get; set; }
        public string Visibility { get; set; }
        public string VendName { get; set; }
        public string Records { get; set; }
        public string Closed { get; set; }

        public string DisplayYear_Fmt { get; set; }
        public string CollectDate_Fmt { get; set; }
        public string CycleDate_Fmt { get; set; }
        public string CycleDays_Fmt { get; set; }
        public string NetTotal_Fmt { get; set; }
        public string NetWasher_Fmt { get; set; }
        public string NetDryer_Fmt { get; set; }
        public string PaymentDate_Fmt { get; set; }
        public string PendingAmount_Fmt { get; set; }
        public string CheckStatus { get; set; }
        public string SplitPercentage_Fmt { get; set; }
        public string RedCoin_Fmt { get; set; }
        public string BUW_Fmt { get; set; }
        public string Gross30PerWasher_Fmt { get; set; }
        public string Gross30PerDryer_Fmt { get; set; }
        public string Gross30PerMach_Fmt { get; set; }
        public string Net30PerMach_Fmt { get; set; }
        public string NoteText { get; set; }
        public string Comment { get; set; }
        public string DocNum { get; set; }
        public string DocTypeCode { get; set; }
        public string InvoiceDate_Fmt { get; set; }
        public string InvoiceAmount_Fmt { get; set; }
        public string CheckNoTitle { get; set; }
        public string noteRowId { get; set; }
        public string noteClass { get; set; }
        public bool isNewTable { get; set; }


    }
}