﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace eLedgerEntities
{
[DataContract]
    public class Activity
    {
        [DataMember]
        public System.DateTime ActivityDate { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string Height { get; set; }
        [DataMember]
        public string Width { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public string Link { get; set; }
        [DataMember]
        public System.DateTime UpdatedDate { get; set; }
        [DataMember]
        public string ULN { get; set; }
       
    }
}