﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace eLedgerEntities
{
    [DataContract]
    public class Equipment
    {
        [DataMember]
        public string WebId { get; set; }
        [DataMember]
        public string LoadType { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public string PurchaseDate_Fmt { get; set; }
        [DataMember]
        public string AgeWhenPurchased_Fmt { get; set; }
        [DataMember]
        public string InstallDate_Fmt { get; set; }
        [DataMember]
        public string TimeOnLoc_Fmt { get; set; }
        [DataMember]
        public string CurrentCondition { get; set; }
        [DataMember]
        public string AgeNow_Fmt { get; set; }
        [DataMember]
        public string CountAsWash_Fmt { get; set; }
        [DataMember]
        public string CountAsDry_Fmt { get; set; }
        [DataMember]
        public string MeterType { get; set; }
        [DataMember]
        public string Sleeved { get; set; }
        [DataMember]
        public string VendingMethod { get; set; }
        [DataMember]
        public List<Display> displaydata { get; set; }
        [DataMember]
        public string FuelType { get; set; }
        [DataMember]
        public string Manufacturer { get; set; }
        [DataMember]
        public string EquipmentType { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string VendPrice_Fmt { get; set; }
        [DataMember]
        public List<OtherEquipment> otherequipmentdetails { get; set; }
        [DataMember]
        public List<Equipmentdata> equipmentdetails { get; set; }
        [DataMember]
        public List<otherEquipmentdatatotalcount> otherEquipmentdatatotal { get; set; }
        [DataMember]
        public List<Equipmentdatatotalcount> equipmentdatatotal { get; set; }
        [DataMember]
        public List<TotalLaundryMachine> totallaundrymachinedet { get; set; }
        [DataMember]
        public List<TotalMachine> totalmachinedet { get; set; }
        public List<Machinedetailsbywebid> Machinelistbywebid { get;set;}
        public List<Machineattributevalubywebid> Machineattributebywebid { get; set; }
    }
    [DataContract]
    public class OtherEquipment
    {
        [DataMember]
        public string OtherActualType { get; set; }
        [DataMember]
        public string OtherEquipmentType { get; set; }
        [DataMember]
        public string OtherManufactureType { get; set; }
        [DataMember]
        public string OtherVendingType { get; set; }
        [DataMember]
        public string OtherSitecode { get; set; }

    }
    [DataContract]
    public class Equipmentdata
    {

        [DataMember]
        public string FuelType { get; set; }
        [DataMember]
        public string Manufacturer { get; set; }
        [DataMember]
        public string Actual { get; set; }
        [DataMember]
        public string CountAsDry { get; set; }
        [DataMember]
        public string CountAsWash { get; set; }
        [DataMember]
        public string CountAs { get; set; }
        [DataMember]
        public string EquipmentType { get; set; }
        [DataMember]
        public string VendPrice_Fmt { get; set; }
        [DataMember]
        public string CycleTime_Fmt { get; set; }
    }
    [DataContract]
    public class Equipmentdatatotalcount
    {
        [DataMember]
        public string TotalActualcount { get; set; }
        [DataMember]
        public string TotlaCountAsDry { get; set; }
        [DataMember]
        public string TotalCountAsWash { get; set; }
        [DataMember]
        public string Totalcount { get; set; }
    }
    [DataContract]
    public class otherEquipmentdatatotalcount
    {
        [DataMember]
        public string OtherActualCount { get; set; }
        [DataMember]
        public string OtherCountaswash { get; set; }
    }

    [DataContract]
    public class TotalLaundryMachine
    {
        [DataMember]
        public string RoomID { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string RoomName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string StreetAddress { get; set; }
        [DataMember]
        public string NumberofInstallers { get; set; }
        [DataMember]
        public string SpecialInstruction { get; set; }
        [DataMember]
        public string Size { get; set; }
    }
    public class TotalMachine
    {
        [DataMember]
        public string WebId { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string RoomId { get; set; }
        [DataMember]
        public string EquipmentType { get; set; }
        [DataMember]
        public string Manufacturer { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public string PurchaseDate_Fmt { get; set; }
        [DataMember]
        public string AgeWhenPurchased_Fmt { get; set; }
        [DataMember]
        public string TimeOnLoc_Fmt { get; set; }
        [DataMember]
        public string AgeNow_Fmt { get; set; }
        [DataMember]
        public string CountAsWash_Fmt { get; set; }
        [DataMember]
        public string CountAsDry_Fmt { get; set; }
        [DataMember]
        public string MeterType { get; set; }
        [DataMember]
        public string Sleeved { get; set; }
        [DataMember]
        public string VendingMethod { get; set; }
        [DataMember]
        public string FuelType { get; set; }
        [DataMember]
        public string VendPrice_Fmt { get; set; }
        [DataMember]
        public string CycleTime_Fmt { get; set; }
        [DataMember]
        public string CurrentCondition { get; set; }
        [DataMember]
        public string InstallDate_Fmt { get; set; }
        [DataMember]
        public string LoadType { get; set; }




    }
    [DataContract]
    public class Machinedetailsbywebid
    {
        [DataMember]
        public string EquipmentType { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string WebId { get; set; }
        [DataMember]
        public string RoomId { get; set; }
        [DataMember]
        public string SerialNumber { get; set; }
        [DataMember]
        public string Manufacturer { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public string ContractType { get; set; }
        [DataMember]
        public string BillingMethod { get; set; }
        [DataMember]
        public string FirstInstallDate_Fmt { get; set; }
        [DataMember]
        public string AgeNow_Fmt { get; set; }
        [DataMember]
        public string InstallDate_Fmt { get; set; }
        [DataMember]
        public string TimeOnLoc_Fmt { get; set; }
        [DataMember]
        public string AgeWhenPurchased_Fmt { get; set; }
        [DataMember]
        public string PurchaseDate_Fmt  { get; set; }
        [DataMember]
        public string PurchaseCost_Fmt { get; set; }
        [DataMember]
        public string PurchaseCondition { get; set; }
        [DataMember]
        public string CurrentCondition { get; set; }
        [DataMember]
        public string OwnedBy { get; set; }
        [DataMember]
        public string ActualContract { get; set; }
        [DataMember]
        public string VendingMethod { get; set; }
        [DataMember]       
        public string LoadType { get; set; }
        [DataMember]
        public string FuelType { get; set; }
        [DataMember]
        public string Voltage { get; set; }
        [DataMember]
        public string MeterType { get; set; }
        [DataMember]
        public string CountAsWash_Fmt { get; set; }
        [DataMember]
        public string CountAsDry_Fmt { get; set; }
        [DataMember]
        public string VendPriceWash_Fmt { get; set; }
        [DataMember]
        public string VendPriceDry_Fmt { get; set; }
        [DataMember]
        public string CycleTime_Fmt { get; set; }
        [DataMember]
        public string SpecialPrice_Fmt { get; set; }
        [DataMember]
        public string SuperCycle { get; set; }
        [DataMember]
        public string SuperCyclePrice_Fmt { get; set; }
        [DataMember]
        public string Topoff { get; set; }
        [DataMember]
        public string TopoffPrice_Fmt { get; set; }        
        [DataMember]
        public string PrevWebId { get; set; }
        [DataMember]
        public string NextWebId { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Sleeved { get; set; }
        [DataMember]
        public string EquipmentDesc { get; set; }
        [DataMember]
        public string SiteCode { get; set; }
        [DataMember]
        public string Dispensing { get; set; }
        [DataMember]
        public string VendPrice_Fmt { get; set; }
    }
    [DataContract]
    public class Machineattributevalubywebid
    {
        [DataMember]
        public string GPAttributeId { get; set; }
        [DataMember]
        public string AttributeValue { get; set; }
        
    }
}