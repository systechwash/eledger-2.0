﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace eLedgerEntities
{
    [DataContract]
    public class Alert
    {
         [DataMember]
         public int DisplayOrder { get; set; }
         [DataMember]
         public int id { get; set; }
         [DataMember]
         public string group2 { get; set; }
         [DataMember]
         public string group1 { get; set; }
         [DataMember]
         public string ULN { get; set; }
         [DataMember]
         public string DisplayText { get; set; }
         [DataMember]
         public string SubType { get; set; }
         [DataMember]
         public int CategoryCount { get; set; }
         [DataMember]
         public int AlertInstNum { get; set; }
         [DataMember]
         public string Source { get; set; }
         [DataMember]
         public string Sort { get; set; }
         [DataMember]
         public string Message { get; set; }
         [DataMember]
         public string Destination { get; set; }
         [DataMember]
         public string CreatedDate { get; set; }
         [DataMember]
         public string DueDate { get; set; }
         [DataMember]
         public string ClearedDate { get; set; }
         [DataMember]
         public string StatusText { get; set; }
         [DataMember]
         public string AlertType { get; set; }
         [DataMember]
         public int RepeatNum { get; set; }
         [DataMember]
         public int SequenceNum { get; set; }
         [DataMember]
         public string FromUserID { get; set; }
         [DataMember]
         public string ToUserID { get; set; }
         [DataMember]
         public string AlertMSG { get; set; }
         [DataMember]
         public int isPrivate { get; set; }
         [DataMember]
         public string SequenceType { get; set; }
         [DataMember]
         public string Staus { get; set; }
         [DataMember]
         public string Clear { get; set; }
         [DataMember]
         public string FullName { get; set; }
         [DataMember]
         public string FutureFlag { get; set; }
         [DataMember]
         public string HistoryFlag { get; set; }
         [DataMember]
         public int rows { get; set; }
         [DataMember]
         public int page { get; set; }
         [DataMember]
         public List<Location> alertLocation { get; set; }
          [DataMember]
         public string AlertStatus { get; set; }
          [DataMember]
          public int sentByMe { get; set; }
          [DataMember]
          public int sentToMe { get; set; }
          [DataMember]
          public int Alert_EventNum { get; set; }
    }
}