﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace eLedgerEntities
{
      [DataContract]
    public class Service
    {

          [DataMember]
          public string ULN { get; set; }
          [DataMember]
          public int m03 { get; set; }
          [DataMember]
          public int m12 { get; set; }
          [DataMember]
          public int m24 { get; set; }
          [DataMember]
          public int maxDays { get; set; }
          [DataMember]
          public string TotPhysicalCountAs { get; set; }
          [DataMember]
          public string AvgMachineAge { get; set; }
          [DataMember]
          public string CallsPerMachine_Fmt { get; set; }         
          [DataMember]
          public string AvgTimeOnLoc { get; set; }
          [DataMember]
          public string AvgCyclesPerDay { get; set; }
         [DataMember]
          public string AvgCyclesPerCall { get; set; }
           [DataMember]
        public string AvgRepairsPerMachine { get; set; }
           [DataMember]
        public string TotCalls { get; set; }
           [DataMember]
        public string TotRepairs { get; set; }
           [DataMember]
        public string AvgResponseTime { get; set; }
           [DataMember]
        public string ServiceGoal { get; set; }
           [DataMember]
        public string Proximity { get; set; }
           [DataMember]
        public string Risk { get; set; }
           [DataMember]
        public string TotClaimsExpense { get; set; }
           [DataMember]
        public string TotExpenses { get; set; }
           [DataMember]
        public string TotPartsExpense { get; set; }
           [DataMember]
        public string TotRefundExpense { get; set; }
           [DataMember]
        public List<Display> displaydata { get; set; }
           [DataMember]
        public string   ResponseHoursInit { get; set; }
           [DataMember]
        public string Appointment { get; set; }
           [DataMember]
        public string WebId { get; set; }
           [DataMember]
        public string Resolution { get; set; }
           [DataMember]
        public string PartNumber { get; set; }
           [DataMember]
        public string Technician { get; set; }
           [DataMember]
        public string Cost { get; set; }
           [DataMember]
           public string EquipmentType { get; set; }
           [DataMember]
           public string SerialNumber { get; set; }
           [DataMember]
           public string Manufacturer { get; set; }
           [DataMember]
           public string Model { get; set; }
           [DataMember]
           public string ContractType { get; set; }
           [DataMember]
           public string BillingMethod { get; set; }
           [DataMember]
           public string FirstInstallDate_Fmt { get; set; }
           [DataMember]
           public string AgeNow_Fmt { get; set; }
           [DataMember]
           public string InstallDate_Fmt { get; set; }
           [DataMember]
           public string TimeOnLoc_Fmt { get; set; }
           [DataMember]
           public string PurchaseDate_Fmt { get; set; }
           [DataMember]
           public string AgeWhenPurchased_Fmt { get; set; }
           [DataMember]
           public string PurchaseCost_Fmt { get; set; }
           [DataMember]
           public string PurchaseCondition { get; set; }
           [DataMember]
           public string CurrentCondition { get; set; }
           [DataMember]
           public string OwnedBy { get; set; }
           [DataMember]
           public string ActualContract { get; set; }
           [DataMember]
           public string VendingMethod { get; set; }
           [DataMember]
           public string LoadType { get; set; }
           [DataMember]
           public string FuelType { get; set; }
           [DataMember]
           public string Voltage { get; set; }
           [DataMember]
           public string MeterType { get; set; }
           [DataMember]
           public string Sleeved { get; set; }
           [DataMember]
           public string CountAsWash_Fmt { get; set; }
           [DataMember]
           public string CountAsDry_Fmt { get; set; }
           [DataMember]
           public string VendPriceWash_Fmt { get; set; }
           [DataMember]
           public string VendPriceDry_Fmt { get; set; }
           [DataMember]
           public string CycleTime_Fmt { get; set; }
           [DataMember]
           public string SpecialPrice_Fmt { get; set; }
           [DataMember]
           public string SuperCycle { get; set; }
           [DataMember]
           public string SuperCyclePrice_Fmt { get; set; }
           [DataMember]
           public string Topoff { get; set; }
           [DataMember]
           public string TopoffPrice_Fmt { get; set; }
           [DataMember]
           public string RoomId { get; set; }
           [DataMember]
           public string Status { get; set; }
           [DataMember]
           public List<ServiceRepair> ServiceRep { get; set; }
           [DataMember]
           public List<ServicePriceChanges> ServicePC { get; set; }
           [DataMember]
           public List<ServiceMaintenance> ServiceM { get; set; }
           [DataMember]
           public List<ServiceMachineMovement> ServiceMM { get; set; }
           [DataMember]
           public List<ServiceDetail> ServiceDet { get; set; }
           [DataMember]
           public List<ServiceAll> ServiceAll { get; set; }
           [DataMember]
           public List<Equipment> Eq { get; set; }
           //[DataMember]
           //public List<Machinedetailsbywebid> EqService { get; set; }
           //[DataMember]
           //public List<Machineattributevalubywebid> EqServiceDesc { get; set; }
           [DataMember]
           public string ScheduleDate_Fmt { get; set; }
           [DataMember]
           public string CompletionDate_Fmt { get; set; }
           [DataMember]
           public string RecordType { get; set; }
           

    }
[DataContract]
      public class ServiceRepair
      {
          [DataMember]
          public string ServiceCallId { get; set; }
          [DataMember]
          public string ResponseHoursInit { get; set; }
          [DataMember]
          public string RoomId { get; set; }
          [DataMember]
          public string ScheduleDate_Fmt { get; set; }
          [DataMember]
          public string CompletionDate_Fmt { get; set; }
          [DataMember]
          public string Status { get; set; }
          [DataMember]
          public string RecordType { get; set; }
          [DataMember]
          public string Description { get; set; }
          [DataMember]
          public List<ServiceDetail> ServiceDet { get; set; }
      }
[DataContract]
public class ServicePriceChanges
{
    [DataMember]
    public string ServiceCallId { get; set; }
    [DataMember]
    public string ResponseHoursInit { get; set; }
    [DataMember]
    public string RoomId { get; set; }
    [DataMember]
    public string ScheduleDate_Fmt { get; set; }
    [DataMember]
    public string CompletionDate_Fmt { get; set; }
    [DataMember]
    public string Status { get; set; }
    [DataMember]
    public string RecordType { get; set; }
    [DataMember]
    public string Description { get; set; }
    [DataMember]
    public List<ServiceDetail> ServiceDet { get; set; }
}
[DataContract]
public class ServiceAll
{
    [DataMember]
    public string ServiceCallId { get; set; }
    [DataMember]
    public string ResponseHoursInit { get; set; }
    [DataMember]
    public string RoomId { get; set; }
    [DataMember]
    public string ScheduleDate_Fmt { get; set; }
    [DataMember]
    public string CompletionDate_Fmt { get; set; }
    [DataMember]
    public string Status { get; set; }
    [DataMember]
    public string RecordType { get; set; }
    [DataMember]
    public string Description { get; set; }
    [DataMember]
    public List<ServiceDetail> ServiceDet { get; set; }
}
[DataContract]
public class ServiceMaintenance
{
    [DataMember]
    public string ServiceCallId { get; set; }
    [DataMember]
    public string ResponseHoursInit { get; set; }
    [DataMember]
    public string RoomId { get; set; }
    [DataMember]
    public string ScheduleDate_Fmt { get; set; }
    [DataMember]
    public string CompletionDate_Fmt { get; set; }
    [DataMember]
    public string Status { get; set; }
    [DataMember]
    public string RecordType { get; set; }
    [DataMember]
    public string Description { get; set; }
    [DataMember]
    public List<ServiceDetail> ServiceDet { get; set; }
}
[DataContract]
public class ServiceMachineMovement
{
    [DataMember]
    public string ServiceCallId { get; set; }
    [DataMember]
    public string ResponseHoursInit { get; set; }
    [DataMember]
    public string RoomId { get; set; }
    [DataMember]
    public string ScheduleDate_Fmt { get; set; }
    [DataMember]
    public string CompletionDate_Fmt { get; set; }
    [DataMember]
    public string Status { get; set; }
    [DataMember]
    public string RecordType { get; set; }
    [DataMember]
    public string Description { get; set; }
}
[DataContract]
public class ServiceDetail
{
    [DataMember]
    public string Appointment { get; set; }
    [DataMember]
    public string RoomId { get; set; }
    [DataMember]
    public string ScheduleDate_Fmt { get; set; }
    [DataMember]
    public string CompletionDate_Fmt { get; set; }
    [DataMember]
    public string Status { get; set; }
    [DataMember]
    public string WebId { get; set; }
    [DataMember]
    public string ShowWebIdlink { get; set; }
    [DataMember]
    public string Resolution { get; set; }
    [DataMember]
    public string PartNumber { get; set; }
    [DataMember]
    public string Technician { get; set; }
    [DataMember]
    public string RecordType { get; set; }
    [DataMember]
    public string Cost { get; set; }
    [DataMember]
    public string Vehicle { get; set; }

    [DataMember]
    public string Ticketid { get; set; }

}
}
    

