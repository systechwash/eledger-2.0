﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eLedgerEntities
{
    public class User
    {
       public string  FirstName { get; set; }
       public string LastName { get; set; }
       public string FullName { get; set; }
       public string NetworkLogin { get; set; }
       public string RoleId { get; set; }
       public string Id { get; set; }
       public string BranchList { get; set; }
       public int UserID { get; set; }
       public string Display_Name { get; set; }
    }
}