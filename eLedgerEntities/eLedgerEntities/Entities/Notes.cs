﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace eLedgerEntities
{
    [DataContract]
    public class Notes
    {
        [DataMember]
        public System.DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedById { get; set; }
        [DataMember]
        public System.DateTime UpdatedDate { get; set; }
        [DataMember]
        public string UpdatedById { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string SourceCode { get; set; }
        [DataMember]
        public string NoteDate { get; set; }
        [DataMember]
        public string NoteText { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
         [DataMember]
        public int CanEdit { get; set; }
         [DataMember]
         public string Id { get; set; }
        
    }
}