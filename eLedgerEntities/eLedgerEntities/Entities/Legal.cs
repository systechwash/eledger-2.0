﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

 namespace eLedgerEntities

{
     [DataContract]
    public class Legal
    {
         [DataMember]
         public string ULN { get; set; }
         [DataMember]
        public List<LegalDetail> LegalDet { get; set; }
         [DataMember]
         public List<LegalMaster> LegalMas { get; set; }
         [DataMember]
         public int CanEdit { get; set; }
         [DataMember]
         public int hasLegalAccess { get; set; }
         
    }
     [DataContract]
     public class LegalMaster
     {
         [DataMember]
         public string ThreadId { get; set; }
         [DataMember]
         public string status { get; set; }
         [DataMember]
         public System.DateTime NoteDate { get; set; }
         [DataMember]
         public string NoteDate_Fmt { get; set; }
         [DataMember]
         public string NoteText { get; set; }
         [DataMember]
         public string CreatedBy { get; set; }
         [DataMember]
         public string legalInformation { get; set; }
         [DataMember]
         public string visibility { get; set; }
         [DataMember]
         public Int16 ledgerStatus { get; set; }
         [DataMember]
         public string IsMaster { get; set; }
         [DataMember]
         public List<LegalDetail> LegalDet { get; set; }
         [DataMember]
         public int CanEdit { get; set; }
         
     }

     [DataContract]
    public class LegalDetail
    {
         [DataMember]
         public string ThreadId { get; set; }
        [DataMember]
        public string NoteDate_Fmt { get; set; }
        [DataMember]
        public string NoteText { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int CanEdit { get; set; }
    }
}