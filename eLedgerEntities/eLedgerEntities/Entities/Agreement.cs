﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace eLedgerEntities
{
    [DataContract]
    public class Agreement
    {
        [DataMember]
        public string AgreementUser { get; set; }
        [DataMember]
        public string RenewalType { get; set; }
        [DataMember]
        public string AgreementType { get; set; }
        [DataMember]
        public string CommissionType { get; set; }
        [DataMember]
        public string TotalCount { get; set; }
        [DataMember]
        public string CPIFlag { get; set; }
        [DataMember]
        public string CPIEffectiveDate { get; set; }
        [DataMember]
        public string MgrAll { get; set; }
        [DataMember]
        public string MachineType1 { get; set; }
        [DataMember]
        public string MachineType2 { get; set; }
        [DataMember]
        public string MachineType3 { get; set; }
        [DataMember]
        public string MachineType4 { get; set; }
        [DataMember]
        public string MachineType5 { get; set; }
        [DataMember]
        public string MachineType6 { get; set; }
        [DataMember]
        public string MachineType7 { get; set; }
        [DataMember]
        public string MachineNum1 { get; set; }
         [DataMember]
        public string MachineNum2 { get; set; }
         [DataMember]
        public string MachineNum3 { get; set; }
        [DataMember]
        public string MachineNum4 { get; set; }
        [DataMember]
        public string MachineNum5 { get; set; }
        [DataMember]
        public string MachineNum6 { get; set; }
        [DataMember]
        public string MachineNum7 { get; set; }
        [DataMember]
        public string MachTotCnt { get; set; }
        [DataMember]
        public string RentalPrice1 { get; set; }
        [DataMember]
        public string RentalPrice2 { get; set; }
        [DataMember]
        public string RentalPrice3 { get; set; }
        [DataMember]
        public string RentalPrice4 { get; set; }
        [DataMember]
        public string RentalPrice5 { get; set; }
        [DataMember]
        public string RentalPrice6 { get; set; }
        [DataMember]
        public string RentalPrice7 { get; set; }
        [DataMember]
        public string RentalSum { get; set; }
        [DataMember]
        public string EnteredIntoDate { get; set; }
        [DataMember]
        public string CommencedDate { get; set; }
        [DataMember]
        public string ExpirationDate { get; set; }
        [DataMember]
        public string InitialTerm { get; set; }
        [DataMember]
        public string RenewalTerm { get; set; }
        [DataMember]
        public string RenewalNum { get; set; }
        [DataMember]
        public string Billing { get; set; }
        [DataMember]
        public string PaymentType { get; set; }
        [DataMember]
        public string ccLastDigits { get; set; }
        [DataMember]
        public string Gpm1 { get; set; }
        [DataMember]
        public string Gpm2 { get; set; }
        [DataMember]
        public string Gpm3 { get; set; }
        [DataMember]
        public string Gpm4 { get; set; }
        [DataMember]
        public string Gpm5 { get; set; }
        [DataMember]
        public string Gpm6 { get; set; }
        [DataMember]
        public string Gpm7 { get; set; }
        [DataMember]
        public string Gpm8 { get; set; }
        [DataMember]
        public string CommPercent1 { get; set; }
        [DataMember]
        public string CommPercent2 { get; set; }
        [DataMember]
        public string CommPercent3 { get; set; }
        [DataMember]
        public string CommPercent4 { get; set; }
        [DataMember]
        public string CommPercent5 { get; set; }
        [DataMember]
        public string CommPercent6 { get; set; }
        [DataMember]
        public string CommPercent7 { get; set; }
        [DataMember]
        public string CommPercent8 { get; set; }
        [DataMember]
        public string Minimum { get; set; }
        [DataMember]
        public string AdvCommAmt { get; set; }
        [DataMember]
        public string PrePaidAmt { get; set; }
        [DataMember]
        public string BonusAmt { get; set; }
        [DataMember]
        public string NoteDate_Fmt { get; set; }
        [DataMember]
        public string NoteText { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public List<Notes> Notedetails { get; set; }
    }
}