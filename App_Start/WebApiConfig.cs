﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace eLedgerMVC
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
               name: "LocationApi",
               routeTemplate: "api/{controller}/{action}/{searchParam}",
               defaults: new
               {
                   controller = "Location",
                   action="SearchLocation",
                   searchParam = RouteParameter.Optional
               }
               );

            config.Routes.MapHttpRoute(
              name: "AgreementApi",
              routeTemplate: "api/{controller}/{action}/{ULN}",
              defaults: new
              {
                  controller = "Agreement",
                  action = "getAgreementDetails",
                  ULN = RouteParameter.Optional
              }
              );
        }
    }
}
