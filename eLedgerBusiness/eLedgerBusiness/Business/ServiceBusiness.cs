﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerBusiness.DAL;
using System.Net;
using eLedgerEntities;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace eLedgerBusiness
{

    public class ServiceBusiness
    {
        public string getJsonServiceSummary(string uln)
        {
            try
            {
                int maxDays = 0;
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(uln);
                DataTable dt = new WashDAL().getDataTable("serviceMonths", paramText, paramValues);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow reader in dt.Rows)
                    {
                        if (reader["MaxDays"].ToString().Trim() != "")
                            maxDays = Convert.ToInt32(reader["MaxDays"].ToString());
                        else
                            maxDays = 0;
                    }
                }
                else
                    maxDays = 0;

                string Json = "\"ServiceSummary\":" + new WashDAL().getJsonFromQuery("serviceSummary", paramText, paramValues) + ",\"maxDays\":\"" + maxDays.ToString() + "\"" + "," + ServiceCategoryPie(uln, "12");
                return Json;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ServiceSummary", "ULN:" + uln);
                return null;
            }
        }

        public DataTable getServiceDetailexport(string serviceCallId)
        {
            try
            {
                DataTable dt = new DataTable();
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ServiceCallId");
                paramValues.Add(serviceCallId);
                dt = new WashDAL().getDataTable("getServicecategory", paramText, paramValues);
                return dt;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ServiceExportTicket", "TicketNo:" + serviceCallId);
                return null;
            }
        }

        public string getJsonServiceDetails(string uln, string dateWindow)
        {
            try
            {
                int rowCountSerRep = 0;
                int rowCountSerM = 0;
                int rowCountSerMM = 0;
                int rowCountSerAll = 0;
                int rowCountSerPC = 0;
                if (dateWindow == "")
                {
                    dateWindow = "90";
                }

                ArrayList paramValues1 = new ArrayList();
                ArrayList paramText1 = new ArrayList();
                paramText1.Add("@ULN");
                paramText1.Add("@Days");
                paramValues1.Add(uln);
                paramValues1.Add(Convert.ToInt32(dateWindow));

                string Seriliazedatat = "";

                DataTable dt = new WashDAL().getDataTable("serviceRecords", paramText1, paramValues1);
                DataTable dtServiceRepair = new DataTable();
                DataTable dtServiceMaintenance = new DataTable();
                DataTable dtServiceMachMove = new DataTable();
                DataTable dtServicePriceChange = new DataTable();
                DataSet ds = new DataSet();

                if (dt.Rows.Count > 0)
                {
                    DataView datavw = new DataView();
                    datavw = dt.DefaultView;
                    datavw.RowFilter = "RecordType='Service/Repair'";
                    if (datavw.Count > 0)
                    {
                        dtServiceRepair = datavw.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerRep";
                        dtServiceRepair.Columns.Add(newColumn);
                    }
                    DataView datavw1 = new DataView();
                    datavw1 = dt.DefaultView;
                    datavw1.RowFilter = "RecordType='Maintenance'";
                    if (datavw1.Count > 0)
                    {
                        dtServiceMaintenance = datavw1.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerM";
                        dtServiceMaintenance.Columns.Add(newColumn);

                    }
                    DataView datavw2 = new DataView();
                    datavw2 = dt.DefaultView;
                    datavw2.RowFilter = "RecordType='Machine Movement'";
                    if (datavw2.Count > 0)
                    {
                        dtServiceMachMove = datavw2.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerMM";
                        dtServiceMachMove.Columns.Add(newColumn);

                    }
                    DataView datavw3 = new DataView();
                    datavw3 = dt.DefaultView;
                    datavw3.RowFilter = "RecordType='Price Change'";
                    if (datavw3.Count > 0)
                    {
                        dtServicePriceChange = datavw3.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerPc";
                        dtServicePriceChange.Columns.Add(newColumn);

                    }
                    System.Data.DataColumn newColumn1 = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                    newColumn1.DefaultValue = "SerAll";
                    dt.Columns.Add(newColumn1);
                    ds.Tables.Add(dt);
                    ds.Tables.Add(dtServiceRepair);
                    ds.Tables.Add(dtServiceMaintenance);
                    ds.Tables.Add(dtServiceMachMove);
                    ds.Tables.Add(dtServicePriceChange);

                    ds.Tables[0].TableName = "ServiceAll";
                    ds.Tables[1].TableName = "ServiceRepair";
                    ds.Tables[2].TableName = "Maintenance";
                    ds.Tables[3].TableName = "MachineMovement";
                    ds.Tables[4].TableName = "PriceChanges";

                    Seriliazedatat = Newtonsoft.Json.JsonConvert.SerializeObject(ds, Formatting.Indented);

                    rowCountSerAll = ds.Tables[0].Rows.Count;
                    rowCountSerRep = ds.Tables[1].Rows.Count;
                    rowCountSerM = ds.Tables[2].Rows.Count;
                    rowCountSerMM = ds.Tables[3].Rows.Count;
                    rowCountSerPC = ds.Tables[4].Rows.Count;
                }
                ds.Dispose();
                string Json = Seriliazedatat;
                if (Json.Length > 0)
                    Json = Json.Substring(0, Json.Length - 1);
                else
                    Json = "{" + "\"ServiceAll\":\"" + "[]" + "\"" + ",\"ServiceRepair\":\"" + "[]" + "\"" + ",\"Maintenance\":\"" + "[]" + "\"" + ",\"MachineMovement\":\"" + "[]" + "\"" + ",\"PriceChanges\":\"" + "[]" + "\"";
                Json += ",\"PriceChangesTotal\":\"" + rowCountSerPC + "\" " + ",\"MaintenanceTotal\":\"" + rowCountSerM + "\",\"MachineMovementTotal\":\"" + rowCountSerMM + "\"," + "\"ServiceRepairTotal\":\"" + rowCountSerRep + "\"," + "\"ServiceAllTotal\":\"" + rowCountSerAll + "\"" + "}";

                return Json;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ServiceDetails", "uln:" + uln + "dateWindow:" + dateWindow);
                return null;
            }
        }

        public DataSet getexporttoexcel(string uln, string dateWindow)
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt;
                DataTable dtlocation;
                DataTable dtservice;
                string query = "serviceRecordsExport";
                ArrayList paramText = new ArrayList();
                ArrayList paramValues = new ArrayList();
                ArrayList paramText2 = new ArrayList();
                ArrayList paramValues2 = new ArrayList();
                paramText.Add("@ULN");
                paramText.Add("@Days");
                string columns = "ServiceCallId As TicketNo,RoomId As Room,CONVERT(VARCHAR(8), CreatedDate_Fmt, 1)As [Created Date],CompletionDate_Fmt As[Closed Date],ResponseHoursInitial_Fmt As [Response (work hrs)],Status,CallType As Type,Description As Complaint,LEFT(ResponseHoursInitial_Fmt, LEN(ResponseHoursInitial_Fmt) - 1) as responsehours";
                paramValues.Add(uln);
                paramValues.Add(Convert.ToInt32(dateWindow));

                paramText2.Add("@ULN");
                paramValues2.Add(uln);
                paramText2.Add("@Mnth");
                paramValues2.Add(Convert.ToInt32("12"));

                DataSet serviceexport = new DataSet();
                ArrayList paramText1 = new ArrayList();
                paramText1.Add("@ULN");
                ArrayList paramValues1 = new ArrayList();
                paramValues1.Add(uln);
                dtlocation = new WashDAL().getDataTable("getlocationSummary", paramText1, paramValues1);
                serviceexport = new WashDAL().getdatasetfromColumn(query, paramText, paramValues, columns);
                serviceexport.Tables[0].TableName = "ServiceDetails";
                serviceexport.Tables[1].TableName = "AvgresponseTime";
                dtservice = new WashDAL().getDataTable("serviceSummary", paramText1, paramValues1);
                DataTable dtchart = new WashDAL().getDataTable("serviceCategoryList", paramText2, paramValues2);
                ds.Tables.Add(dtlocation);
                ds.Tables.Add(serviceexport.Tables[0].Copy());
                ds.Tables.Add(dtservice);
                ds.Tables.Add(dtchart);
                ds.Tables.Add(serviceexport.Tables[1].Copy());
                return ds;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ServiceExport", "ULN:" + uln + "Date:" + dateWindow);
                return null;
            }
        }
        public string getServiceDetail(string serviceCallId, string tblID)
        {
            try
            {                
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ServiceCallId");
                paramValues.Add(serviceCallId);
                DataTable dt = new WashDAL().getDataTable("serviceDetail", paramText, paramValues);
                string json="";
                dt.TableName = "ServiceCallIDDetils";
                if (dt.Rows.Count > 0)
                {
                    json = "{" + "\"ServiceCallIDDetils\":" + Newtonsoft.Json.JsonConvert.SerializeObject(dt, Formatting.Indented) + "}";
                }
                else
                    json = "\"ServiceCallIDDetils\":[]";
                
                return json;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ServiceTickno", "serviceCallId:" + serviceCallId + "tblID:" + tblID);
                return null;
            }
        }

        public string ServiceCategoryPie(string uln, string month)
        {
            try
            {
                string queryCodeSummary = "serviceCategoryList";
                int Mnth;
                if (month == "")
                    Mnth = 12;
                else
                    Mnth = Convert.ToInt32(month);
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(uln);
                paramText.Add("@Mnth");
                paramValues.Add(Mnth);
                int Totalmachincerepair = 0;
                DataTable dt = new WashDAL().getDataTable(queryCodeSummary, paramText, paramValues);
                StringBuilder sbJson = new StringBuilder();
                sbJson.Append("\"ServiceCategories\":[{ ");
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["ServiceRepairCategory"].ToString().Trim() == "Building / LR Related")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Building_Issue", "Building / LR Issues");
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Building_Issue_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Building_Issue_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "User / Customer Related")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Customer_Generated_Repair", "Customer Education Tickets");
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Customer_Generated_Repair_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\",  ", "Customer_Generated_Repair_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "Internal Company Business")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Internal_Company_Business", dr["ServiceRepairCategory"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Internal_Company_Business_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Internal_Company_Business_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "Mechanical Related")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Mechanical_Repair", "Machine Repairs");
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Mechanical_Repair_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Mechanical_Repair_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "Unknown")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Unknown", dr["ServiceRepairCategory"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Unknown_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Unknown_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    Totalmachincerepair = Totalmachincerepair + Convert.ToInt32(dr["ServiceRepairCount"]);
                }

                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Total_Machine", Totalmachincerepair);
                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Total_Machine_Percentage", "100%");


                if (sbJson.ToString() != "\"ServiceCategories\":[{ ")
                {
                    sbJson.Replace(", ", "", sbJson.Length - 2, 2);
                    sbJson.Append("}],");
                }
                else
                {
                    sbJson.Remove(sbJson.Length - 2, 2);
                    sbJson.Append("],");
                }

                sbJson.Append("\"Servicecategorypie\":[");
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["ServiceRepairCount"].ToString().Trim() != "0")
                    {
                        sbJson.Append("{");
                        if (dr["ServiceRepairCategory"].ToString().Trim() == "Mechanical Related")
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", "Machine Repairs");
                        else if (dr["ServiceRepairCategory"].ToString().Trim() == "User / Customer Related")
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", "Customer Education Tickets");
                        else if (dr["ServiceRepairCategory"].ToString().Trim() == "Building / LR Related")
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", "Building / LR Issues");
                        else
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", dr["ServiceRepairCategory"].ToString().Trim());
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "value", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\" ", "series1", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString());
                        sbJson.Append("}, ");
                    }
                }
                sbJson.Replace(", ", "", sbJson.Length - 2, 2);
                sbJson.Append("]");
                return sbJson.ToString();
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ServiceChart", "uln:" + uln + "month:" + month);
                return null;
            }
        }

        public string getviewRefundURL(string ULN)
        {
            string viewJsonRefundURL = eLedgerBusiness.Utils.ReadWebConfig.ViewRefund() + ULN;
            StringBuilder sbJson = new StringBuilder("{");
            sbJson.Append("\"viewRefundURL\":[{");
            sbJson.AppendFormat("\"{0}\":\"{1}\" ", "URL", viewJsonRefundURL);
            sbJson.Append("}]}");
            return sbJson.ToString();

        }

        public Array getMMRURL()
        {
            string MMRURL = eLedgerBusiness.Utils.ReadWebConfig.ViewMMR();
            List<String> strList = new List<String>();
            strList.Add(MMRURL);
            Array data = null;
            data = strList.ToArray();
            return data;
        }
    }
}
