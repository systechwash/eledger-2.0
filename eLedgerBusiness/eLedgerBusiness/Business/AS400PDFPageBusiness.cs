﻿using eLedgerBusiness.DAL;
using eLedgerEntities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace eLedgerBusiness
{
    public class AS400PDFPageBusiness
    {
        public MemoryStream getas400pdf(string uln)
        {
            try
            {
                byte[] byteArray = new byte[0];
                string AS400LedgersPath = eLedgerBusiness.Utils.ReadWebConfig.AS400LedgersPath() + uln + ".gif";
                System.Drawing.Image myImage = null;
                myImage = System.Drawing.Image.FromFile(AS400LedgersPath);
                MemoryStream stream = new MemoryStream();
                myImage.Save(stream, System.Drawing.Imaging.ImageFormat.Gif);
                stream.Close();
                byteArray = stream.ToArray();

                return stream;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "As400", "ULN:" + uln);
                return null;
            }
        }
        public string getas400pdfdetails(string uln)
        {
            string AS400LedgersPath = eLedgerBusiness.Utils.ReadWebConfig.AS400LedgersPath() + uln + ".gif";
            return AS400LedgersPath;
        } 
    }
}