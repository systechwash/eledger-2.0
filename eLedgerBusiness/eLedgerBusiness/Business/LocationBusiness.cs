﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using eLedgerEntities;
using eLedgerBusiness.DAL;
using System.Net;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using System.Web.Script.Serialization;


namespace eLedgerBusiness
{
    public class LocationBusiness
    {
        protected SqlCommand dbCommand;
        protected SqlDataAdapter dbDataAdapter;

        public string getLocationSummaryJson(string uln)
        {
            User user1 = new User();
            string queryCode = "";
            string POD = "";
            string MaxPODDiv = "";
            string DoNotIncrease = "";
           //string UserName = System.Web.HttpContext.Current.User.Identity.Name;
           string UserName = "washlaundry\\vmuthuswamy";
            if (UserName == null || UserName == "")
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            user1 = new UserCredentials().GetUser(UserName);
            ArrayList paramText2 = new ArrayList();
            ArrayList paramValues2 = new ArrayList();
            if ((user1.BranchList == "ALL") || (user1.BranchList == "*"))
            {
                queryCode = "getPODAll";
                paramText2.Add("@ULN");
                paramValues2.Add(uln);
            }
            else if (user1.BranchList == "")
            {
                paramText2.Add("@ULN");
                paramValues2.Add(uln);
                queryCode = "getPODEmpty";
            }
            else if (user1.BranchList == "LUR")
            {
                queryCode = "getPODLUR";
                paramText2.Add("@ULN");
                paramValues2.Add(uln);
                paramText2.Add("@Id");
                paramValues2.Add(user1.UserID);
            }
            else
            {
                queryCode = "getPODBrCode";
                paramText2.Add("@ULN");
                paramValues2.Add(uln);
                paramText2.Add("@BranchList");
                paramValues2.Add(user1.BranchList);
            }
            DataTable dt;
            dt = new WashDAL().getDataTable(queryCode, paramText2, paramValues2);
            foreach (DataRow row1 in dt.Rows)
            {

                if (row1["MaxPOD"].ToString().Trim().Length > 0)
                    POD = "1";
                else
                    POD = "0";
                MaxPODDiv = row1["MaxPOD"].ToString().Trim().ToUpper();

            }
            DataSet ds = new DataSet("Location");
            User user = new User();
            ArrayList paramValues = new ArrayList();
            DataTable dtSummary;
            DataTable dtSalesTeam;
            string queryCodeSummary = "getlocationSummary";
            string queryCodeSalesTeam = "getSalesTeam";

            ArrayList paramText = new ArrayList();
            Display formatColumn = new Display();
            paramText.Add("@ULN");
            paramValues.Add(uln);
            dtSummary = new WashDAL().getDataTable(queryCodeSummary, paramText, paramValues);
            dtSummary.TableName = "LocationSummary";
            dtSalesTeam = new WashDAL().getDataTable(queryCodeSalesTeam, paramText, paramValues);
            dtSalesTeam.TableName = "SalesTeam";
            string showRouteDayRoleList = eLedgerBusiness.Utils.ReadWebConfig.GetShowRouteDayRoleList();
            string showDay = "false";
            if (showRouteDayRoleList.Contains(user1.RoleId))
            {
                showDay = "true";
            }
            string mapFile = "";
            string siteMapsPath = eLedgerBusiness.Utils.ReadWebConfig.GetSiteMapsPath() + uln + "Map.pdf";
            string siteMapsURL = eLedgerBusiness.Utils.ReadWebConfig.GetSiteMapsURL() + uln + "Map.pdf";
            try
            {
                if (RemoteFileExists(siteMapsPath))
                {
                    mapFile = siteMapsURL;
                }
            }
            catch
            {
            }
            dtSummary.Columns.Add("showDay", typeof(String));
            dtSummary.Columns.Add("mapFile", typeof(String));
            dtSummary.Columns.Add("Totaluser", typeof(Int32));
            dtSummary.Columns.Add("FirstName", typeof(String));
            dtSummary.Columns.Add("LastName", typeof(String));
            dtSummary.Columns.Add("SplInst", typeof(String));
            dtSummary.Columns.Add("POD", typeof(String));
            dtSummary.Columns.Add("MaxPODDiv", typeof(String));

            if (dtSummary.Rows.Count > 0)
            {
                dtSummary.Rows[0]["showDay"] = showDay;
                dtSummary.Rows[0]["mapFile"] = mapFile;
                dtSummary.Rows[0]["Totaluser"] = dtSalesTeam.Rows.Count;
                dtSummary.Rows[0]["POD"] = POD;
                dtSummary.Rows[0]["MaxPODDiv"] = MaxPODDiv;
                if (dtSalesTeam.Rows.Count > 0)
                {
                    dtSummary.Rows[0]["FirstName"] = dtSalesTeam.Rows[0]["FirstName"];
                    dtSummary.Rows[0]["LastName"] = dtSalesTeam.Rows[0]["LastName"];
                }
                if (dtSummary.Rows[0]["SpecialInstructions"].ToString().Trim() != "")
                {
                    dtSummary.Rows[0]["SplInst"] = dtSummary.Rows[0]["SpecialInstructions"].ToString();
                    if (dtSummary.Rows[0]["SpecialInstructions"].ToString().Trim().Length > 160)
                    {
                        dtSummary.Rows[0]["SpecialInstructions"] = dtSummary.Rows[0]["SpecialInstructions"].ToString().Substring(0, 160) + "............ ";
                    }
                }
            }
            if (dtSalesTeam.Rows.Count >= 1)
            {
                dtSalesTeam.Rows[0].Delete();
                dtSalesTeam.AcceptChanges();
            }
            //string dni="";
            //dni=dtSummary.Rows[0].Field<Int32>("DoNotIncreaseVendorPrice").ToString();
            StringBuilder sbJson = new StringBuilder("[");
            //if(dni!="0")
            //{
            //    sbJson.Append("{");
            //    sbJson.AppendFormat("\"{0}\":\"{1}\"", "DNI", dni);
            //    sbJson.Append("}");
            //}
            sbJson.Append("]");
            string jsonSummary = new WashDAL().GetJson(dtSummary);
            string jsonSalesTeam = new WashDAL().GetJson(dtSalesTeam.Copy());
            string Json = "\"LocationSummary\":" + jsonSummary + ",\"SalesTeam\":" + jsonSalesTeam + ",\"DoNotIncrease\":" + sbJson.ToString();
            return Json;
        }

        private bool RemoteFileExists(string url)
        {
            bool bResult = false;
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.UseDefaultCredentials = true;
                    Stream stream = client.OpenRead(url);
                    if (stream != null)
                        bResult = true;
                    else
                        bResult = false;
                    stream.Close();
                }
                catch
                {
                    bResult = false;
                }
            }
            return bResult;
        }

        public string formattedULN(string ULN)
        {
            if (ULN.Length > 8)
            {
                ULN = ULN.Insert(4, "-");
                ULN = ULN.Insert(7, "-");
                return ULN;
            }
            else
                return ULN;
        }
    }
}

      

    










