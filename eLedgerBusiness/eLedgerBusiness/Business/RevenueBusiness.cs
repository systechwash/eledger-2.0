﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using eLedgerBusiness.DAL;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
namespace eLedgerBusiness
{
    public class RevenueBusiness
    {
        Revenue revenue = new Revenue();
        StringBuilder sbJson = new StringBuilder();

        public string getRevenueheader(string uln, string splitRevenueNotes, string showRecords)
        {
            try
            {
                string queryCode = "";
                if (showRecords == "")
                {
                    showRecords = "Recent";
                }

                if (showRecords == "Recent")
                    queryCode = "revenueYearHeader";
                else if (showRecords == "All")
                    queryCode = "revenueYearHeaderAll";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);
                DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);

                StringBuilder sbJson = new StringBuilder();
                sbJson.Append("\"RevenueHeader\":[");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["CollectYear_Fmt"].ToString() != "2079C")
                        {
                            sbJson.Append("{");
                            if (row["RT"].ToString() == "C")
                            {
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "DisplayYear", row["CollectYear_Fmt"].ToString().TrimEnd('C'));
                            }
                            else
                            {
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "DisplayYear", row["CollectYear_Fmt"].ToString().TrimEnd('R'));
                            }
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RecordType", row["RT"].ToString());
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CollectYear_Fmt", row["CollectYear_Fmt"].ToString().TrimEnd('C'));
                            sbJson.Replace(", ", "},", sbJson.Length - 2, 2);
                        }
                        if (row["CollectYear_Fmt"].ToString() == "2079C")
                        {
                            sbJson.Append("{");
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "DisplayYear", "Pinned Notes");
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RecordType", row["RT"].ToString());
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CollectYear_Fmt", row["CollectYear_Fmt"].ToString().TrimEnd('C'));
                            sbJson.Replace(", ", "},", sbJson.Length - 2, 2);
                        }

                    }
                    sbJson.Replace("},", "}]", sbJson.Length - 2, 2);
                }
                else
                {
                    sbJson.Append("]");
                }
                return sbJson.ToString();
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "Revenueheader", "uln:" + uln + "splitRevenueNotes:" + splitRevenueNotes + "showRecords:" + showRecords);
                return null;
            }
        }

        public DataSet getRevenueExportdetails(string uln, string years)
        {
            try
            {
                DataTable dtlocation = new DataTable();
                string strYears = string.Empty;
                string[] revenueYear = years.Split(',');
                foreach (string year in revenueYear)
                {
                    if (strYears != string.Empty) strYears = strYears + ",";
                    if (year.Contains('R') || year.Contains('C'))
                        strYears = strYears + "\'" + year + "\'";
                    else
                        strYears = strYears + "\'" + year + "C\'";
                }

                string queryCode = "";
                queryCode = "revenueRecordsExport";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);

                DataTable dt = new WashDAL().getDataTableColumns(queryCode, paramText, paramValues, strYears);
                DataTable dtCommercial = dt.Clone();
                DataTable dtRevenue = dt.Clone();
                DataSet ds = new DataSet();
                dtCommercial.Columns.Remove("Document Number");
                dtCommercial.Columns.Remove("Code");
                dtCommercial.Columns.Remove("Description");
                dtCommercial.Columns.Remove("Invoice Date");
                dtCommercial.Columns.Remove("Invoice Amount");
                dtCommercial.Columns.Remove("Payment Date");
                dtCommercial.Columns.Remove("Payment Amount");

                dtRevenue.Columns.Remove("Cycle Date");
                dtRevenue.Columns.Remove("Cycle Days");
                dtRevenue.Columns.Remove("Gross Total");
                dtRevenue.Columns.Remove("Washer");
                dtRevenue.Columns.Remove("Dryer");
                dtRevenue.Columns.Remove("Paid Date");
                dtRevenue.Columns.Remove("Paid Amount");
                dtRevenue.Columns.Remove("Status");
                dtRevenue.Columns.Remove("Mgr");
                dtRevenue.Columns.Remove("BUW");
                dtRevenue.Columns.Remove("Gross Washer");
                dtRevenue.Columns.Remove("Gross Dryer");
                dtRevenue.Columns.Remove("Gross");
                dtRevenue.Columns.Remove("Net");

                ArrayList paramText1 = new ArrayList();
                paramText1.Add("@ULN");
                ArrayList paramValues1 = new ArrayList();
                paramValues1.Add(uln);
                dtlocation = new WashDAL().getDataTable("getlocationSummary", paramText1, paramValues1);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["RecordType"].ToString() == "C")
                    {
                        dtCommercial.ImportRow(dt.Rows[i]);
                    }
                    else if (dt.Rows[i]["RecordType"].ToString() == "R")
                    {
                        dtRevenue.ImportRow(dt.Rows[i]);
                    }
                }
                if (dtRevenue.Rows.Count > 0 && dtCommercial.Rows.Count <= 0)
                {
                    ds.Tables.Add(dtCommercial);
                    ds.Tables.Add(dtRevenue);
                    ds.Tables.Add(dtlocation);
                    return ds;
                }
                else if (dtCommercial.Rows.Count > 0 && dtRevenue.Rows.Count <= 0)
                {
                    ds.Tables.Add(dtCommercial);
                    ds.Tables.Add(dtRevenue);
                    ds.Tables.Add(dtlocation);
                    return ds;
                }
                else if (dtCommercial.Rows.Count > 0 && dtRevenue.Rows.Count > 0)
                {
                    ds.Tables.Add(dtCommercial);
                    ds.Tables.Add(dtRevenue);
                    ds.Tables.Add(dtlocation);
                    return ds;
                }
                else
                {
                    ds.Tables.Add(dtlocation);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "RevenueDetails", "uln:" + uln + "years:" + years);
                return null;
            }
        }

        public string getRevenuedetails(string uln, string splitRevenueNotes, string showRecords, string year)
        {
            try
            {
                string queryCode = "";

                string escClause = "ESCAPE '" + eLedgerBusiness.Utils.ReadWebConfig.EscapeCode() + "'";
                if (showRecords == "")
                {
                    showRecords = "Recent";
                }

                if (showRecords == "Recent")
                    queryCode = "getRevenue";
                else if (showRecords == "All")
                    queryCode = "getRevenueAll";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);
                DataTable dt = new WashDAL().getDataTableColumns(queryCode, paramText, paramValues, year);

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    if (dr["RecordType"].ToString() == "P")
                    {
                        if (dr["CollectDate_Fmt"].ToString() == "99/99/99")
                        {
                            row.Add("CollectDate_Fmt", dr["NoteText"].ToString().Substring(0, 8).Replace(System.Environment.NewLine, ""));
                            row.Add("DisplayYear_Fmt", "Pinned Notes");
                            row.Add("RecordType", dr["RecordType"].ToString().Trim());
                            row.Add("NoteText", dr["NoteText"].ToString().Substring(10).Replace(System.Environment.NewLine, ""));
                        }
                        else if (dr["CollectDate_Fmt"].ToString() != "99/99/99")
                        {
                            row.Add("CollectDate_Fmt", dr["CollectDate_Fmt"].ToString());
                            row.Add("DisplayYear_Fmt", "Pinned Notes");
                            row.Add("RecordType", dr["RecordType"].ToString().Trim());
                            row.Add("NoteText", dr["NoteText"].ToString().Replace(System.Environment.NewLine, ""));
                        }
                        row.Add("RowNo", dr["Row"].ToString().Trim());
                    }
                    else
                    {
                        row.Add("NetTotal", dr["NetTotal"].ToString());
                        row.Add("CycleDays", dr["CycleDays"].ToString());
                        row.Add("ThreadId", dr["ThreadId"].ToString());
                        row.Add("Visibility", dr["Visibility"].ToString());
                        row.Add("VendName", dr["VendName"].ToString());
                        row.Add("Closed", dr["Closed"].ToString());
                        row.Add("DocNum", dr["DocNum"].ToString());
                        row.Add("DocTypeCode", dr["DocTypeCode"].ToString());
                        row.Add("InvoiceDate_Fmt", dr["InvoiceDate_Fmt"].ToString());
                        row.Add("InvoiceAmount_Fmt", dr["InvoiceAmount_Fmt"].ToString());
                        row.Add("RecordType", dr["RecordType"].ToString().Trim());
                        row.Add("Comment", dr["Comment"].ToString().Trim());
                        row.Add("CountryID", dr["countryid"].ToString().Trim());
                        if (dr["NoteText"].ToString().Contains("\""))
                        {
                            row.Add("NoteText", (dr["NoteText"].ToString().Replace("\"", "")).Replace(System.Environment.NewLine, ""));
                            row.Add("CheckNoteText", dr["NoteText"].ToString().Substring(10).Replace(System.Environment.NewLine, ""));
                        }
                        else
                        {
                            row.Add("NoteText", dr["NoteText"].ToString().Trim().Replace(System.Environment.NewLine, ""));
                            if (dr["NoteText"].ToString().Length >= 10)
                                row.Add("CheckNoteText", dr["NoteText"].ToString().Substring(10).Replace(System.Environment.NewLine, ""));
                            else
                                row.Add("CheckNoteText", "");
                        }


                        if (dr["CheckStatus"].ToString() == null || dr["CheckStatus"].ToString() == "")
                            row.Add("CheckStatus", "");
                        else if (dr["CheckStatus"].ToString() == "P")
                            row.Add("CheckStatus", "Printed");
                        else if (dr["CheckStatus"].ToString() == "R")
                            row.Add("CheckStatus", "Ready");
                        else if (dr["CheckStatus"].ToString() == "S")
                            row.Add("CheckStatus", "Sent");
                        else if (dr["CheckStatus"].ToString() == "M")
                            row.Add("CheckStatus", "Mailed");
                        else if (dr["CheckStatus"].ToString() == "V")
                            row.Add("CheckStatus", "Void");
                        else
                            row.Add("CheckStatus", "");
                        row.Add("CollectDate_Fmt", dr["CollectDate_Fmt"].ToString().Trim());
                        row.Add("CycleDate_Fmt", dr["CycleDate_Fmt"].ToString().Trim());
                        row.Add("CycleDays_Fmt", dr["CycleDays_Fmt"].ToString().Trim());
                        row.Add("DisplayYear_Fmt", dr["DisplayYear_Fmt"].ToString().Trim());
                        row.Add("NetTotal_Fmt", dr["NetTotal_Fmt"].ToString().Trim());
                        row.Add("NetWasher_Fmt", dr["NetWasher_Fmt"].ToString().Trim());
                        row.Add("NetDryer_Fmt", dr["NetDryer_Fmt"].ToString().Trim());
                        row.Add("PaymentDate_Fmt", dr["PaymentDate_Fmt"].ToString().Trim());
                        row.Add("PendingAmount_Fmt", dr["PendingAmount_Fmt"].ToString().Trim());
                        row.Add("CheckNum", dr["CheckNum"].ToString().Trim());
                        row.Add("RedCoin_Fmt", dr["RedCoin_Fmt"].ToString().Trim());
                        row.Add("BUW_Fmt", dr["BUW_Fmt"].ToString().Trim());

                        if ((float)System.Convert.ToSingle(dr["NetTotal_Fmt"].ToString().TrimStart('$')) !=
                            (float)System.Convert.ToSingle(dr["CurrencyAmt"].ToString().TrimStart('$'))
                            + (float)System.Convert.ToSingle(dr["CardAmt"].ToString().TrimStart('$')) + (float)System.Convert.ToSingle(dr["ManualAmt"].ToString().TrimStart('$')))
                            if (dr["Note"].ToString().Trim() != "")
                                row.Add("NoteComments", "Note :" + dr["Note"].ToString().Trim());
                            else
                                row.Add("NoteComments", "");
                        else
                            row.Add("NoteComments", "");

                        row.Add("CheckNum_Fmt", dr["CheckNum"].ToString().Trim().Replace("(", "").Replace(")", ""));

                        row.Add("Coin", dr["CoinAmt_Fmt"].ToString().Trim());
                        row.Add("Token", dr["TokenAmt_Fmt"].ToString().Trim());
                        row.Add("Usage", dr["UsageAmt_Fmt"].ToString().Trim());
                        row.Add("Currency", dr["CurrencyAmt_Fmt"].ToString().Trim());
                        row.Add("Card", dr["CardAmt_Fmt"].ToString().Trim());
                        row.Add("Manual", dr["ManualAmt_Fmt"].ToString().Trim());
                        row.Add("CardSales", dr["CreditCardSalesAmt_Fmt"].ToString());
                        row.Add("CardFee", dr["CreditCardFeeAmt_Fmt"].ToString().Trim());
                      

                        row.Add("CurrencyCardSales", dr["CurrencyCardSalesAmt_Fmt"].ToString().Trim());
                        row.Add("CardRevRowID", dr["Id"].ToString().Trim());


                        row.Add("Gross30PerWasher_Fmt", dr["Gross30PerWasher_Fmt"].ToString().Trim());
                        row.Add("Gross30PerDryer_Fmt", dr["Gross30PerDryer_Fmt"].ToString().Trim());
                        row.Add("Gross30PerMach_Fmt", dr["Gross30PerMach_Fmt"].ToString().Trim());
                        row.Add("Net30PerMach_Fmt", dr["Net30PerMach_Fmt"].ToString().Trim());
                        row.Add("Permitamt", dr["Permit_Amt"].ToString().Trim());
                        //   01/29/2015 Yokesh, SYSTECH - Added Credit_Amt
                        row.Add("Credit_Amt", dr["Credit_Amt"].ToString().Trim());
                        if (dr["VendName"].ToString().Trim().Length > 0 && dr["SplitPercentage"].ToString().Trim().Length > 0)
                            row.Add("CheckNoTitle", dr["VendName"].ToString() + " (" + Math.Round(Convert.ToDecimal(dr["SplitPercentage"]), 2) + "%)");
                        else if (dr["VendName"].ToString().Length > 0)
                            row.Add("CheckNoTitle", dr["VendName"].ToString());
                        else
                            row.Add("CheckNoTitle", "");

                        if (dr["RecordType"].ToString() == "L" || dr["RecordType"].ToString() == "N")
                        {
                            row.Add("noteRowId", "noteClass");
                            if (dr["RecordType"].ToString() == "N")
                            {
                                row.Add("noteClass", "nNDT");
                            }
                            else if (dr["RecordType"].ToString() == "L")
                            {
                                if (dr["Closed"].ToString() == "0" && dr["Visibility"].ToString() == "Public")
                                {
                                    row.Add("noteClass", "lNDTrev");
                                }
                                else if (dr["Closed"].ToString() != "0" && dr["Visibility"].ToString() == "Public")
                                {
                                    row.Add("noteClass", "lcNDTrev");
                                }
                                else
                                {
                                    row.Add("noteClass", "");
                                }
                            }
                        }
                        else
                        {
                            row.Add("noteRowId", "");
                            row.Add("noteClass", "dT");
                        }
                        row.Add("RowNo", dr["Row"].ToString().Trim());
                    }
                    row.Add("LDG_Breakdown_Flag", dr["LDG_Breakdown_Flag"].ToString().Trim());
                    row.Add("ID", dr["ID"].ToString().Trim());
                    row.Add("SourceSystem", dr["SourceSystem"].ToString().Trim());
                    row.Add("TSREFee", dr["TSREFee"].ToString().Trim());
                    if (dr["LDG_Breakdown_Flag"].ToString().Trim() == "1") row.Add("RevenueBreakdown", getRevenuedetailsBreakdown(Convert.ToInt32(dr["ID"]), dr["SourceSystem"].ToString().Trim()));
                    rows.Add(row);
                }
                var JsonString = "\"Commercial\":" + serializer.Serialize(rows);
                JsonString = JsonString.Replace(System.Environment.NewLine, "");
                return JsonString.ToString();
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "RevenueSplit", "uln:" + uln + "splitRevenueNotes:" + splitRevenueNotes + "showRecords:" + showRecords + "year" + year);
                return null;
            }
        }

        public string getRevenuedetailsBreakdown(int ID, string SourceSystem)
        {
            try
            {
                int i = 0;
                string queryCode = "getRevenueBreakdown";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ID");
                paramText.Add("@SourceSystem");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(ID);
                paramValues.Add(SourceSystem);
                DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;

                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    row = new Dictionary<string, object>();
                    if (dr["RecordType"].ToString() == "P")
                    {
                        if (dr["CollectDate_Fmt"].ToString() == "99/99/99")
                        {
                            row.Add("CollectDate_Fmt", dr["NoteText"].ToString().Substring(0, 8).Replace(System.Environment.NewLine, ""));
                            row.Add("DisplayYear_Fmt", "Pinned Notes");
                            row.Add("RecordType", dr["RecordType"].ToString().Trim());
                            row.Add("NoteText", dr["NoteText"].ToString().Substring(10).Replace(System.Environment.NewLine, ""));
                        }
                        else if (dr["CollectDate_Fmt"].ToString() != "99/99/99")
                        {
                            row.Add("CollectDate_Fmt", dr["CollectDate_Fmt"].ToString());
                            row.Add("DisplayYear_Fmt", "Pinned Notes");
                            row.Add("RecordType", dr["RecordType"].ToString().Trim());
                            row.Add("NoteText", dr["NoteText"].ToString().Replace(System.Environment.NewLine, ""));
                        }
                        row.Add("RowNo", dr["Row"].ToString().Trim());
                    }
                    else
                    {
                        row.Add("NetTotal", dr["NetTotal"].ToString());
                        row.Add("CycleDays", dr["CycleDays"].ToString());
                        row.Add("ThreadId", dr["ThreadId"].ToString());
                        row.Add("Visibility", dr["Visibility"].ToString());
                        row.Add("VendName", "");
                        row.Add("Closed", dr["Closed"].ToString());
                        row.Add("DocNum", dr["DocNum"].ToString());
                        row.Add("DocTypeCode", dr["DocTypeCode"].ToString());
                        row.Add("InvoiceDate_Fmt", dr["InvoiceDate_Fmt"].ToString());
                        row.Add("InvoiceAmount_Fmt", dr["InvoiceAmount_Fmt"].ToString());
                        row.Add("RecordType", dr["RecordType"].ToString().Trim());
                        row.Add("Comment", dr["Comment"].ToString().Trim());
                        if (dr["NoteText"].ToString().Contains("\""))
                        {
                            row.Add("NoteText", (dr["NoteText"].ToString().Replace("\"", "")).Replace(System.Environment.NewLine, ""));
                            row.Add("CheckNoteText", dr["NoteText"].ToString().Substring(10).Replace(System.Environment.NewLine, ""));
                        }
                        else
                        {
                            row.Add("NoteText", dr["NoteText"].ToString().Trim().Replace(System.Environment.NewLine, ""));
                            if (dr["NoteText"].ToString().Length >= 10)
                                row.Add("CheckNoteText", dr["NoteText"].ToString().Substring(10).Replace(System.Environment.NewLine, ""));
                            else
                                row.Add("CheckNoteText", "");
                        }

                        row.Add("CheckStatus", "");
                        row.Add("CollectDate_Fmt", dr["CollectDate_Fmt"].ToString().Trim());
                        row.Add("CycleDate_Fmt", dr["CycleDate_Fmt"].ToString().Trim());
                        row.Add("CycleDays_Fmt", dr["CycleDays_Fmt"].ToString().Trim());
                        row.Add("DisplayYear_Fmt", dr["DisplayYear_Fmt"].ToString().Trim());
                        row.Add("NetTotal_Fmt", dr["NetTotal_Fmt"].ToString().Trim());
                        row.Add("NetWasher_Fmt", dr["NetWasher_Fmt"].ToString().Trim());
                        row.Add("NetDryer_Fmt", dr["NetDryer_Fmt"].ToString().Trim());
                        row.Add("PaymentDate_Fmt", "");
                        row.Add("PendingAmount_Fmt", "");
                        row.Add("CheckNum", "");
                        row.Add("RedCoin_Fmt", dr["RedCoin_Fmt"].ToString().Trim());
                        row.Add("BUW_Fmt", dr["BUW_Fmt"].ToString().Trim());

                        if ((float)System.Convert.ToSingle(dr["NetTotal_Fmt"].ToString().TrimStart('$')) != (float)System.Convert.ToSingle(dr["CurrencyAmt"].ToString().TrimStart('$')) + (float)System.Convert.ToSingle(dr["CardAmt"].ToString().TrimStart('$')) + (float)System.Convert.ToSingle(dr["ManualAmt"].ToString().TrimStart('$')))
                            if (dr["Note"].ToString().Trim() != "")
                                row.Add("NoteComments", "Note :" + dr["Note"].ToString().Trim());
                            else
                                row.Add("NoteComments", "");
                        else
                            row.Add("NoteComments", "");

                        row.Add("CheckNum_Fmt", "");

                        row.Add("Coin", dr["CoinAmt_Fmt"].ToString().Trim());
                        row.Add("Token", dr["TokenAmt_Fmt"].ToString().Trim());
                        row.Add("Usage", dr["UsageAmt_Fmt"].ToString().Trim());
                        row.Add("Currency", dr["CurrencyAmt_Fmt"].ToString().Trim());
                        row.Add("Card", dr["CardAmt_Fmt"].ToString().Trim());
                        row.Add("Manual", dr["ManualAmt_Fmt"].ToString().Trim());
                        //   01/29/2015 Yokesh, SYSTECH - Added Credit_Amt and Permit_Amt
                        row.Add("Credit_Amt", dr["Credit_Amt"].ToString().Trim());
                        row.Add("Permitamt", dr["Permit_Amt"].ToString().Trim());
                        row.Add("CardSales", dr["CreditCardSalesAmt_Fmt"].ToString());
                        row.Add("CardFee", dr["CreditCardFeeAmt_Fmt"].ToString().Trim());
                        row.Add("CurrencyCardSales", dr["CurrencyCardSalesAmt_Fmt"].ToString().Trim());
                        row.Add("CardRevRowID", dr["Id"].ToString().Trim());


                        row.Add("Gross30PerWasher_Fmt", dr["Gross30PerWasher_Fmt"].ToString().Trim());
                        row.Add("Gross30PerDryer_Fmt", dr["Gross30PerDryer_Fmt"].ToString().Trim());
                        row.Add("Gross30PerMach_Fmt", dr["Gross30PerMach_Fmt"].ToString().Trim());
                        row.Add("Net30PerMach_Fmt", "");


                        row.Add("CheckNoTitle", "");

                        if (dr["RecordType"].ToString() == "L" || dr["RecordType"].ToString() == "N")
                        {
                            row.Add("noteRowId", "noteClass");
                            if (dr["RecordType"].ToString() == "N")
                            {
                                row.Add("noteClass", "nNDT");
                            }
                            else if (dr["RecordType"].ToString() == "L")
                            {
                                if (dr["Closed"].ToString() == "0" && dr["Visibility"].ToString() == "Public")
                                {
                                    row.Add("noteClass", "lNDTrev");
                                }
                                else if (dr["Closed"].ToString() != "0" && dr["Visibility"].ToString() == "Public")
                                {
                                    row.Add("noteClass", "lcNDTrev");
                                }
                                else
                                {
                                    row.Add("noteClass", "");
                                }
                            }
                        }
                        else
                        {
                            row.Add("noteRowId", "");
                            row.Add("noteClass", "dT");
                        }
                        row.Add("RowNo", ID + SourceSystem + i);
                        row.Add("LDG_Breakdown_Flag", "-1");
                        row.Add("ID", ID);
                        row.Add("SourceSystem", SourceSystem);
                        row.Add("TSREFee",dr["TSREFee"].ToString().Trim());
         
                    }
                    rows.Add(row);
                }
                var JsonString = serializer.Serialize(rows);
                JsonString = JsonString.Replace(System.Environment.NewLine, "");
                return JsonString.ToString();
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "RevenueBreakdown", "ID:" + ID + "SourceSystem:" + SourceSystem);
                return null;
            }
        }

        //Revenue Card Detail Split Up
        public string getRevenueCardSplitJson(string CardRevRowID)
        {
            string queryCode = "getRevenueCardDetail";
            ArrayList paramText = new ArrayList();
            ArrayList paramValues = new ArrayList();
            paramText.Add("@CardRevRowID");
            paramValues.Add(CardRevRowID);
            string RevenueCardSplitJson = new WashDAL().getJsonFromQueryColumns(queryCode, paramText, paramValues, "");
            return "\"RevenueCardSplit\" : " + RevenueCardSplitJson;
        }

        //Revenue Check Number Split 
        public string getRevenueCheckNumber(string I_CheckNumber_Start, string I_CheckNumber_End, string ULN)
        {
            string queryCode = "getRevenueCheckNumber";
            ArrayList paramText = new ArrayList();
            ArrayList paramValues = new ArrayList();
            paramText.Add("@I_CheckNumber_Start");
            paramValues.Add(I_CheckNumber_Start);
            paramText.Add("@I_CheckNumber_End");
            paramValues.Add(I_CheckNumber_End);
            DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
            // verify if the ULN is part of One Check/Group Check, If it is One Check add a new column ‘Calculated Amount’ in Collection Details grid which will be the CalculatedAmount column
            string queryCode1 = "getOneCheckDet";
            ArrayList paramText1 = new ArrayList();
            ArrayList paramValues1 = new ArrayList();
            paramText1.Add("@ULN");
            paramValues1.Add(ULN);
            DataTable dt1 = new WashDAL().getDataTable(queryCode1, paramText1, paramValues1);
            int flagOneCheck = 0;
            if (dt1.Rows.Count > 0)
                if (dt1.Rows[0]["OneCheck"] != DBNull.Value) flagOneCheck = Convert.ToInt32(dt1.Rows[0]["OneCheck"]);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            List<Dictionary<string, object>> rowsrevenue = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> rowscommercial = new List<Dictionary<string, object>>();
            Dictionary<string, object> rowreveneue = null;
            Dictionary<string, object> rowCommercial = null;
            decimal washer = 0, dryer = 0, card = 0, CommTotal = 0, CalculatedAmount = 0, TotalCalcAmount = 0, totalgrossrevenue = 0;
            decimal ARInvoiceAmt1 = 0, ARInvoiceAmt2 = 0, ARInvoiceAmt3 = 0, ARInvoiceAmt4 = 0, ARInvoiceAmt5 = 0;
            string ARInvoice1 = "", ARInvoice2 = "", ARInvoice3 = "", ARInvoice4 = "", ARInvoice5 = "";
            decimal PreviousBalance = 0, OtherAdjustment = 0, CheckAmount = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    rowreveneue = new Dictionary<string, object>();
                    rowCommercial = new Dictionary<string, object>();
                    if (washer == 0)
                        washer = Convert.ToDecimal((dr["Washer"] == DBNull.Value) ? 0 : dr["Washer"]);
                    else
                        washer = washer + Convert.ToDecimal((dr["Washer"] == DBNull.Value) ? 0 : dr["Washer"]);
                    if (dryer == 0)
                        dryer = Convert.ToDecimal((dr["Dryer"] == DBNull.Value) ? 0 : dr["Dryer"]);
                    else
                        dryer = dryer + Convert.ToDecimal((dr["Dryer"] == DBNull.Value) ? 0 : dr["Dryer"]);
                    if (card == 0)
                        card = Convert.ToDecimal((dr["Card"] == DBNull.Value) ? 0 : dr["Card"]);
                    else
                        card = card + Convert.ToDecimal((dr["Card"] == DBNull.Value) ? 0 : dr["Card"]);


                    ARInvoice1 = dr["ARInvoice1"].ToString();
                    ARInvoice2 = dr["ARInvoice2"].ToString();
                    ARInvoice3 = dr["ARInvoice3"].ToString();
                    ARInvoice4 = dr["ARInvoice4"].ToString();
                    ARInvoice5 = dr["ARInvoice5"].ToString();
                    ARInvoiceAmt1 = Convert.ToDecimal((dr["ARInvoiceAmt1"] == DBNull.Value) ? 0 : dr["ARInvoiceAmt1"]);
                    ARInvoiceAmt2 = Convert.ToDecimal((dr["ARInvoiceAmt2"] == DBNull.Value) ? 0 : dr["ARInvoiceAmt2"]);
                    ARInvoiceAmt3 = Convert.ToDecimal((dr["ARInvoiceAmt3"] == DBNull.Value) ? 0 : dr["ARInvoiceAmt3"]);
                    ARInvoiceAmt4 = Convert.ToDecimal((dr["ARInvoiceAmt4"] == DBNull.Value) ? 0 : dr["ARInvoiceAmt4"]);
                    ARInvoiceAmt5 = Convert.ToDecimal((dr["ARInvoiceAmt5"] == DBNull.Value) ? 0 : dr["ARInvoiceAmt5"]);


                    PreviousBalance = Convert.ToDecimal((dr["PreviousBalance"] == DBNull.Value) ? 0 : dr["PreviousBalance"]);

                    OtherAdjustment = Convert.ToDecimal((dr["OtherAdjustment"] == DBNull.Value) ? 0 : dr["OtherAdjustment"]);

                    CheckAmount = Convert.ToDecimal(dr["CheckAmount"]);
                    if (CheckAmount == 0)
                        CheckAmount = Convert.ToDecimal(dr["CheckAmount"]);

                    if (totalgrossrevenue == 0)
                        totalgrossrevenue = Convert.ToDecimal((dr["GrossReveneu"] == DBNull.Value) ? 0 : dr["GrossReveneu"]);
                    else
                        totalgrossrevenue = totalgrossrevenue + Convert.ToDecimal((dr["GrossReveneu"] == DBNull.Value) ? 0 : dr["GrossReveneu"]);

                    TotalCalcAmount = Convert.ToDecimal((dr["TotalCalcAmount"] == DBNull.Value) ? 0 : dr["TotalCalcAmount"]);

                    CalculatedAmount = CalculatedAmount + Convert.ToDecimal((dr["CalculatedAmount"] == DBNull.Value) ? 0 : dr["CalculatedAmount"]);

                    rowCommercial.Add("ULN", formattedULN(dr["Collection_ULN"].ToString()));
                    rowCommercial.Add("Address", dr["Collection_LocAddress"].ToString());
                    rowCommercial.Add("CollectionDate", dr["CollectionDate"].ToString());
                    rowCommercial.Add("DaysInCollection", dr["DaysInCollection"].ToString().Split('.')[0].Trim());
                    rowCommercial.Add("YTDGrossRevenue", Convert.ToDecimal((dr["YTDGrossRevenue"] == DBNull.Value) ? 0 : dr["YTDGrossRevenue"]));
                    rowCommercial.Add("GrossRevenue", Convert.ToDecimal((dr["GrossReveneu"] == DBNull.Value) ? 0 : dr["GrossReveneu"]));
                    rowCommercial.Add("CalculatedAmount", Convert.ToDecimal((dr["CalculatedAmount"] == DBNull.Value) ? 0 : dr["CalculatedAmount"]));
                    rowCommercial.Add("OneCheckFlag", flagOneCheck.ToString());
                    rowscommercial.Add(rowCommercial);
                }


                rowreveneue.Add("Washer", decimal.Round(washer, 2, MidpointRounding.AwayFromZero));
                rowreveneue.Add("Dryer", decimal.Round(dryer, 2, MidpointRounding.AwayFromZero));
                rowreveneue.Add("Card", decimal.Round(card, 2, MidpointRounding.AwayFromZero));
                rowreveneue.Add("CommTotal", CommTotal);
                rowreveneue.Add("GrossTotal", totalgrossrevenue);
                rowreveneue.Add("OneCheckFlag", flagOneCheck.ToString());
                rowreveneue.Add("CalculatedAmount", CalculatedAmount);
                rowreveneue.Add("TotalCalcAmount", TotalCalcAmount);
                rowreveneue.Add("PreviousBalance", PreviousBalance);
                rowreveneue.Add("OtherAdjustment", OtherAdjustment);
                rowreveneue.Add("CheckAmount", CheckAmount);
                rowreveneue.Add("ARInvoice1", ARInvoice1);
                rowreveneue.Add("ARInvoice2", ARInvoice2);
                rowreveneue.Add("ARInvoice3", ARInvoice3);
                rowreveneue.Add("ARInvoice4", ARInvoice4);
                rowreveneue.Add("ARInvoice5", ARInvoice5);
                rowreveneue.Add("ARInvoiceAmt1", ARInvoiceAmt1);
                rowreveneue.Add("ARInvoiceAmt2", ARInvoiceAmt2);
                rowreveneue.Add("ARInvoiceAmt3", ARInvoiceAmt3);
                rowreveneue.Add("ARInvoiceAmt4", ARInvoiceAmt4);
                rowreveneue.Add("ARInvoiceAmt5", ARInvoiceAmt5);

                rowsrevenue.Add(rowreveneue);
            }

            var JsonString = "";
            var JsonRString = "\"Revenue\":" + serializer.Serialize(rowsrevenue);
            var JsonCString = "\"Commercial\":" + serializer.Serialize(rowscommercial);
            JsonString = JsonRString.Replace(System.Environment.NewLine, "") + "," + JsonCString.Replace(System.Environment.NewLine, "");
            return JsonString.ToString();
        }

        //Revenue Document Number Split 
        public string getRevenueDocumentNumber(string DocumentNumber)
        {
            string queryCode = "getRevenueDocumentNumber";
            ArrayList paramText = new ArrayList();
            ArrayList paramValues = new ArrayList();
            paramText.Add("@InvoiceNo");
            paramValues.Add(DocumentNumber);
            DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            List<Dictionary<string, object>> rowsrevenue = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> rowscommercial = new List<Dictionary<string, object>>();
            Dictionary<string, object> rowreveneue = null;
            Dictionary<string, object> rowCommercial = null;

            string InvoiceDate = "", InvoiceNo = "", BillingPeriod = "", Collections = "", FeeDescription = "", MinimumCompensation = "",
                TaxDesc_1 = "", TaxDesc_2 = "", TaxDesc_3 = "", TaxDesc_4 = "", TaxDesc_5 = "", TaxRegNo_1 = "", TaxRegNo_2 = "", TaxRegNo_3 = "", TaxRegNo_4 = "", TaxRegNo_5 = "",
                SOPInvoiceNo_1 = "", SOPInvoiceNo_2 = "", SOPInvoiceNo_3 = "", SOPInvoiceNo_4 = "", SOPInvoiceNo_5 = "";
            decimal RentalAmount = 0, TotalTax = 0, CollectionFees = 0, CardOrder = 0, Interest = 0, PaymentAmount = 0, Previousbal = 0, CurrentBal = 0, Days3160 = 0,
                TaxAmount_1 = 0, TaxAmount_2 = 0, TaxAmount_3 = 0, TaxAmount_4 = 0, TaxAmount_5 = 0,
                SOPInvoiceAmount_1 = 0, SOPInvoiceAmount_2 = 0, SOPInvoiceAmount_3 = 0, SOPInvoiceAmount_4 = 0, SOPInvoiceAmount_5 = 0;
            decimal Days6190 = 0, Over120Days = 0, Days91120 = 0;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    rowreveneue = new Dictionary<string, object>();
                    InvoiceDate = dr["InvoiceDate"].ToString();
                    InvoiceNo = dr["InvoiceNo"].ToString();
                    BillingPeriod = dr["BillingPeriod"].ToString();
                    MinimumCompensation = dr["MinimumCompensation"].ToString();
                    TaxDesc_1 = dr["TaxDesc_1"].ToString();
                    TaxDesc_2 = dr["TaxDesc_2"].ToString();
                    TaxDesc_3 = dr["TaxDesc_3"].ToString();
                    TaxDesc_4 = dr["TaxDesc_4"].ToString();
                    TaxDesc_5 = dr["TaxDesc_5"].ToString();
                    TaxRegNo_1 = dr["TaxRegNo_1"].ToString();
                    TaxRegNo_2 = dr["TaxRegNo_2"].ToString();
                    TaxRegNo_3 = dr["TaxRegNo_3"].ToString();
                    TaxRegNo_4 = dr["TaxRegNo_4"].ToString();
                    TaxRegNo_5 = dr["TaxRegNo_5"].ToString();
                    TaxAmount_1 = Convert.ToDecimal((dr["TaxAmount_1"] == DBNull.Value) ? 0 : dr["TaxAmount_1"]);
                    TaxAmount_2 = Convert.ToDecimal((dr["TaxAmount_2"] == DBNull.Value) ? 0 : dr["TaxAmount_2"]);
                    TaxAmount_3 = Convert.ToDecimal((dr["TaxAmount_3"] == DBNull.Value) ? 0 : dr["TaxAmount_3"]);
                    TaxAmount_4 = Convert.ToDecimal((dr["TaxAmount_4"] == DBNull.Value) ? 0 : dr["TaxAmount_4"]);
                    TaxAmount_5 = Convert.ToDecimal((dr["TaxAmount_5"] == DBNull.Value) ? 0 : dr["TaxAmount_5"]);

                    if (dr["RentalAmount"].ToString() != string.Empty)
                        RentalAmount = Convert.ToDecimal(dr["RentalAmount"]);
                    if (dr["TotalTax"].ToString() != string.Empty)
                        TotalTax = Convert.ToDecimal(dr["TotalTax"]);
                    if (dr["CollectionFees"].ToString() != string.Empty)
                        CollectionFees = Convert.ToDecimal(dr["CollectionFees"]);
                    if (dr["CardOrder"].ToString() != string.Empty)
                        CardOrder = Convert.ToDecimal(dr["CardOrder"]);
                    if (dr["Interest"].ToString() != string.Empty)
                        Interest = Convert.ToDecimal(dr["Interest"]);

                    if (dr["TrxAmnt_Prior"].ToString() != string.Empty)
                        Previousbal = Convert.ToDecimal(dr["TrxAmnt_Prior"]);
                    if (dr["PaymentAmount"].ToString() != string.Empty)
                        PaymentAmount = Convert.ToDecimal(dr["PaymentAmount"]);

                    Collections = dr["Collections"].ToString();
                    SOPInvoiceNo_1 = dr["SOPInvoiceNo_1"].ToString();
                    SOPInvoiceNo_2 = dr["SOPInvoiceNo_2"].ToString();
                    SOPInvoiceNo_3 = dr["SOPInvoiceNo_3"].ToString();
                    SOPInvoiceNo_4 = dr["SOPInvoiceNo_4"].ToString();
                    SOPInvoiceNo_5 = dr["SOPInvoiceNo_5"].ToString();
                    SOPInvoiceAmount_1 = Convert.ToDecimal((dr["SOPInvoiceAmount_1"] == DBNull.Value) ? 0 : dr["SOPInvoiceAmount_1"]);
                    SOPInvoiceAmount_2 = Convert.ToDecimal((dr["SOPInvoiceAmount_2"] == DBNull.Value) ? 0 : dr["SOPInvoiceAmount_2"]);
                    SOPInvoiceAmount_3 = Convert.ToDecimal((dr["SOPInvoiceAmount_3"] == DBNull.Value) ? 0 : dr["SOPInvoiceAmount_3"]);
                    SOPInvoiceAmount_4 = Convert.ToDecimal((dr["SOPInvoiceAmount_4"] == DBNull.Value) ? 0 : dr["SOPInvoiceAmount_4"]);
                    SOPInvoiceAmount_5 = Convert.ToDecimal((dr["SOPInvoiceAmount_5"] == DBNull.Value) ? 0 : dr["SOPInvoiceAmount_5"]);
                    FeeDescription = dr["FeeDescription"].ToString();

                    if (dr["CurrentBal"].ToString() != string.Empty)
                        CurrentBal = Convert.ToDecimal(dr["CurrentBal"]);
                    if (dr["3160Days"].ToString() != string.Empty)
                        Days3160 = Convert.ToDecimal(dr["3160Days"]);
                    if (dr["6190Days"].ToString() != string.Empty)
                        Days6190 = Convert.ToDecimal(dr["6190Days"]);
                    if (dr["Over120Days"].ToString() != string.Empty)
                        Over120Days = Convert.ToDecimal(dr["Over120Days"]);
                    if (dr["91120Days"].ToString() != string.Empty)
                        Days91120 = Convert.ToDecimal(dr["91120Days"]);

                    rowCommercial = new Dictionary<string, object>();
                    rowCommercial.Add("ULN", formattedULN(dr["ULN_Page2"].ToString()));
                    rowCommercial.Add("Address", dr["LocationAddress_Page2"].ToString());
                    //rowCommercial.Add("CollectionDate", dr["CollectionDate"].ToString());
                    //rowCommercial.Add("DaysInCollection", dr["DaysInCollection"].ToString().Split('.')[0].Trim());
                    //rowCommercial.Add("YTDGrossRevenue", dr["YTDGrossRevenue"].ToString());
                    rowscommercial.Add(rowCommercial);
                }
                rowreveneue = new Dictionary<string, object>();
                rowreveneue.Add("InvoiceDate", InvoiceDate);
                rowreveneue.Add("InvoiceNo", InvoiceNo);
                rowreveneue.Add("BillingPeriod", BillingPeriod);
                rowreveneue.Add("MinimumCompensation", MinimumCompensation);

                rowreveneue.Add("TaxDesc_1", TaxDesc_1);
                rowreveneue.Add("TaxRegNo_1", TaxRegNo_1);
                rowreveneue.Add("TaxAmount_1", TaxAmount_1);
                rowreveneue.Add("TaxDesc_2", TaxDesc_2);
                rowreveneue.Add("TaxRegNo_2", TaxRegNo_2);
                rowreveneue.Add("TaxAmount_2", TaxAmount_2);
                rowreveneue.Add("TaxDesc_3", TaxDesc_3);
                rowreveneue.Add("TaxRegNo_3", TaxRegNo_3);
                rowreveneue.Add("TaxAmount_3", TaxAmount_3);
                rowreveneue.Add("TaxDesc_4", TaxDesc_4);
                rowreveneue.Add("TaxRegNo_4", TaxRegNo_4);
                rowreveneue.Add("TaxAmount_4", TaxAmount_4);
                rowreveneue.Add("TaxDesc_5", TaxDesc_5);
                rowreveneue.Add("TaxRegNo_5", TaxRegNo_5);
                rowreveneue.Add("TaxAmount_5", TaxAmount_5);

                rowreveneue.Add("Previousbal", Previousbal);
                rowreveneue.Add("PaymentAmount", PaymentAmount);
                rowreveneue.Add("BalFWD", Previousbal - PaymentAmount);

                rowreveneue.Add("RentalAmount", RentalAmount);
                if (Collections == string.Empty)
                    rowreveneue.Add("Collections", 0);
                else
                    rowreveneue.Add("Collections", Collections);

                rowreveneue.Add("CollectionFees", CollectionFees);
                rowreveneue.Add("CardOrder", CardOrder);

                rowreveneue.Add("SOPInvoiceNo_1", SOPInvoiceNo_1);
                rowreveneue.Add("SOPInvoiceNo_2", SOPInvoiceNo_2);
                rowreveneue.Add("SOPInvoiceNo_3", SOPInvoiceNo_3);
                rowreveneue.Add("SOPInvoiceNo_4", SOPInvoiceNo_4);
                rowreveneue.Add("SOPInvoiceNo_5", SOPInvoiceNo_5);
                rowreveneue.Add("SOPInvoiceAmount_1", SOPInvoiceAmount_1);
                rowreveneue.Add("SOPInvoiceAmount_2", SOPInvoiceAmount_2);
                rowreveneue.Add("SOPInvoiceAmount_3", SOPInvoiceAmount_3);
                rowreveneue.Add("SOPInvoiceAmount_4", SOPInvoiceAmount_4);
                rowreveneue.Add("SOPInvoiceAmount_5", SOPInvoiceAmount_5);

                rowreveneue.Add("Interest", Interest);
                rowreveneue.Add("FeeDescription", FeeDescription);

                rowreveneue.Add("TOTBAL", RentalAmount + TotalTax + CollectionFees + CardOrder + Interest);
                rowreveneue.Add("TOTDUE", RentalAmount + TotalTax + CollectionFees + CardOrder + Interest + Previousbal - PaymentAmount + SOPInvoiceAmount_1 + SOPInvoiceAmount_2 + SOPInvoiceAmount_3 + SOPInvoiceAmount_4 + SOPInvoiceAmount_5);
                rowreveneue.Add("CurrentBal", CurrentBal);
                rowreveneue.Add("Days3160", Days3160);
                rowreveneue.Add("Days6190", Days6190);
                rowreveneue.Add("Days91120", Days91120);
                rowreveneue.Add("Over120Days", Over120Days);
                rowsrevenue.Add(rowreveneue);
            }
            var JsonString = "";
            var JsonRString = "\"Revenue\":" + serializer.Serialize(rowsrevenue);
            var JsonCString = "\"Commercial\":" + serializer.Serialize(rowscommercial);
            JsonString = JsonRString.Replace(System.Environment.NewLine, "") + "," + JsonCString.Replace(System.Environment.NewLine, "");
            return JsonString.ToString();
        }

        protected void MakeBlankRow(ref DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
        }

        public string formattedULN(string ULN)
        {
            if (ULN.Length > 8)
            {
                ULN = ULN.Insert(4, "-");
                ULN = ULN.Insert(7, "-");
                return ULN;
            }
            else
                return ULN;
        }

    }
}
