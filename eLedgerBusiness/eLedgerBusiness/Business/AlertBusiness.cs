﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using eLedgerBusiness.DAL;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

namespace eLedgerBusiness
{
    public class AlertBusiness
    {
        string AlertList = "";
        Alert alert = new Alert();
        User userdet = new User();
        List<Location> locationList = new List<Location>();
        Location location = new Location();
        public string formatnotetext(string notetext)
        {
            if (notetext.Contains("\\n"))
                notetext = notetext.Replace("\\n", "<br />");
            return notetext;
        }
        public string getAlertList(string alertStatus, string alertSubtype)
        {
            try
            {
                string queryCode;
                if (alertStatus == "")
                {
                    alertStatus = "all";
                    alertSubtype = "all";
                }
                if (alertStatus == "all" && alertSubtype == "all")
                {
                    queryCode = "getAlertList";
                }
                else
                {
                    queryCode = "getAlertTypeList";
                }
                string userid = HttpContext.Current.User.Identity.Name;
                if (userid == "")
                    userid = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                if (alertStatus == "all" && alertSubtype == "all")
                {
                    AlertList = searchAlerts(queryCode, userid, alertStatus, alertSubtype);
                }

                return AlertList;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "Alerts", "getAlertTypeList : " + alertStatus + " " + alertSubtype);
                return null;
            }
        }

        public string getAlertsULN(string ULN, int showHistory, int showFuture)
        {
            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> jSonList = new List<Dictionary<string, object>>();
                Dictionary<string, object> jSon = null;
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                if (UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                string FutureFlag = "0";
                string HistoryFlag = "0";
                string queryCode = "getAlertDet";
                StringBuilder sbJson = new StringBuilder("[");
                ArrayList paramText = new ArrayList();

                paramText.Add("@ULN");
                paramText.Add("@UserID");

                ArrayList paramValues = new ArrayList();

                paramValues.Add(ULN);
                paramValues.Add(UserName);

                Boolean test = true;
                DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows) // Loop over the rows..
                    {
                        if (row["StatusText"].ToString() == "Future")
                        {
                            FutureFlag = "1";
                        }
                        if (row["StatusText"].ToString() == "History")
                        {
                            HistoryFlag = "1";
                        }
                    }
                }
                string cClass = "";
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows) // Loop over the rows..
                    {
                        if (showFuture == 1)
                        {
                            test = true;
                        }
                        else if (showHistory == 1)
                        {
                            test = true;
                        }
                        else
                        {
                            test = row["StatusText"].ToString() != "History" && row["StatusText"].ToString() != "Future";
                        }
                        if (row["StatusText"].ToString() == "Future")
                        {
                            cClass = "futureAlert";
                        }
                        else if (row["StatusText"].ToString() == "History")
                        {
                            cClass = "futureAlert";
                        }
                        else
                        {
                            if (row["StatusText"].ToString() == "Completed")
                            {
                                cClass = "";
                            }
                            else
                            {
                                cClass = "activeAlert";
                            }
                        }

                        if (test)
                        {
                            if (showHistory == 1 && (row["StatusText"].ToString() == "History" || row["StatusText"].ToString() != "New" || row["StatusText"].ToString() != "Completed" || row["StatusText"].ToString() != "InProgress") && row["StatusText"].ToString() != "Future")
                            {
                                jSon = new Dictionary<string, object>();                                
                                jSon.Add("AlertInstNum", row["AlertInstNum"].ToString());
                                jSon.Add("ULN", row["ULN"].ToString());
                                jSon.Add("RepeatNum", row["RepeatNum"].ToString());
                                jSon.Add("StatusText", row["StatusText"].ToString());
                                jSon.Add("AlertType", row["AlertType"].ToString());
                                jSon.Add("DueDate", row["DueDate"].ToString());
                                jSon.Add("Message", formatnotetext(row["Message"].ToString()));
                                jSon.Add("Sort", row["Sort"].ToString());
                                jSon.Add("Source", row["Source"].ToString());
                                jSon.Add("Destination", row["Destination"].ToString());
                                jSon.Add("CreatedDate", row["CreatedDate"].ToString());
                                jSon.Add("EventInstNum", row["EventInstNum"].ToString());
                                jSon.Add("FullName", userdet.FullName);
                                jSon.Add("FutureFlag", FutureFlag.ToString());
                                jSon.Add("HistoryFlag", HistoryFlag.ToString());
                                jSon.Add("sentByMe", row["sentByMe"].ToString());
                                jSon.Add("sentToMe", row["sentToMe"].ToString());
                                jSon.Add("cClass", cClass.ToString());
                                jSon.Add("SequenceNum", row["SequenceNum"].ToString());
                                jSonList.Add(jSon);
                            }
                            else if (showFuture == 1 && (row["StatusText"].ToString() == "Future" || row["StatusText"].ToString() != "New" || row["StatusText"].ToString() != "Completed" || row["StatusText"].ToString() != "InProgress") && row["StatusText"].ToString() != "History")
                            {
                                jSon = new Dictionary<string, object>();                                
                                jSon.Add("AlertInstNum", row["AlertInstNum"].ToString());
                                jSon.Add("ULN", row["ULN"].ToString());
                                jSon.Add("RepeatNum", row["RepeatNum"].ToString());
                                jSon.Add("StatusText", row["StatusText"].ToString());
                                jSon.Add("AlertType", row["AlertType"].ToString());
                                jSon.Add("DueDate", row["DueDate"].ToString());
                                jSon.Add("Message", formatnotetext(row["Message"].ToString()));
                                jSon.Add("Sort", row["Sort"].ToString());
                                jSon.Add("Source", row["Source"].ToString());
                                jSon.Add("Destination", row["Destination"].ToString());
                                jSon.Add("CreatedDate", row["CreatedDate"].ToString());
                                jSon.Add("EventInstNum", row["EventInstNum"].ToString());
                                jSon.Add("FullName", userdet.FullName);
                                jSon.Add("FutureFlag", FutureFlag.ToString());
                                jSon.Add("HistoryFlag", HistoryFlag.ToString());
                                jSon.Add("sentByMe", row["sentByMe"].ToString());
                                jSon.Add("sentToMe", row["sentToMe"].ToString());
                                jSon.Add("SequenceNum", row["SequenceNum"].ToString());
                                jSon.Add("cClass", cClass.ToString());
                                jSon.Add("ClearedDate", row["ClearedDate"].ToString());
                                jSonList.Add(jSon);
                            }
                            else if (showHistory == 0 && showFuture == 0)
                            {
                                jSon = new Dictionary<string, object>();                                
                                jSon.Add("AlertInstNum", row["AlertInstNum"].ToString());
                                jSon.Add("EventInstNum", row["EventInstNum"].ToString());
                                jSon.Add("FullName", userdet.FullName);
                                jSon.Add("ULN", row["ULN"].ToString());
                                jSon.Add("RepeatNum", row["RepeatNum"].ToString());
                                jSon.Add("StatusText", row["StatusText"].ToString());
                                jSon.Add("AlertType", row["AlertType"].ToString());
                                jSon.Add("DueDate", row["DueDate"].ToString());
                                jSon.Add("Message", formatnotetext(row["Message"].ToString()));
                                jSon.Add("Sort", row["Sort"].ToString());
                                jSon.Add("Source", row["Source"].ToString());
                                jSon.Add("Destination", row["Destination"].ToString());
                                jSon.Add("CreatedDate", row["CreatedDate"].ToString());
                                jSon.Add("FutureFlag", FutureFlag.ToString());
                                jSon.Add("HistoryFlag", HistoryFlag.ToString());
                                jSon.Add("sentByMe", row["sentByMe"].ToString());
                                jSon.Add("sentToMe", row["sentToMe"].ToString());
                                jSon.Add("cClass", cClass.ToString());
                                jSon.Add("SequenceNum", row["SequenceNum"].ToString());
                                jSonList.Add(jSon);
                            }
                        }
                    }
                }
                string jsondat = serializer.Serialize(jSonList) + ",\"FutureFlag\":\"" + FutureFlag + "\"" + ",\"HistoryFlag\":\"" + HistoryFlag + "\"";
                return jsondat;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "Alert", "ULN :" + ULN + "showHistory:" + showHistory + "showFuture:" + showHistory);
                return null;
            }
        }

        public string RecipientByType(string AlertEventNum, string Type)
        {
            try
            {
                string queryCode = "";
                string jsondata = "";
                ArrayList paramText = new ArrayList();
                DataTable dt;
                if (Type == "Alert")
                {
                    queryCode = "getAlertRecipient";
                    paramText.Add("@AlertNum");
                }
                else
                {
                    queryCode = "getEventRecipient";
                    paramText.Add("@EventInstID");
                }
                ArrayList paramValues = new ArrayList();
                paramValues.Add(AlertEventNum);
                dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
                jsondata = new WashDAL().GetJson(dt);
                return jsondata;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "Alertusername", "AlertEventNum:" + AlertEventNum + "Type:" + Type);
                return null;
            }
        }
        public string searchAlerts(string queryCode, string userid, string alertStatus, string alertSubtype)
        {
            try
            {
                ArrayList paramText = new ArrayList();
                paramText.Add("@UserID");
                paramText.Add("@AlertStatus");
                paramText.Add("@AlertSubtype");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(userid);
                paramValues.Add(alertStatus);
                paramValues.Add(alertSubtype);

                DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
                StringBuilder sbJson = new StringBuilder("[");

                if ((alertStatus == "all") && (alertSubtype == "all"))
                {
                    foreach (DataRow row in dt.Rows) // Loop over the rows..
                    {
                        if ((row["DisplayText"].ToString() == "New") || (row["DisplayText"].ToString() == "Active") || (row["DisplayText"].ToString() == "Future") || (row["DisplayText"].ToString() == "History"))
                        {
                            sbJson.Append("{");
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "DisplayOrder", row["DisplayOrder"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "DisplayText", row["DisplayText"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SubType", row["SubType"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\"", "CategoryCount", row["CategoryCount"]);
                            sbJson.Append("}, ");
                        }
                    }
                    if (sbJson.ToString().Length > 2)
                        sbJson.Replace(", ", "]", sbJson.Length - 2, 2);
                    else
                        sbJson.Append("]");
                }
                return sbJson.ToString();
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "AlertSearch", "userid:" + userid + "alertStatus:" + alertStatus + "alertSubtype:" + alertSubtype);
                return null;
            }
        }
        public string AlertsLocationList(string alertstatus, string alerttype)
        {
            try
            {
                string userid = HttpContext.Current.User.Identity.Name;
                if (userid == "")
                    userid = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                ArrayList paramText = new ArrayList();
                ArrayList paramValues = new ArrayList();

                paramText.Add("@UserID");
                paramText.Add("@AlertStatus");
                paramText.Add("@AlertSubtype");

                paramValues.Add(userid);
                paramValues.Add(alertstatus);
                paramValues.Add(alerttype);

                string jsondata = "{\"SearchAlert\":" + new WashDAL().getJsonFromQueryColumns("getAlertTypeList", paramText, paramValues, "ULN, Address, City, State, Zip, Status, ContractType, PropertyName");
                string jsondata1 = jsondata.Replace("},{", "}{");
                int total = (jsondata.Length - jsondata1.Length) + 1;
                jsondata = jsondata + ",\"total\":\"" + total + "\"}";
                return jsondata;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "AlertULNSearch", "alertstatus:" + alertstatus + "alerttype:" + alerttype);
                return null;
            }
        }
        public string formattedULN(string ULN)
        {
            if (ULN.Length > 8)
            {
                ULN = ULN.Insert(4, "-");
                ULN = ULN.Insert(7, "-");                
                return ULN;
            }
            else
                return ULN;
        }
        public string  AddNewAlert(string ULN, string ToUserID, string AlertType, string AlertMSG, string DueDate, string isPrivate, string SequenceType, string SequenceNum, string RepeatNum)
        {
            try
            {
                String queryCode = "addAlert";
                string FromUser;
                int AlertInstNum = 0;
               //string UserName = System.Web.HttpContext.Current.User.Identity.Name;
               string UserName = "WASHLAUNDRY\\vmuthuswamy";
                if (UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                FromUser = new UserCredentials().GetUser(UserName).LastName.ToString();

                int isPriv = Convert.ToInt16(isPrivate.ToString());
                //if (isPrivate == "True")
                //{
                //    isPriv = 1;
                //}
                //else
                //{
                //    isPriv = 0;
                //}
                if (DueDate == "")
                {
                    DueDate = DateTime.Now.Date.ToString();
                }
                ArrayList paramText = new ArrayList();
                ArrayList paramValues = new ArrayList();
                if (SequenceType == "2")
                {
                    paramText = new ArrayList();
                    paramValues = new ArrayList();
                    paramText.Add("@ULN");
                    paramText.Add("@FromUserID");
                    paramText.Add("@ToUserID");
                    paramText.Add("@AlertType");
                    paramText.Add("@AlertMSG");
                    paramText.Add("@DueDate");
                    paramText.Add("@isPrivate");
                    paramText.Add("@AlertInstNum");

                    paramText.Add("@SequenceType");
                    paramText.Add("@SequenceNum");
                    paramText.Add("@RepeatNum");

                    paramValues.Add(ULN);

                    paramValues.Add(FromUser);
                    paramValues.Add(Convert.ToInt32(0));
                    paramValues.Add(Convert.ToInt32(AlertType));
                    paramValues.Add(AlertMSG);
                    paramValues.Add(DateTime.Parse(DueDate));
                    paramValues.Add(Convert.ToInt32(isPriv));
                    paramValues.Add(Convert.ToInt32(0));

                    paramValues.Add(Convert.ToInt32(SequenceType));
                    paramValues.Add(Convert.ToInt32(SequenceNum));
                    paramValues.Add(Convert.ToInt32(RepeatNum));

                    DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
                    DataRow row = dt.Rows[0];
                    AlertInstNum = Convert.ToInt32(row["AlertInstNum"].ToString());
                }

                string[] Touser = ToUserID.Split(',');
                foreach (string Touserid in Touser)
                {
                    paramText = new ArrayList();
                    paramValues = new ArrayList();
                    paramText.Add("@ULN");
                    paramText.Add("@FromUserID");
                    paramText.Add("@ToUserID");
                    paramText.Add("@AlertType");
                    paramText.Add("@AlertMSG");
                    paramText.Add("@DueDate");
                    paramText.Add("@isPrivate");
                    paramText.Add("@AlertInstNum");

                    paramText.Add("@SequenceType");
                    paramText.Add("@SequenceNum");
                    paramText.Add("@RepeatNum");

                    paramValues.Add(ULN);

                    paramValues.Add(FromUser);
                    paramValues.Add(Touserid);
                    paramValues.Add(Convert.ToInt32(AlertType));
                    paramValues.Add(AlertMSG);
                    paramValues.Add(DateTime.Parse(DueDate));
                    paramValues.Add(Convert.ToInt32(isPriv));
                    paramValues.Add(Convert.ToInt32(AlertInstNum));

                    paramValues.Add(Convert.ToInt32(SequenceType));
                    paramValues.Add(Convert.ToInt32(SequenceNum));
                    paramValues.Add(Convert.ToInt32(RepeatNum));
                    new WashDAL().executeQuery(queryCode, paramText, paramValues);
                }
                return "true";
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "Addalerts", "ULN:" + ULN + "ToUserID:" + ToUserID + "AlertType:" + AlertType + "AlertMSG:" + AlertMSG + "DueDate:" + DueDate + "isPrivate:" + isPrivate + "SequenceType:" + SequenceType + "SequenceNum:" + SequenceNum + "RepeatNum:" + RepeatNum);
                return null;
            }
        }
        public String AlertUpdate(string ULN, string ALID, string newStatus, string newClear)
        {
            try
            {
                String queryCode = "AlertUpdate";
                DataTable dt;
                string UserID;
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                if (UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                UserID = new UserCredentials().GetUser(UserName).LastName.ToString();

                ArrayList paramText = new ArrayList();
                ArrayList paramValues = new ArrayList();
                paramText.Add("@ULN");
                paramText.Add("@UserID");
                paramText.Add("@AlertID");
                paramText.Add("@Status");
                paramText.Add("@Clear");
                paramValues.Add(ULN);
                paramValues.Add(UserID);
                paramValues.Add(ALID);
                paramValues.Add(newStatus);
                paramValues.Add(newClear);
                dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
                return "true";
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "Alertupdate", "ULN:" + ULN + "ALID:" + ALID + "newStatus:" + newStatus + "newClear:" + newClear);
                return null;
            }
        }

    }

}
