﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using System.Data;
using eLedgerBusiness.DAL;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
namespace eLedgerBusiness
{
    public class LegalBusiness
    {
        List<Legal> legalList = new List<Legal>();
        Legal legal;
        User userdet = new User();

        public string formattext(string notetext)
        {
            if (notetext.Contains("\\n"))
                notetext = notetext.Replace("\\n", "<br />");
            return notetext;
        }

        public string AddNewLegalNote(string uln, string noteText, string visibilityCode, string threadClosed, string ThreadId)
        {
            try
            {
                string legalins;
                int result = 0;
                string sourceCode = "LEGA";
                ArrayList paramText = new ArrayList();
                paramText.Add("@CreatedDt");
                paramText.Add("@CreatedById");
                paramText.Add("@UpdatedDt");
                paramText.Add("@UpdatedById");
                paramText.Add("@KeyId");
                paramText.Add("@SourceCd");
                paramText.Add("@NoteText");
                paramText.Add("@VisibilityCode");
                paramText.Add("@Closed");
                paramText.Add("@ULN");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(DateTime.Today);
                paramValues.Add(getUserId());
                paramValues.Add(DateTime.Today);
                paramValues.Add(getUserId());
                paramValues.Add(uln);
                paramValues.Add(sourceCode);
                paramValues.Add(noteText);
                paramValues.Add(visibilityCode);
                paramValues.Add(threadClosed);
                paramValues.Add(uln);
                if (ThreadId != null && Convert.ToInt16(ThreadId) > -1)
                {
                    paramText.Add("@ThreadId");
                    paramValues.Add(ThreadId);
                    string queryCode = "updateLegalNote";
                    result = new WashDAL().executeQuery(queryCode, paramText, paramValues);

                    //create new note with sequence num
                    queryCode = "addNewLegalThreadSeq";
                    result = new WashDAL().executeQuery(queryCode, paramText, paramValues);
                }
                else
                {
                    string queryCode = "addNewLegalNote";
                    result = new WashDAL().executeQuery(queryCode, paramText, paramValues);

                }
                legalins = "true";
                return legalins;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ddLegalNotes", "uln:" + uln + " noteText:" + noteText + " visibilityCode:" + visibilityCode + " threadClosed:" + threadClosed + "ThreadId: " + ThreadId);
                return null;
            }
        }
        private string getUserId()
        {
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
            if (UserName == "")
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            return (new UserCredentials().GetUser(UserName).UserID.ToString());
        }

        public string deleteAllLegalNotes(string uln, string threadid)
        {
            try
            {
                int result = 0;
                string resdata = "";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramText.Add("@ThreadId");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);
                paramValues.Add(threadid);

                string queryCode = "deleteAllLegalThread";
                result = new WashDAL().executeQuery(queryCode, paramText, paramValues);

                resdata = "true";
                return resdata;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "DeleteLegalNotes", "uln:" + uln + " threadid" + threadid);
                return null;
            }

        }

        public string deleteLegalNotesById(string uln, string id, string threadid)
        {
            try
            {
                int result = 0;
                string dellegal;
                ArrayList paramText = new ArrayList();
                paramText.Add("@id");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(id);

                string queryCode = "deleteLegalIssueById";
                result = new WashDAL().executeQuery(queryCode, paramText, paramValues);
                dellegal = "true";
                return dellegal;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "DeleteLegal", "uln:" + uln + "id:" + id + "threadid:" + threadid);
                return null;
            }
        }
        public string getlegalissuebyid(string uln, string ID)
        {
            try
            {
                int canEditCount = 0;
                int totallegalcount = 0;
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramText.Add("@ThreadId");
                legal = new Legal();
                legal.LegalMas = new List<LegalMaster>();

                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);
                paramValues.Add(ID);
                string querycode = "LegalNoteListById";
                DataTable dt = new WashDAL().getDataTable(querycode, paramText, paramValues);
                int CanEdit;
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new
                 System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> Srows =
                  new List<Dictionary<string, object>>();
                Dictionary<string, object> Srow = null;
                foreach (DataRow row in dt.Rows)
                {
                    Srow = new Dictionary<string, object>();
                    Srow.Add("ThreadId", row["Id"].ToString());
                    Srow.Add("NoteDate_Fmt", row["NoteDate_Fmt"].ToString().Trim());
                    Srow.Add("uln", uln);
                    Srow.Add("id", ID);
                    Srow.Add("NoteText", row["NoteText"].ToString().Trim());
                    Srow.Add("visibility", row["VisibilityCode"].ToString().Trim());
                    Srow.Add("status", row["Closed"].ToString().Trim());
                    if ((DateTime.Now.Date - Convert.ToDateTime(row["NoteDate_Fmt"].ToString().Trim())).TotalDays > 0)
                        CanEdit = 0;
                    else
                    {
                        CanEdit = 1;
                        canEditCount++;
                    }
                    Srow.Add("CanEdit", CanEdit.ToString());
                    Srows.Add(Srow);
                    totallegalcount++;
                }

                string json = serializer.Serialize(Srows);

                json = json + ",\"LegalEdit\":\"" + canEditCount + "\",\"LegalTotal\":\"" + totallegalcount + "\"";

                return formattext(json);
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "getLegalIssue", "Uln:" + uln + " ID:" + ID);
                return null;
            }

        }

        public string LegalIssues(string uln)
        {
            try
            {

                string UserName;
                int hasLegalAccess = 0;
                UserName = System.Web.HttpContext.Current.User.Identity.Name;
                if (UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                userdet = new UserCredentials().GetUser(UserName);

                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);
                string querycode = "getLegelNotes";
                DataTable dt = new WashDAL().getDataTable(querycode, paramText, paramValues);

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row1 = null;
                string status = "", NoteDate = "", visibility = "", lClass = "", l_dRow = "", l_Style = "", threadId = "";
                if (hasLegalAccess == 2 || hasLegalAccess == 3)
                {
                    if (userdet.RoleId == "10000005" && hasLegalAccess == 2)
                        hasLegalAccess = 4;
                    else if (userdet.RoleId == "10000006" && hasLegalAccess == 3)
                        hasLegalAccess = 4;
                }
                if (userdet.RoleId == "10000002") //|| (((User)userdetArray.GetValue(0)).RoleId == "10000001"))
                {
                    hasLegalAccess = 1;
                }
                else if ((userdet.RoleId == "10000006") || (userdet.RoleId == "10000004")) //|| (((User)userdetArray.GetValue(0)).RoleId == "10000001"))
                {
                    hasLegalAccess = 2;
                }
                else if (userdet.RoleId == "10000005")
                {
                    hasLegalAccess = 3;
                }
                else if (userdet.RoleId == "10000001")
                {
                    hasLegalAccess = 4;
                }
                else if (userdet.RoleId == "10000007")
                {
                    hasLegalAccess = 5;
                }
                else
                {
                    hasLegalAccess = 0;
                }


                List<string> cols = new List<string>(10);

                StringBuilder sbJson = new StringBuilder("[");

                using (SqlConnection dbConnection = new WashDAL().openConnection1())
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {

                        dbCommand.Connection = dbConnection;
                        dbCommand.CommandText = new WashDAL().getQuery(querycode);
                        dbCommand.CommandTimeout = 0;

                        if (paramText != null)
                        {
                            for (var i = 0; i < paramText.Count; i++)
                            {
                                string paramName = paramText[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }

                        using (SqlDataReader reader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            int ncols = reader.FieldCount;
                            for (int i = 0; i < ncols; ++i)
                            {
                                cols.Add(reader.GetName(i));
                            }

                            while (reader.Read()) // Loop over the rows..
                            {
                                row1 = new Dictionary<string, object>();
                                if ((hasLegalAccess == 1 || hasLegalAccess == 4) || !(reader["VisibilityCode"].ToString() == "PRIV"))
                                {

                                    foreach (string col in cols)
                                    {
                                        if (col == "ThreadId")
                                        {
                                            row1.Add("ThreadId", reader["ThreadId"].ToString().PadLeft(4, '0'));
                                            row1.Add("ThreadId_org", reader["ThreadId"].ToString());

                                        }
                                        else if (col == "CreatedBy")
                                        {
                                            row1.Add(col, reader["CreatedBy"].ToString().Trim());

                                        }
                                        else
                                            row1.Add(col, reader[col]);

                                    }
                                    if (reader["Closed"].ToString() == "0")
                                    {
                                        status = "Open";
                                        lClass = "openLegal";
                                    }

                                    if (reader["Closed"].ToString() == "1")
                                    {
                                        status = "Closed";
                                        lClass = "closedLegal";
                                    }

                                    if (status == "Closed")
                                    {
                                        if (reader["IsMaster"].ToString() != "1")
                                        {
                                            l_dRow = reader["ThreadId"].ToString().PadLeft(4, '0');
                                            l_Style = "display:none";
                                        }
                                    }
                                    visibility = (reader["VisibilityCode"].ToString() == "PRIV" ? "Private" : "Public").ToString();
                                    row1.Add("status", status);
                                    row1.Add("ULN", uln);
                                    row1.Add("hasLegalAccess", hasLegalAccess);
                                    row1.Add("lClass", lClass);
                                    row1.Add("l_dRow", l_dRow);
                                    row1.Add("l_Style", l_Style);
                                    row1.Add("threadid", threadId);
                                    row1.Add("visibility", visibility);
                                    threadId = reader["ThreadId"].ToString().PadLeft(4, '0');
                                    rows.Add(row1);
                                    l_Style = "";
                                    l_dRow = "";
                                    status = "";
                                }

                            }
                            sbJson = new StringBuilder(serializer.Serialize(rows));

                            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
                            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
                            Dictionary<string, object> row2 = null;
                            row2 = new Dictionary<string, object>();

                            row2.Add("hasLegalAccess", hasLegalAccess);

                            rows1.Add(row2);
                            string Json = "{\"Legal\":" + serializer.Serialize(rows) + ",\"hasLegalAccess\":" + hasLegalAccess + "}";

                            return Json;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "LegalIssue", "uln:" + uln);
                return null;
            }

        }

    }

}
