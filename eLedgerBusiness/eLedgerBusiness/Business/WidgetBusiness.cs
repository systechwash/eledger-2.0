﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using System.Data;


using eLedgerBusiness.DAL;
using System.Net;
using System.Collections;
using System.Web.Services;
namespace eLedgerBusiness
{
    public class WidgetBusiness
    {
        List<Widget> widgetMenuList = new List<Widget>();
        Widget widget;
        User userdet = new User();
        public Array getWidgetMenu(string pageName)
        {
           
            return getWidgetMenuList(pageName).ToArray();
        }
        public List<Widget> getWidgetMenuList(string pageName)
        {
            DataTable dt = getDataTable(pageName);
            foreach (DataRow row in dt.Rows) // Loop over the rows..
            {
                widget = new Widget();
                widget.WidgetName = row["Name"].ToString();
                widgetMenuList.Add(widget);

            }
            return widgetMenuList;
        }
        public String getWidgetMenuString(string pageName)
        {
            DataTable dt = getDataTable(pageName);
            string widgets = "";
            foreach (DataRow row in dt.Rows) // Loop over the rows..
            {
                widgets="~"+widgets+row["Name"].ToString()+"~";
                

            }
            return widgets;
        }
        public DataTable getDataTable(string pageName)
        {
            DataTable dt = new DataTable();
            string querycode;
            querycode = "createWidget";
            string UserName = HttpContext.Current.User.Identity.Name;
            if (UserName == "")
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;            
            userdet = new UserCredentials().GetUser(UserName);
            //string pagename = eLedger_WEBAPI.Models.Strings.WildCardAdjust(PageName);
            ArrayList paramText = new ArrayList();
            paramText.Add("@UserRole");
            paramText.Add("@DefaultRole");
            paramText.Add("@PageName");

            ArrayList paramValues = new ArrayList();
            paramValues.Add(userdet.RoleId);
            paramValues.Add(eLedgerBusiness.Utils.ReadWebConfig.GetDefaultRole());
            paramValues.Add(pageName);
            dt = new WashDAL().getDataTable(querycode, paramText, paramValues);
            return dt;
        }

    }
}