﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using eLedgerBusiness.DAL;
using System.Net;
using System.Collections.Specialized;
using eLedgerBusiness.ASAP;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

namespace eLedgerBusiness
{
    public class AgreementBusiness
    {
        List<Agreement> AgmtList = new List<Agreement>();
        Agreement agmt = new Agreement();
        string AgmtKey = "";
        string ULN = "";
        int ContractType = 0;
        protected string domain = "washlaundry";
        public string getAgreementDetails(string uln)
        {
            try
            {
                string WinLoginID = System.Web.HttpContext.Current.User.Identity.Name;
                if (WinLoginID == null || WinLoginID == "")
                    WinLoginID = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                DataTable dt;
                DataTable dt1;
                DataTable dtmoreactive;
                DataTable dtRntl;
                DataSet ds;
                int UserID = 0;
                string querycode = "getAgreementInfo";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);
                ds = new WashDAL().getAgmtInfoDS(querycode, paramText, paramValues);
                StringBuilder sbJson = new StringBuilder("");
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    dtRntl = ds.Tables[1];
                    dt1 = ds.Tables[2];
                    dtmoreactive = ds.Tables[3];
                    Notes note = new Notes();

                    sbJson.Append("\"Agreement\":[");
                    if (dt.Rows.Count > 0)
                    {  
                            DataRow row = dt.Rows[0];
                            AgmtKey = row["AgmtKeyId"].ToString();
                            ULN = row["ULN"].ToString();
                            ContractType = Convert.ToInt32(row["ContractType"].ToString());
                            DataTable dt2 = new DataTable();
                            string querycode1 = "getAgmtSalesPerson";
                            ArrayList paramText1 = new ArrayList();
                            paramText1.Add("@AcctKeyID");
                            paramText1.Add("@AgmtID");
                            paramText1.Add("@ULN");
                            ArrayList paramValues1 = new ArrayList();
                            paramValues1.Add(0);
                            paramValues1.Add(AgmtKey);
                            paramValues1.Add(ULN);
                            dt2 = new WashDAL().getDataTable(querycode1, paramText1, paramValues1);

                            sbJson.Append("{");                            
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RenewalType", row["RenewalType"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ContractType", row["ContractType"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgreementType", row["AgreementType"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FutureExpiration", row["FutureExpiration"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TotalCount", row["TotalCount"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CPI", row["CPIFlag"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CPIFlag", row["CPIFlag"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EffectiveDate", row["EffectiveDate"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MgrAll", row["MgrAll"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendingMethod", row["VendingMethod"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceWasher", row["VendPriceWasher_Fmt"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceDryer", row["VendPriceDryer_Fmt"]);
                            if (dt2.Rows.Count > 0)
                            {
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TotalUsers", dt2.Rows.Count);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "NAME", dt2.Rows[0]["Name"]);
                            }
                            else
                            {
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TotalUsers", 0);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "NAME", "");
                            }

                            if (ContractType == 1 || ContractType == 13 || ContractType == 11 || ContractType == 14)
                            {
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Commission", row["Commission"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommissionType", row["CommissionType"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MaxCPIPercent", row["MaxCPIPercent"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm1", row["Gpm1"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm2", row["Gpm2"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm3", row["Gpm3"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm4", row["Gpm4"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm5", row["Gpm5"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm6", row["Gpm6"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm7", row["Gpm7"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Gpm8", row["Gpm8"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent1", row["CommPercent1"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent2", row["CommPercent2"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent3", row["CommPercent3"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent4", row["CommPercent4"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent5", row["CommPercent5"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent6", row["CommPercent6"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent7", row["CommPercent7"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommPercent8", row["CommPercent8"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MinComp", row["MinComp"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EnteredIntoDate", row["EnteredIntoDate"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommencedDate", row["CommencedDate"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ExpirationDate", row["ExpirationDate"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Minimum", row["Minimum"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InitialTerm", row["InitialTerm"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RenewalTerm", row["RenewalTerm"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RenewalNum", row["RenewalNum"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PaymentMethod", row["PaymentMethod"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PaymentType", row["PaymentType"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CollPayFrequency", row["CollPayFrequency"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PoolCollections", row["PoolCollections"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CalcBasis", row["CalcBasis"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CalcFrequency", row["CalcFrequency"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ThresholdBasis", row["ThresholdBasis"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ThresholdFrequency", row["ThresholdFrequency"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ReconciliationCommPercent", row["ReconciliationCommPercent"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ReconciliationEnabled", row["ReconciliationEnabled"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ReconciliationFreq", row["ReconciliationFreq"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ReconciliationMethod", row["ReconciliationMethod"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FixedAmount", row["FixedAmount"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FixedAmtBasis", row["FixedAmtBasis"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FixedAmtPayType", row["FixedAmtPayType"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FixedAmtPayFreq", row["FixedAmtPayFreq"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "BillShortfall", row["BillShortfall"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AdvCommAmt", row["AdvCommAmt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PrePaidAmt", row["PrePaidAmt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "BonusAmt", row["BonusAmt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FixedCPIValue", row["FixedCPIValue"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InterestFlag", row["InterestFlag"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TaxExempt", row["TaxExempt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CPIFreq", row["CPIFreq"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "DeductCCFeeFrom", row["DeductCCFeeFrom"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TSRE", row["TSRE"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TSREPercent", row["TSREPercent"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Country", row["Country"]);


                            }
                            if (ContractType == 2 || ContractType == 12 || ContractType == 11 || ContractType == 14)                            
                            {                                
                                if (dtRntl.Rows.Count > 0)
                                {
                                    sbJson.Append("\"RentalInfo\":[");
                                    for (int i = 0; i < dtRntl.Rows.Count; i++)
                                    {
                                        DataRow rowRntl = dtRntl.Rows[i];
                                        sbJson.Append("{");
                                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CalcBasis", rowRntl["CalcBasis"]);
                                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CalcFrequency", rowRntl["CalcFrequency"]);
                                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MachType", rowRntl["MachType"]);
                                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MachCnt", rowRntl["MachCnt"]);
                                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RntlPrice", rowRntl["RntlPrice"]);
                                        sbJson.AppendFormat("\"{0}\":\"{1}\"", "RntlSum", rowRntl["RntlSum"]);
                                        sbJson.Append("},");
                                    }
                                    sbJson.Replace(",", "],", sbJson.Length - 2, 2);
                                }
                                else sbJson.Append("\"RentalInfo\":[],");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TotalMachCnt", row["TotalMachCnt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TotalRntlPrice", row["TotalRntlPrice"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TotalRntlSum", row["TotalRntlSum"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EnteredIntoDate", row["EnteredIntoDate"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CommencedDate", row["CommencedDate"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ExpirationDate", row["ExpirationDate"]);                                
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InitialTerm", row["InitialTerm"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RenewalTerm", row["RenewalTerm"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RenewalNum", row["RenewalNum"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Billing", row["Billing"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PaymentType", row["PaymentType"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ccLastDigits", row["ccLastDigits"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MaxCPIPercent", row["MaxCPIPercent"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AdvCommAmt", row["AdvCommAmt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PrePaidAmt", row["PrePaidAmt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "BonusAmt", row["BonusAmt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FixedCPIValue", row["FixedCPIValue"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InterestFlag", row["InterestFlag"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TaxExempt", row["TaxExempt"]);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CPIFreq", row["CPIFreq"]);
                            }
                        sbJson.Replace(", ", "}],", sbJson.Length - 2, 2);
                    }
                    else
                    {
                        sbJson.Append("],");
                    }
                    if (dt1.Rows.Count > 0)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer serializerNotes = new System.Web.Script.Serialization.JavaScriptSerializer();
                        List<Dictionary<string, object>> rowsNotes = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowNotes = null;
                        foreach (DataRow row1 in dt1.Rows)
                        {
                            if (row1["NoteText"].ToString() != "")
                            {
                                rowNotes = new Dictionary<string, object>();
                                rowNotes.Add("NoteDate_Fmt", row1["NoteDate_Fmt"]);
                                rowNotes.Add("NoteText", row1["NoteText"].ToString());
                                rowsNotes.Add(rowNotes);
                            }
                        }
                        sbJson.Append("\"AgreementNotes\":" + serializerNotes.Serialize(rowsNotes) +",");
                    }
                    else
                        sbJson.Append("\"AgreementNotes\":[],");
                    if (dtmoreactive.Rows.Count > 0)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer serializerNotes = new System.Web.Script.Serialization.JavaScriptSerializer();
                        List<Dictionary<string, object>> rowsNotes = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowNotes = null;
                        foreach (DataRow row1 in dtmoreactive.Rows)
                        {
                            if (row1["Msg"].ToString() != "")
                            {
                                rowNotes = new Dictionary<string, object>();
                                rowNotes.Add("Msg", row1["Msg"]);
                                rowsNotes.Add(rowNotes);
                            }
                        }
                        sbJson.Append("\"AgreementMore\":" + serializerNotes.Serialize(rowsNotes));
                    }
                    else
                        sbJson.Append("\"AgreementMore\":[]");

                }
                if (AgmtKey != "")
                {
                    DataTable dt2 = new DataTable();
                    string querycode1 = "getAgmtSalesPerson";
                    ArrayList paramText1 = new ArrayList();
                    paramText1.Add("@AcctKeyID");
                    paramText1.Add("@AgmtID");
                    paramText1.Add("@ULN");
                    ArrayList paramValues1 = new ArrayList();
                    paramValues1.Add(0);
                    paramValues1.Add(AgmtKey);
                    paramValues1.Add(ULN);                    
                    dt2 = new WashDAL().getDataTable(querycode1, paramText1, paramValues1);
                    if (dt2.Rows.Count > 0)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer serializerAggsales = new System.Web.Script.Serialization.JavaScriptSerializer();
                        List<Dictionary<string, object>> rowsAggsales = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowAggsales = null;
                        foreach (DataRow row2 in dt2.Rows)
                        {
                            if (row2["Rank"].ToString() != "1")
                            {
                                rowAggsales = new Dictionary<string, object>();
                                rowAggsales.Add("Rank", row2["Rank"]);
                                rowAggsales.Add("SalesRepKeyID", row2["SalesRepKeyID"].ToString());
                                rowAggsales.Add("SalesAgmtID", row2["SalesAgmtID"].ToString());
                                rowAggsales.Add("Name", row2["Name"].ToString());
                                rowAggsales.Add("SalesUserID", row2["SalesUserID"].ToString());
                                rowAggsales.Add("Percentage", row2["Percentage"].ToString());
                                rowAggsales.Add("TotPct", row2["TotPct"].ToString());
                                rowAggsales.Add("Balance", row2["Balance"].ToString());
                                rowsAggsales.Add(rowAggsales);
                            }

                        }
                        sbJson.Append(",\"AgreementSalesTeam\":" + serializerAggsales.Serialize(rowsAggsales));
                    }
                    else
                        sbJson.Append(",\"AgreementSalesTeam\":[]");
                }
                return sbJson.ToString();
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(),"Agreement","ULN:"+uln);
                return null;
            }
        }


        protected void MakeBlankRow(ref DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
        }
        public string UserGet(string LoginId)
        {
            try
            {
                DataTable dt1;
                string querycode1 = "getAgreementUser";
                ArrayList paramText1 = new ArrayList();
                paramText1.Add("@UserDomain");
                ArrayList paramValues1 = new ArrayList();
                paramValues1.Add(LoginId);
                dt1 = new WashDAL().getDataTable(querycode1, paramText1, paramValues1);
                string winLoginID = "";
                foreach (DataRow row in dt1.Rows)
                {
                    winLoginID = row["UserDomain1"].ToString();
                }
                return winLoginID;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(),"Agreement : userget()","Login:"+LoginId);
                return null;
            }
        }
    }
    
}