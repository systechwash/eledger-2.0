﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using eLedgerBusiness.DAL;
using System.Net;
using System.Collections.Specialized;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

namespace eLedgerBusiness
{
    public class LMSurveyBusiness
    {
        public enum enLocationType { Unknown = 0, All = 1, LocationCenter = 2, LocationNearBy = 3, Laundromat = 4 };
        
        public string getLMSurvey(string uln)
        {
            int rowCount = 0;
            int rowCount1 = 0;
            int rowCount2 = 0;
            StringBuilder sbJson = new StringBuilder("{\"LMSurveyDet\":[");
            string WinLoginID = System.Web.HttpContext.Current.User.Identity.Name;
            if (WinLoginID == null || WinLoginID == "")
                WinLoginID = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string locNum = uln.Trim().ToUpper().Replace("-", "");
            Int32 locTypeValue = (Int32)enLocationType.Laundromat;
            string queryCode = "getLMSurvey";
            Double dist2 = 2.0;
            ArrayList paramText = new ArrayList();
            paramText.Add("@locationType");
            paramText.Add("@uln");
            paramText.Add("@distance");
            paramText.Add("@user");
            ArrayList paramValues = new ArrayList();
            paramValues.Add(locTypeValue);
            paramValues.Add(locNum);
            paramValues.Add(dist2);
            paramValues.Add(WinLoginID);

            DataTable dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);

            foreach (DataRow reader1 in dt.Rows)
            {
                if (Convert.ToDouble(reader1["distance"].ToString()) > 0.0 && Convert.ToDouble(reader1["distance"].ToString()) <= 0.5)
                {
                    if (rowCount != 0)
                        sbJson.Append(",{");
                    else
                        sbJson.Append("{");
                    rowCount++;
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", uln);
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "distance", reader1["distance"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LocId", reader1["LocId"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LocName", reader1["LocName"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Address", reader1["Address"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "City", reader1["City"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "State", reader1["State"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Zip", reader1["Zip"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "WD", reader1["WD"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\" ", "SurveyedOn", reader1["SurveyedOn"].ToString());
                    sbJson.Append("}");
                }
                if (Convert.ToDouble(reader1["distance"].ToString()) > 0.5 && Convert.ToDouble(reader1["distance"].ToString()) <= 1.0)
                {
                    if (rowCount1 != 0)
                        sbJson.Append(",{");
                    else
                        sbJson.Append("],\"LMSurveyDet1\":[{");
                    rowCount1++;
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", uln);
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "distance", reader1["distance"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LocId", reader1["LocId"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LocName", reader1["LocName"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Address", reader1["Address"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "City", reader1["City"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "State", reader1["State"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Zip", reader1["Zip"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "WD", reader1["WD"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\" ", "SurveyedOn", reader1["SurveyedOn"].ToString());
                    sbJson.Append("}");
                }
                if (Convert.ToDouble(reader1["distance"].ToString()) > 1.0 && Convert.ToDouble(reader1["distance"].ToString()) <= 2.0)
                {
                    if (rowCount2 != 0)
                        sbJson.Append(",{");
                    else
                    {
                        if (rowCount1 == 0) { sbJson.Append("],\"LMSurveyDet1\":["); }
                        sbJson.Append("],\"LMSurveyDet2\":[{");
                    }
                    rowCount2++;
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", uln);
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "distance", reader1["distance"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LocId", reader1["LocId"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LocName", reader1["LocName"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Address", reader1["Address"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "City", reader1["City"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "State", reader1["State"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Zip", reader1["Zip"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\", ", "WD", reader1["WD"].ToString());
                    sbJson.AppendFormat("\"{0}\":\"{1}\" ", "SurveyedOn", reader1["SurveyedOn"].ToString());
                    sbJson.Append("}");
                }
            }
            if (rowCount1 == 0 && rowCount2 == 0) { sbJson.Append("],\"LMSurveyDet1\":[],\"LMSurveyDet2\":[]"); }
            else if (rowCount2 == 0) { sbJson.Append("],\"LMSurveyDet2\":[]"); }
            else { sbJson.Append("]"); }
            sbJson.Append(",\"LMSurveyCount\":[{\"rowCount\":\"" + rowCount + "\",\"rowCount1\":\"" + rowCount1 + "\",\"rowCount2\":\"" + rowCount2 + "\"}]}");
            return sbJson.ToString();
        }

        public string getLMSurveyLink(string locId)
        {
            string link = System.Configuration.ConfigurationManager.AppSettings["LMSurveyLink"].ToString();

            StringBuilder sbJson = new StringBuilder("{");
            sbJson.Append("\"LMSurveyLink\":[{");
            sbJson.AppendFormat("\"{0}\":\"{1}\" ", "URL", link + locId + "&outside=true");
            sbJson.Append("}]}");
            return sbJson.ToString();
        }

    }
}