﻿using eLedgerBusiness.DAL;
using eLedgerEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using eLedgerBusiness.DAL;
using System.Data.SqlClient;using Newtonsoft.Json;
namespace eLedgerBusiness
{
    public class SearchBusiness
    {
        public string SearchLocation(Location searchParam)
        {
            ArrayList paramText = new ArrayList();
            paramText.Add("@SearchULN");
            paramText.Add("@SearchName");
            paramText.Add("@LaundryRoomDesc");
            paramText.Add("@SearchAddress");
            paramText.Add("@SearchCity");
            paramText.Add("@SearchState");
            paramText.Add("@SearchZip");
            paramText.Add("@SearchWebNum");
            ArrayList paramValues = new ArrayList();
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.ULN)));
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.Name)));
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.LaundryRooms_Fmt)));
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.Address)));
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.City)));
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.State)));
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.Zip)));
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.WebNum)));
            string columns = "ULN, Address, City, State, Zip, Status, ContractType, PropertyName";
            string querycode = "getLocationsListJson";
            string jsondata = "{\"SearchLocation\":" + new WashDAL().getJsonFromQueryColumns(querycode, paramText, paramValues, columns) + "}";
            return jsondata;
        }


        public string SearchContactList(string id, string name, string address, string city, string state, string zip, string phone, string flag)
        {
            DataSet ds = new DataSet("Data");
            DataTable dt;
            string searchName = new WashDAL().MakeBlkstring(name);
            string searchId;
            if ((id == "") || (id == null))
                searchId = new WashDAL().MakeBlkstring(id);
            else
                searchId = "*" + new WashDAL().MakeBlkstring(id);
            string searchAddress = new WashDAL().MakeBlkstring(address);
            string searchCity = new WashDAL().MakeBlkstring(city);
            string searchState = new WashDAL().MakeBlkstring(state);
            string searchZip = new WashDAL().MakeBlkstring(zip);
            string searchPhone = new WashDAL().MakeBlkstring(phone);
            string searchFlag = new WashDAL().MakeBlkstring(flag);
            int maxSearchResults = eLedgerBusiness.Utils.ReadWebConfig.MaxSearchResults();
            string escClause = "ESCAPE '" + eLedgerBusiness.Utils.ReadWebConfig.EscapeCode() + "'";
            string querycode = "getContactlist";
            if (searchId != "")
                searchId = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchId);
            searchName = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchName);
            searchAddress = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchAddress);
            searchCity = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchCity);
            searchState = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchState);
            searchZip = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchZip);
            searchPhone = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchPhone.Trim().Replace("-", ""));
            searchFlag = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchFlag);
            string contactList = searchContacts(querycode, searchId, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
            return contactList.ToString();
        }


        public string searchContacts(string querycode, string id, string name, string address, string city, string state, string zip, string phone, string flag)
        {
            ArrayList paramText = new ArrayList();
            paramText.Add("@SearchId");
            paramText.Add("@SearchName");
            paramText.Add("@SearchAddress");
            paramText.Add("@SearchCity");
            paramText.Add("@SearchState");
            paramText.Add("@SearchZip");
            paramText.Add("@SearchPhone");
            paramText.Add("@Flag");

            ArrayList paramValues = new ArrayList();


            paramValues.Add(id);
            paramValues.Add(name);
            paramValues.Add(address);
            paramValues.Add(city);
            paramValues.Add(state);
            paramValues.Add(zip);
            paramValues.Add(phone);
            paramValues.Add(flag);
            string Json = "";
            try
            {         

                DataTable dt = new WashDAL().getDataTable(querycode, paramText, paramValues);

                 Json = Newtonsoft.Json.JsonConvert.SerializeObject(dt, Formatting.Indented);

                 Json = "{\"SearchContact\":" + Json + ",\"total\":\"" + dt.Rows.Count + "\" " + "}";

            }
            catch
            {
                Json = "{\"SearchContact\":" + "[]" + ",\"total\":\"" + "[]" + "\"}";
            }
            return Json;
        }
        

        public string replaceNull(string value)
        {
            if (value == null)
                return "";
            else
                return value;
        }
    }
}