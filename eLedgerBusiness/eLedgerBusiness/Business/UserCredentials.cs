﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using System.Data;
using System.Text;


using eLedgerBusiness.DAL;
using System.Net;

using System.Collections;

namespace eLedgerBusiness
{
    public class UserCredentials
    {
        public string GetUsersList()
        {
            DataTable dt = new DataTable();
            User userdet = new User();
            List<User> userdetList = new List<User>();
            string queryCode;
            queryCode = "UsersList";
            ArrayList paramText = new ArrayList();
            ArrayList paramValues = new ArrayList();
            dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
            StringBuilder sbJson = new StringBuilder("[");
            foreach (DataRow row in dt.Rows) // Loop over the rows..
            {
                sbJson.Append("{");
                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "UserID", (int)row["UserID"]);
                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "Display_Name", row["Display_Name"].ToString());
                sbJson.Append("}, ");
         
            }
             if (sbJson.ToString().Length > 2)
                    sbJson.Replace(", ", "]", sbJson.Length - 2, 2);

             return sbJson.ToString();
        }

        public User GetUser(string _networkLogin)
        {
            if (_networkLogin == null)
                _networkLogin = HttpContext.Current.User.Identity.Name;
            if (_networkLogin == null)
                _networkLogin = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            User userdet = new User();
            
            if (HttpContext.Current.Session != null)
            {
                userdet = (User)HttpContext.Current.Session["User_" + _networkLogin];
            }
            if (userdet != null && userdet.UserID > 0) { return userdet; }
            else
            {
                userdet = new User();
                DataTable dt = new DataTable();
                string queryCode;
                queryCode = "userCredentialsName";
                ArrayList paramText = new ArrayList();
                paramText.Add("@NAME");
                ArrayList paramValues = new ArrayList();
               // paramValues.Add(_networkLogin);
               paramValues.Add("WASHLAUNDRY\\vmuthuswamy");
                dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    userdet.FirstName = row["FirstName"].ToString();
                    userdet.RoleId = row["RoleId"].ToString();
                    userdet.UserID = (int)row["UserId"];
                    userdet.FullName = row["FullName"].ToString();
                    userdet.LastName = row["Name"].ToString();
                    userdet.BranchList = row["BranchList"].ToString();
                    if (HttpContext.Current.Session != null)
                    {
                        HttpContext.Current.Session.Add("User_" + _networkLogin, userdet);
                    }
                    return userdet;
                }
                else return null;
            }
        }

        public User GetLogonUser(string _networkLogin)
        {
            if (_networkLogin == null)
                _networkLogin = HttpContext.Current.User.Identity.Name;
            if (_networkLogin == null)
                _networkLogin = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            User userdet = new User();
            DataTable dt = new DataTable();
            string queryCode;
            queryCode = "userCredentialsName";
            ArrayList paramText = new ArrayList();
            paramText.Add("@NAME");
            ArrayList paramValues = new ArrayList();
            //paramValues.Add(_networkLogin);
            paramValues.Add("WASHLAUNDRY\\vmuthuswamy");

            dt = new WashDAL().getDataTable(queryCode, paramText, paramValues);
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                userdet.FirstName = row["FirstName"].ToString();
                userdet.RoleId = row["RoleId"].ToString();
                userdet.UserID = (int)row["UserId"];
                userdet.FullName = row["FullName"].ToString();
                userdet.LastName = row["Name"].ToString();
                userdet.BranchList = row["BranchList"].ToString();
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session.Add("User_" + _networkLogin, userdet);
                }
                return userdet;
            }
            else return null;
        }


        public string GetJsonUserData(string _networkLogin)
        {
            if (_networkLogin == null)
                _networkLogin = HttpContext.Current.User.Identity.Name;
            if (_networkLogin == null)
                _networkLogin = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            User userdet = new User();
            StringBuilder sbJson = new StringBuilder("[");
            userdet = GetUser(_networkLogin);
            sbJson.Append("{");
            if (userdet != null && userdet.UserID >= 0)
            {
                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FirstName", userdet.FirstName);
                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RoleId", userdet.RoleId);
                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "UserId", userdet.UserID);
                sbJson.AppendFormat("\"{0}\":\"{1}\"", "FullName", userdet.FullName);
            }
            sbJson.Append("}]");
            return sbJson.ToString();
        }
    }
}