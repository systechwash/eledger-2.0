﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using eLedgerWebAPI.Properties;
using System.Xml;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using Newtonsoft.Json;

namespace eLedgerBusiness.DAL
{
    public class WashDAL
    {
        string ConStr;

        public void initConnectionString()
        {
            ConStr = System.Configuration.ConfigurationManager.AppSettings["SQL_Conn"];
        }
        public void CMSinitConnectionString()
        {
            ConStr = System.Configuration.ConfigurationManager.AppSettings["CMS"];
        }
        public void ASAPinitConnectionString()
        {
            ConStr = System.Configuration.ConfigurationManager.AppSettings["ASAP_SQL_Conn"];
        }        

        public SqlConnection openConnection1()
        {
            initConnectionString();
            SqlConnection dbConn = new SqlConnection(ConStr);
            dbConn.Open();
            return dbConn;

        }

        public DataTable getDataTable(string queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            DataTable dt = new DataTable();
            try
            {
                //openConnection();
                
                if (queryCode == "getAgmtSalesPerson")
                {
                    ASAPinitConnectionString();
                }               
               
                else
                    initConnectionString();
                using (SqlConnection dbConn = new SqlConnection(ConStr))
                {
                    string query = getQuery(queryCode);
                    using (SqlCommand dbCommand1 = new SqlCommand(query, dbConn))
                    {
                        dbCommand1.CommandTimeout = 300;
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand1.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }

                        using (SqlDataAdapter dbDataAdapter = new SqlDataAdapter(dbCommand1))
                        {
                            dbDataAdapter.Fill(dt);
                        }
                    }
                    dbConn.Close();
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return dt;
        }

        public DataSet getdatasetfromColumn(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection dbConnection = openConnection1())
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.Connection = dbConnection;
                        string query = getQuery(queryCode);
                        query = query.Replace("@Columns", Columns);
                        dbCommand.CommandText = query;
                        dbCommand.CommandTimeout = 0;
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }
                        using (SqlDataAdapter dbDataAdapter = new SqlDataAdapter(dbCommand))
                        {
                            dbDataAdapter.Fill(ds);
                        }
                    }
                    dbConnection.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //closeConnection();
            return ds;

        }

        public DataTable getDataTableColumns(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns)
        {
            DataTable dt = new DataTable();
            try
            {
                //openConnection();               
                initConnectionString();
                using (SqlConnection dbConn = new SqlConnection(ConStr))
                {
                    string query = getQuery(queryCode);
                    query = query.Replace("@Columns", Columns);
                    using (SqlCommand dbCommand1 = new SqlCommand(query, dbConn))
                    {
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand1.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }

                        using (SqlDataAdapter dbDataAdapter = new SqlDataAdapter(dbCommand1))
                        {
                            dbDataAdapter.Fill(dt);
                        }
                    }
                    dbConn.Close();
                }

            }
            catch (Exception e)
            {
                throw e;
                //throw e;
            }
            return dt;
        }

        public DataSet getAgmtInfoDS(string queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection dbConnection = openConnection1())
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.Connection = dbConnection;
                        dbCommand.CommandText = getQuery(queryCode);
                        dbCommand.CommandTimeout = 0;
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }
                        using (SqlDataAdapter dbDataAdapter = new SqlDataAdapter(dbCommand))
                        {
                            dbDataAdapter.Fill(ds);
                        }
                    }
                    dbConnection.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //closeConnection();
            return ds;

        }

        public string getJsonFromQuery(string queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            string sb = "";            
            
            DataTable dt=getDataTable(queryCode,paramNames,paramValues);
         
                try
                {
                    sb = GetJson(dt);                          
                }
                catch (SqlException sqlException)
                { // exception                    
                    throw sqlException;
                }
                finally
                {
                    // close the connection
                }

                return sb;
        }

        public string getJsonFromQueryColumns(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns)
        {
            int rowCount = 0;
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
          
                initConnectionString();
            using (SqlConnection dbConn = new SqlConnection(ConStr))
            {
                try
                {
                    DataTable dt = getDataTableColumns(queryCode, paramNames, paramValues, Columns);

                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;

                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            if (col.ColumnName == "ULN")
                                if (dr[col].ToString().Length > 8)
                                {
                                    row.Add(col.ColumnName, dr[col].ToString().Substring(0, 4) + "-" + dr[col].ToString().Substring(4, 2) + "-" + dr[col].ToString().Substring(6, 3));
                                }
                                else
                                {
                                    row.Add(col.ColumnName.Trim(), dr[col]);
                                }
                                else
                                    row.Add(col.ColumnName.Trim(), dr[col]);
                        }
                        rows.Add(row);
                    } 
                    string jsonOutput = serializer.Serialize(rows);;
                    if (jsonOutput.ToString().Length > 0)
                        jsonOutput = jsonOutput+",\"total\":\"" + dt.Rows.Count + "\"";
                    return jsonOutput;
                }
                catch (SqlException sqlException)
                { // exception
                    
                    throw sqlException;
                }
                finally
                {
                    dbConn.Close(); // close the connection
                }
            }
        }
      
        public int executeQuery(String queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            int j = 0;
            try
            {
                using (SqlConnection dbConnection = openConnection1())
                {
                    using (SqlCommand dbCommand = new SqlCommand())
                    {
                        dbCommand.Connection = dbConnection;
                        dbCommand.CommandText = getQuery(queryCode);

                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }

                        j = dbCommand.ExecuteNonQuery();
                    }
                    dbConnection.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                //closeConnection();
                //dbConnection.Close();
            }
            return j;

        }

        public String getQuery(string queryCode)
        {
            return eLedgerBusiness.Properties.WashQueries.ResourceManager.GetString(queryCode);
        }
      
        //Create the LOG File
        public void CreateLog(string exception, string method,string param)
        {

            // Create a writer and open the file:            
            //string UserName = System.Web.HttpContext.Current.User.Identity.Name;

             string UserName = "WASHLAUNDRY\\vmuthuswamy";
            if (UserName == "")
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;


            var fpath = string.Empty;
            var path = string.Concat(AppDomain.CurrentDomain.BaseDirectory, @"\log\");
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
            fpath = path + "ErrorLog_" + DateTime.Now.ToString("dd-MMM-yy") + ".txt";
            if (System.IO.File.Exists(fpath) == false)
                System.IO.File.Create(fpath).Close();

            FileStream fs = new FileStream(fpath, FileMode.Append, FileAccess.Write);
            StreamWriter SWriter = new StreamWriter(fs);
            SWriter.WriteLine("--------Error Log --- Time:" + DateTime.Now.ToString("dd-MMM-yy") + " -" + DateTime.Now.ToString("T") + "----" + new UserCredentials().GetUser(UserName).LastName.ToString() + "--------"+method+"---"+param);
            SWriter.WriteLine("");
            SWriter.WriteLine(exception);
            SWriter.WriteLine("");
            SWriter.Flush();
            SWriter.Close();
        }

        //Converting DataTable to Json data
        public string GetJson(DataTable dt)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.Indented);
        }

        public string MakeBlkstring(string val)
        {
            if (val == null)
            {
                val = "";
            }
            return val;
        }

    }
}