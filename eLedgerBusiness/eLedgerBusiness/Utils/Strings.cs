﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  eLedgerBusiness.Utils
{
    public class Strings
    {

        public Strings()
        {

        }

        /// <summary>
        /// Make a dos style string for 'like' querying.  
        /// </summary>
        /// <param name="str">String to convert</param>
        /// <returns>Converted string</returns>
        public static string WildCardAdjust(string str)
        {
            string outStr = str;
            string escCode =  eLedgerBusiness.Utils.ReadWebConfig.EscapeCode();

            outStr = outStr.Replace(escCode, escCode + escCode);
            outStr = outStr.Replace("%", escCode + "%");
            outStr = outStr.Replace("_", escCode + "_");
            outStr = outStr.Replace("*", "%");
            outStr = outStr.Replace("?", "_");

            return outStr;
        }
        public static string LoseSingleQuote(string str)
        {
            string outStr = str;
            outStr = outStr.Replace("'", "''");
            return outStr;
        }
    }
}
